package com.luc.logistica.crearOrdenes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class CrearOrdenesApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CrearOrdenesApplication.class, args);
	}

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CrearOrdenesApplication.class);
	}
	
}
