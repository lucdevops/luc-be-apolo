package com.luc.logistica.crearOrdenes.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.luc.logistica.crearOrdenes.model.Referencias;
import com.luc.logistica.crearOrdenes.model.User;
import com.luc.logistica.crearOrdenes.service.OrdenCompraService;
import com.luc.logistica.crearOrdenes.service.ProveedorService;
import com.luc.logistica.crearOrdenes.service.ReferenciasService;
import com.luc.logistica.crearOrdenes.service.UserService;


@RestController
//@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
//@RequestMapping("/api")
public class IndexController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private OrdenCompraService ordenCompraService;
	@Autowired
	private ReferenciasService refService;
	
	@RequestMapping("/")
	public ModelAndView index(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		return modelAndView;
	}

	@RequestMapping("/recepcion")
	public ModelAndView indexEntrada() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("indexEntrada");
		return modelAndView;
	}
	
	@RequestMapping("/carga") 
	public ModelAndView carga(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("carga");
		modelAndView.addObject("proveedores",proveedorService.findAll());
		return modelAndView;
	}
	
	@GetMapping("/libera") 
	public ModelAndView libera(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("libera");
		return modelAndView;
	}
	
	@GetMapping("/factura") 
	public ModelAndView factura(){
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("factura");
		modelAndView.addObject("pedidos",ordenCompraService.findFinalizado());
		
		//ordenCompraService.findFinalizado();
		return modelAndView;
	}
	
	@GetMapping("/prods")
	public List<Referencias> getProds(){
		return (List<Referencias>) refService.findAll();
	}


	@RequestMapping(value="/login", method = RequestMethod.POST) 
	public void login(@Valid User user, BindingResult bindingResult, HttpServletResponse response) throws IOException{
		User log = userService.findUserByUserName(user.getId());
		
		if(log != null && (log.getPassword().equals(user.getPassword()))) {
			response.sendRedirect("/logistica/pedidos");
		}else {
			response.sendRedirect("/");
		}
	}

	@RequestMapping(value = "/loginE", method = RequestMethod.POST)
	public void loginE(@Valid User user, BindingResult bindingResult, HttpServletResponse response) throws IOException {
		User log = userService.findUserByUserName(user.getId());

		if (log != null && (log.getPassword().equals(user.getPassword()))) { 
			response.sendRedirect("/logistica/recepcion/pedidos");
		} else {
			response.sendRedirect("/logistica/recepcion");
		}

	}

}
