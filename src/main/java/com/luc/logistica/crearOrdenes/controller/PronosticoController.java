package com.luc.logistica.crearOrdenes.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.luc.logistica.crearOrdenes.service.ArticuloService;
import com.luc.logistica.crearOrdenes.service.PedidoService;

@Controller
public class PronosticoController {
		
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ArticuloService articuloService;
	@Autowired
	private PedidoService pedidoService;
	
	@RequestMapping("/pronosticos")
	public ModelAndView carga(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pronosticos/consulta");
		modelAndView.addObject("articulos",articuloService.findAll());
		return modelAndView;
	}
	
	@PostMapping("/pronosticos/ventas")
	public ModelAndView buscar(@RequestParam("texto") String texto){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pronosticos/ventas");
		modelAndView.addObject("infoPronostico",pedidoService.obtenerInfoPronostico(texto));
		if(texto.equals("")) {
			modelAndView.addObject("articulos",articuloService.findAll());
		}else {
			modelAndView.addObject("articulos",articuloService.findByTexto(texto));
		}
		return modelAndView;
	}
	
}
