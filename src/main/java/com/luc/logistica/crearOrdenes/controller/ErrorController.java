package com.luc.logistica.crearOrdenes.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.luc.logistica.crearOrdenes.model.ErrorResponse;

@RestController
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {
	
	@RequestMapping("/error")
	@ResponseBody
	public ResponseEntity<ErrorResponse> error(HttpServletRequest request) {
		ErrorResponse err = new ErrorResponse();
		err.setStatus((Integer) request.getAttribute("javax.servlet.error.status_code"));
		err.setMsj(Optional.of(HttpStatus.valueOf(err.getStatus()))
		        .map(HttpStatus::getReasonPhrase).orElseGet(null));
		return new ResponseEntity<>(err,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	  
	@Override
	public String getErrorPath() {
		return null;
	}

}
