package com.luc.logistica.crearOrdenes.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.luc.logistica.crearOrdenes.model.EncabezadoPedido;
import com.luc.logistica.crearOrdenes.model.User;
import com.luc.logistica.crearOrdenes.service.PedidoService;
import com.luc.logistica.crearOrdenes.service.UserService;
import com.luc.logistica.crearOrdenes.model.DetalleOCPed;
import com.luc.logistica.crearOrdenes.model.LoteEnt;
import com.luc.logistica.crearOrdenes.model.Ped;
import com.luc.logistica.crearOrdenes.model.Um;
import com.luc.logistica.crearOrdenes.model.Und;
import com.luc.logistica.crearOrdenes.service.DetalleOCPedService;
import com.luc.logistica.crearOrdenes.service.LoteEntService;
import com.luc.logistica.crearOrdenes.service.PedService;
import com.luc.logistica.crearOrdenes.service.UmService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PedidosController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PedidoService pedidoService;
	@Autowired
	private UserService userService;
	@Autowired
	private PedService pedService;
	@Autowired
	private DetalleOCPedService detOrdenCompraPedService;
	@Autowired
	private UmService umService;
	@Autowired
	private LoteEntService loteEntService;

	@GetMapping("/pedidos")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pedidos/proceso");
		modelAndView.addObject("pedidos", pedidoService.findEnProceso());
		return modelAndView;
	}

	@PostMapping("/pedidos/buscar")
	public ModelAndView buscar(@RequestParam("texto") String texto) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pedidos/proceso");
		if (texto.equals("")) {
			modelAndView.addObject("pedidos", pedidoService.findEnProceso());
		} else {
			modelAndView.addObject("pedidos", pedidoService.findByTexto(texto));
		}
		return modelAndView;
	}

	@GetMapping("/pedidos/asignar")
	public ModelAndView formularioCliente() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pedidos/asignar");
		modelAndView.addObject("pedidos", pedidoService.findEnProceso());
		modelAndView.addObject("colaboradores", userService.findAll());
		return modelAndView;
	}

	@PostMapping("/pedidos/asignar")
	public String crearCliente(@Valid @ModelAttribute("pedido") String pedido,
			@Valid @ModelAttribute("colaborador") String usuario, RedirectAttributes redirectAttributes) {
		EncabezadoPedido pedCom = pedidoService.findById(pedido);
		User usuCom = userService.findUserByUserName(usuario);
		pedCom.setAlista(usuCom.getId());
		pedidoService.savePedido(pedCom);
		redirectAttributes.addFlashAttribute("message", "Pedido asignado con exito");
		return "redirect:/informe";
	}

	@GetMapping("/recepcion/pedidos")
	public ModelAndView indexE() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pedidos/ordenE");
		modelAndView.addObject("pedidos", pedService.findEnEspera());
		return modelAndView;
	}

	@GetMapping("/pedidos/error/{numero}/{tipo}")
	@ResponseBody
	public ModelAndView error(@PathVariable("numero") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/error");
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/fin/{numero}/{tipo}")
	@ResponseBody
	public ModelAndView fin(@PathVariable("numero") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/fin");
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/errorRecep/{numero}/{tipo}")
	@ResponseBody
	public ModelAndView errorRecep(@PathVariable("numero") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/errorRecep");
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/errorCom/{numero}/{tipo}")
	@ResponseBody
	public ModelAndView errorCom(@PathVariable("numero") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/errorCom");
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@PostMapping("/pedidos/logout")
	public String logout() {
		return "redirect:/recepcion";
	}

	@GetMapping("/pedidos/lote/{prod}/{num}/{cnt}/{tipo}/{estado}")
	@ResponseBody
	public ModelAndView lotes(@PathVariable("prod") String prod, @PathVariable("num") String num,
			@PathVariable("cnt") int cnt, @PathVariable("tipo") String tipo, @PathVariable("estado") String estado) {

		if (estado.equals("x")) {
			ModelAndView modelAndView = new ModelAndView();
			List<Um> unidad = umService.findById(prod);

			Und un = new Und();
			int cj = 0, dp = 0, numcajas = 0, numdisplay = 0, numunidades = cnt;

			if (unidad.size() > 0) {
				for (int i = 0; i < unidad.size(); i++) {
					if (unidad.get(i).getOrg().equals("CJ")) {
						cj = unidad.get(i).getConv();
					} else if (unidad.get(i).getOrg().equals("DP")) {
						dp = unidad.get(i).getConv();
					} else {
						cj = 0;
						dp = 0;
					}
				}

				if (cj > 0) {
					double divcj = (double) cnt / (double) cj;
					int parteEntCj = (int) divcj;
					double parteDecCj = divcj - parteEntCj;
					numcajas = parteEntCj;

					if (parteDecCj > 0) {
						if (dp > 0) {
							double divdp = (cj * (divcj - parteEntCj)) / dp;
							int parteEntDp = (int) divdp;
							double parteDecDp = divdp - parteEntDp;
							numdisplay = parteEntDp;
							if (parteDecDp > 0) {
								numunidades = (int) Math.round(((divdp - numdisplay) * dp));
							} else {
								numunidades = 0;
							}

						}
					} else {
						numdisplay = 0;
						numunidades = 0;
					}
				} else {
					numcajas = 0;
					numdisplay = 0;
					numunidades = cnt;
				}

				un.setCajas(numcajas);
				un.setDisplay(numdisplay);
				un.setUnidades(numunidades);
				un.setProducto(prod);
				un.setNumero(Integer.parseInt(num));
				un.setTipo(tipo);
			} else {
				if (unidad.size() == 0) {
					un.setCajas(numcajas);
					un.setDisplay(numdisplay);
					un.setUnidades(numunidades);
					un.setProducto(prod);
					un.setNumero(Integer.parseInt(num));
					un.setTipo(tipo);
				}
			}

			modelAndView.setViewName("/pedidos/lotesE");
			modelAndView.addObject("und", un);

			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView();
			List<DetalleOCPed> det = detOrdenCompraPedService.findByDetalle(Integer.parseInt(num), tipo);

			Und un = new Und();
			un.setNumero(Integer.parseInt(num));
			un.setTipo(tipo);
			modelAndView.setViewName("/pedidos/detalleE");
			modelAndView.addObject("detalle", det);
			modelAndView.addObject("und", un);
			return modelAndView;
		}
	}

	@PostMapping("/recepcion/pedidos/buscarE")
	public ModelAndView buscarE(@Valid @RequestParam("numero") Integer numero, @RequestParam("tipo") String tipo,
			@RequestParam("fac") String fac) {
		ModelAndView modelAndView = new ModelAndView();
		List<DetalleOCPed> det = detOrdenCompraPedService.findByDetalle(numero, tipo);
		Ped ped = pedService.findbyPed(numero, tipo);
		ped.setNotas(fac);
		pedService.savePed(ped);

		Und un = new Und();
		un.setNumero(numero);
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/detalleE");
		modelAndView.addObject("detalle", det);
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/atras/{num}/{tipo}")
	@ResponseBody
	public ModelAndView atras(@PathVariable("num") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		List<DetalleOCPed> det = detOrdenCompraPedService.findByDetalle(Integer.parseInt(numero), tipo);
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/detalleE");
		modelAndView.addObject("detalle", det);
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/atrasP/{num}/{tipo}")
	@ResponseBody
	public ModelAndView atrasP(@PathVariable("num") String numero, @PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/pedidos/ordenE");
		modelAndView.addObject("pedidos", pedService.findEnEspera());
		return modelAndView;
	}

	@RequestMapping(value = "/pedidos/fprod", method = RequestMethod.POST)
	public ModelAndView finProducto(@RequestParam Map<String, String> requestParams) throws Exception {
		String prod = requestParams.get("pt");
		String tipo = requestParams.get("tipo");
		String numero = requestParams.get("num");
		String lineas = requestParams.get("lineas");
		int total, total2, cajas = 0, display = 0, sumacj = 0, sumadp = 0, sumaun = 0;
		List<Um> unidad = umService.findById(prod);
		for (int i = 0; i < unidad.size(); i++) {
			if (unidad.get(i).getOrg().equals("CJ")) {
				cajas = unidad.get(i).getConv();
			} else if (unidad.get(i).getOrg().equals("DP")) {
				display = unidad.get(i).getConv();
			} else {
				cajas = 0;
				display = 0;
			}
		}

		for (int i = 1; i <= Integer.parseInt(lineas); i++) {

			String lote = requestParams.get("lote" + i);
			String fecha = requestParams.get("fecha" + i);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dateFormat.parse(fecha);
			String cj = requestParams.get("cj" + i);
			String dp = requestParams.get("dp" + i);
			String un = requestParams.get("un" + i);

			if (unidad == null) {
				total = Integer.parseInt(un);
			} else {
				cj = String.valueOf(Integer.parseInt(cj) * cajas);
				dp = String.valueOf(Integer.parseInt(dp) * display);
				total = Integer.parseInt(cj) + Integer.parseInt(dp) + Integer.parseInt(un);
			}

			LoteEnt lt = new LoteEnt();

			lt.setLote(lote);
			lt.setNumero(numero);
			lt.setTipo(tipo);
			lt.setCodigo(prod);
			lt.setCantidad(Double.valueOf(total));
			lt.setVencimiento(date);
			loteEntService.saveEntLote(lt);

			sumacj += Integer.parseInt(cj);
			sumadp += Integer.parseInt(dp);
			sumaun += Integer.parseInt(un);
		}

		DetalleOCPed pedido = detOrdenCompraPedService.findByPed(Integer.parseInt(numero), tipo, prod);

		total2 = sumacj + sumadp + sumaun;

		if (pedido.getCantidad() > total2) {
			pedido.setEstado("VF");
		} else {
			pedido.setEstado("V");
		}

		pedido.setCantidadRec(total2);
		detOrdenCompraPedService.saveDetOCPed(pedido);
		ModelAndView modelAndView = new ModelAndView();
		List<DetalleOCPed> det = detOrdenCompraPedService.findByDetalle(Integer.parseInt(numero), tipo);
		Und un = new Und();
		un.setNumero(Integer.parseInt(numero));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/detalleE");
		modelAndView.addObject("detalle", det);
		modelAndView.addObject("und", un);
		return modelAndView;
	}

	@GetMapping("/pedidos/nova/{prod}/{num}/{tipo}")
	@ResponseBody
	public ModelAndView nova(@PathVariable("prod") String prod, @PathVariable("num") String num,
			@PathVariable("tipo") String tipo) {
		ModelAndView modelAndView = new ModelAndView();
		DetalleOCPed pedido = detOrdenCompraPedService.findByPed(Integer.parseInt(num), tipo, prod);
		pedido.setEstado("NL");
		pedido.setCantidadRec(0);
		detOrdenCompraPedService.saveDetOCPed(pedido);
		List<DetalleOCPed> det = detOrdenCompraPedService.findByDetalle(Integer.parseInt(num), tipo);
		Und un = new Und();
		un.setNumero(Integer.parseInt(num));
		un.setTipo(tipo);
		modelAndView.setViewName("/pedidos/detalleE");
		modelAndView.addObject("detalle", det);
		modelAndView.addObject("und", un);
		return modelAndView;
	}

}
