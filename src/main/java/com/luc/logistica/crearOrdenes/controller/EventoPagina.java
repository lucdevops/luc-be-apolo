
package com.luc.logistica.crearOrdenes.controller;

import java.io.IOException;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.luc.logistica.crearOrdenes.model.EncabezadoOC;

public class EventoPagina implements IEventHandler {

    private final Document documento;
    private final EncabezadoOC pedido;
    private final String cliente;
    private final String fecha;
    private final String digito;
    private final String direccion;
    private final String telefono;
    private final String ciudad;
    private final String numFac;
    private final String vendedor;

    public EventoPagina(Document doc, EncabezadoOC ped, String cli, String fec, String dig, String dir, String tel,
            String city, String num, String ven) {
        documento = doc;
        pedido = ped;
        cliente = cli;
        fecha = fec;
        digito = dig;
        telefono = tel;
        direccion = dir;
        ciudad = city;
        numFac = num;
        vendedor = ven;
    }

    /**
     * Crea el rectangulo donde pondremos el encabezado
     * 
     * @param docEvent Evento de documento
     * @return Area donde colocaremos el encabezado
     */
    private Rectangle crearRectanguloEncabezado(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();

        float xEncabezado = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        // float yEncabezado = pdfDoc.getDefaultPageSize().getTop() -
        // documento.getTopMargin();
        float yEncabezado = 590;
        float anchoEncabezado = page.getPageSize().getWidth() - 72;
        float altoEncabezado = 150;

        Rectangle rectanguloEncabezado = new Rectangle(xEncabezado, yEncabezado, anchoEncabezado, altoEncabezado);

        return rectanguloEncabezado;
    }

    /**
     * Crea el rectangulo donde pondremos el pie de pagina
     * 
     * @param docEvent Evento del documento
     * @return Area donde colocaremos el pie de pagina
     */
    /*private Rectangle crearRectanguloPie(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();

        float xPie = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yPie = pdfDoc.getDefaultPageSize().getBottom();
        float anchoPie = page.getPageSize().getWidth() - 72;
        float altoPie = 50F;

        Rectangle rectanguloPie = new Rectangle(xPie, yPie, anchoPie, altoPie);

        return rectanguloPie;
    }*/

    /**
     * Crea la tabla que contendra el mensaje del encabezado
     * 
     * @param mensaje Mensaje que desplegaremos
     * @return Tabla con el mensaje de encabezado
     * @throws IOException
     */
    private Table crearTablaEncabezado() throws IOException {

        java.text.DecimalFormat df = new java.text.DecimalFormat(
                "##########################################################");
        float[] anchos = { 10, 55, 35 };
        Table tablaDatos = new Table(anchos);
        tablaDatos.setWidth(550);
        PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);

        // tablaEncabezado.addCell(mensaje);
        Cell cell;
        cell = new Cell(1,2).add(new Paragraph("LINEA UNO COMERCIALIZADORA LTDA.").setFont(font).setFontSize(11));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph("Factura de venta").setFont(font).setFontSize(10));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell(1,2).add(new Paragraph("NIT. 800.113.803.5 IVA RÉGIMEN COMÚN").setFontSize(9));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph(numFac).setFontSize(10));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell(1,2).add(new Paragraph("CL 39 15 17 CALI TEL.:8897172").setFontSize(9));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        tablaDatos.addCell(new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER));

        cell = new Cell(1, 2).add(new Paragraph(cliente).setFont(font).setFontSize(10));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER));
        cell = new Cell().add(new Paragraph("FECHA: MM/DD/AAAA  " + fecha).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER));
        if (digito.equals("")) {
            cell = new Cell().add(new Paragraph("CC No.: ").setFont(font).setFontSize(8));
            tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
            cell = new Cell().add(new Paragraph(df.format(pedido.getTercero().getNitReal())).setFontSize(8));
            tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        }else{
            cell = new Cell().add(new Paragraph("NIT No.: ").setFont(font).setFontSize(8));
            tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
            cell = new Cell().add(new Paragraph(df.format(pedido.getTercero().getNitReal()) + "-" + digito).setFontSize(8));
            tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        }           
        cell = new Cell().add(new Paragraph("VENDEDOR: " + vendedor).setFont(font).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph("DIRECCION: ").setFont(font).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph(direccion).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        tablaDatos.addCell((new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER)));
        cell = new Cell().add(new Paragraph("TELEFONO: ").setFont(font).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph(telefono).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        tablaDatos.addCell((new Cell().add(new Paragraph("OC: " + pedido.getNotas()).setFontSize(9)).setBorder(Border.NO_BORDER)).setPadding(0));
        cell = new Cell().add(new Paragraph("CIUDAD: ").setFont(font).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        cell = new Cell().add(new Paragraph(ciudad).setFontSize(8));
        tablaDatos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
        tablaDatos.addCell((new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER)));
        return tablaDatos;
    }

    /**
     * Crea la tabla de pie de pagina, con el numero de pagina
     * @param docEvent Evento del documento
     * @return Pie de pagina con el numero de pagina
     */
    /*private Table crearTablaPie(PdfDocumentEvent docEvent) {
        PdfPage page = docEvent.getPage();
        float[] anchos = { 1F };
        Table tablaPie = new Table(anchos);
        tablaPie.setWidth(527F);
        Integer pageNum = docEvent.getDocument().getPageNumber(page);

        tablaPie.addCell("Pagina " + pageNum);

        return tablaPie;
    }*/

    /**
     * Manejador del evento de cambio de pagina, agrega el encabezado y pie de
     * pagina
     * 
     * @param event Evento de pagina
     */
    @Override
    public void handleEvent(Event event) {
        PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
        PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);

        Table tablaEncabezado = null;
        try {
            tablaEncabezado = this.crearTablaEncabezado();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Rectangle rectanguloEncabezado = this.crearRectanguloEncabezado(docEvent);
        Canvas canvasEncabezado = new Canvas(canvas, pdfDoc, rectanguloEncabezado);
        canvasEncabezado.add(tablaEncabezado);

        /*
         * Table tablaNumeracion = this.crearTablaPie(docEvent); Rectangle rectanguloPie
         * = this.crearRectanguloPie(docEvent); Canvas canvasPie = new Canvas(canvas,
         * pdfDoc, rectanguloPie); canvasPie.add(tablaNumeracion);
         */
    }
}