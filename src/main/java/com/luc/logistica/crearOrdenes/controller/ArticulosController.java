package com.luc.logistica.crearOrdenes.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.luc.logistica.crearOrdenes.model.Grupo;
import com.luc.logistica.crearOrdenes.model.Marca;
import com.luc.logistica.crearOrdenes.model.Referencias;
import com.luc.logistica.crearOrdenes.service.GrupoService;
import com.luc.logistica.crearOrdenes.service.MarcaService;
import com.luc.logistica.crearOrdenes.service.ReferenciasService;

import javax.validation.Valid;

@Controller
public class ArticulosController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private GrupoService grupoService;
	@Autowired
	private ReferenciasService refeService;
	
	@GetMapping("/articulos")
	public ModelAndView index(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/articulos/listado");
		modelAndView.addObject("referencias",refeService.findAll());
		return modelAndView;
	}
	
	@PostMapping("/articulos/buscar")
	public ModelAndView buscar(@RequestParam("texto") String texto){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/articulos/listado");
		//modelAndView.addObject("articulos",articuloService.findAll());
		if(texto.equals("")) {
			modelAndView.addObject("referencias",refeService.findAll());
		}else {
			modelAndView.addObject("referencias",refeService.findByTexto(texto));
		}
		return modelAndView;
	}
	
	@GetMapping("/articulos/crear")
	public ModelAndView formularioCliente(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/articulos/crear");
		modelAndView.addObject("articulo",new Referencias());
		modelAndView.addObject("marcas",marcaService.findAll());
		modelAndView.addObject("grupos",grupoService.findAll());
		return modelAndView;
	}
	
	@PostMapping("/articulos/crear")
	public String crearCliente(@Valid @ModelAttribute("articulo") Referencias articulo, @RequestParam("marca") Marca marca, @RequestParam("grupo") Grupo grupo, RedirectAttributes redirectAttributes){
		Marca reg = marcaService.findById(marca.getId());
		Grupo gru = grupoService.findByGru(grupo.getGrupo());
		String clase = "";

		if (reg.getId().equals("01") || reg.getId().equals("02") || reg.getId().equals("09") || reg.getId().equals("10")) {
			clase = "DIS";
		} else {
			clase = "REP";
		}
		
		articulo.setGenerico("123");
		articulo.setContable(2);
		articulo.setClase(clase);
		articulo.setGrupo(gru.getGrupo());
		articulo.setSubgrupo("1");
		articulo.setMarca(reg.getId());
		articulo.setNit(Double.parseDouble(reg.getNit()));
		articulo.setAlto(1);
		articulo.setAncho(1);
		articulo.setInventario(1);

		Referencias nuevo = refeService.saveRefe(articulo);
		
		if(nuevo == null) {
			redirectAttributes.addFlashAttribute("message", "Error al crear el articulo");
		}else {		
			redirectAttributes.addFlashAttribute("message", "Articulo " + nuevo.getDescripcion() + " creado con exito");
		}
		return "redirect:/informe";
	}
	
}
