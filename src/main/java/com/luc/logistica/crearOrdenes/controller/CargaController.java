package com.luc.logistica.crearOrdenes.controller;

import java.awt.print.PrinterException;
//import java.awt.print.PrinterJob;
import java.io.BufferedReader;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.validation.Valid;

/*import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.renderer.CellRenderer;
import com.itextpdf.layout.renderer.DrawContext;*/

import com.luc.logistica.crearOrdenes.model.CenData;
import com.luc.logistica.crearOrdenes.model.CotizacionVista;
import com.luc.logistica.crearOrdenes.model.DetalleOC;
import com.luc.logistica.crearOrdenes.model.DetalleOCPed;
import com.luc.logistica.crearOrdenes.model.DetalleOCPedHis;
import com.luc.logistica.crearOrdenes.model.DocKey;
import com.luc.logistica.crearOrdenes.model.EncabezadoOC;
import com.luc.logistica.crearOrdenes.model.Impuesto;
import com.luc.logistica.crearOrdenes.model.Lote;
import com.luc.logistica.crearOrdenes.model.LoteEnt;
import com.luc.logistica.crearOrdenes.model.Marca;
import com.luc.logistica.crearOrdenes.model.Ped;
import com.luc.logistica.crearOrdenes.model.PedHis;
import com.luc.logistica.crearOrdenes.model.PedKey;
import com.luc.logistica.crearOrdenes.model.Planilla;
import com.luc.logistica.crearOrdenes.model.PlanillaLin;
import com.luc.logistica.crearOrdenes.model.Precio;
import com.luc.logistica.crearOrdenes.model.Proveedor;
import com.luc.logistica.crearOrdenes.model.Referencias;
import com.luc.logistica.crearOrdenes.model.ReferenciasSto;
import com.luc.logistica.crearOrdenes.model.StoKey;
import com.luc.logistica.crearOrdenes.model.Terceros;
import com.luc.logistica.crearOrdenes.service.CenService;
import com.luc.logistica.crearOrdenes.service.DetalleOCPedHisService;
import com.luc.logistica.crearOrdenes.service.DetalleOCPedService;
import com.luc.logistica.crearOrdenes.service.LoteEntService;
import com.luc.logistica.crearOrdenes.service.LoteService;
import com.luc.logistica.crearOrdenes.service.MarcaService;
import com.luc.logistica.crearOrdenes.service.OrdenCompraService;
import com.luc.logistica.crearOrdenes.service.PedHisService;
import com.luc.logistica.crearOrdenes.service.PedService;
import com.luc.logistica.crearOrdenes.service.PlanillaLinService;
import com.luc.logistica.crearOrdenes.service.PlanillaService;
import com.luc.logistica.crearOrdenes.service.ProveedorService;
import com.luc.logistica.crearOrdenes.service.RefStoService;
import com.luc.logistica.crearOrdenes.service.ReferenciasService;
import com.luc.logistica.crearOrdenes.service.TercerosService;

//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.printing.PDFPageable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
//import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CargaController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProveedorService proveedorService;
    @Autowired
    private DetalleOCPedService docPedService;
    @Autowired
    private DetalleOCPedHisService docPedHisService;
    @Autowired
    private RefStoService refService;
    @Autowired
    private LoteService loteService;
    @Autowired
    private OrdenCompraService ocService;
    @Autowired
    private OrdenCompraService ordenCompraService;
    @Autowired
    private PedService pedService;
    @Autowired
    private PedHisService pedHisService;
    @Autowired
    private ReferenciasService referenciasService;
    @Autowired
    private TercerosService tercerosService;
    @Autowired
    private CenService cenService;
    @Autowired
    private MarcaService marcaService;
    @Autowired
    private LoteEntService loteEntService;
    @Autowired
    private PlanillaService planillaService;
    @Autowired
    private PlanillaLinService planillaLinService;

    private StringBuffer buf;
    @Value("${contapyme.host}")
    private String ip;
    @Value("${contapyme.port}")
    private String puerto;
    private Random rnd = new Random();
    private String url = "";
    private String consulta = "";
    @Value("${contapyme.user}")
    private String usuario;
    @Value("${contapyme.password}")
    private String clave;
    @Value("${contapyme.app}")
    private String app;
    private String key = "";
    private RestTemplate restTemplate = new RestTemplate();

    /* CARGAR ORDENES TXT A COT */
    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("proveedor") Proveedor prov,
            RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            autenticaAgente();

            /* Datos del proveedor para el cual se van a cargar las ordenes */
            Proveedor completo = proveedorService.findById(prov.getId());

            String ordenActual = "", str = "", productos = "", separador = "", qty;
            String data[] = null;
            Terceros clienteCarga = new Terceros();
            int lineaArchivo = 0, errores = 0;
            double bruto, neto, descuento, total, precio;
            buf = new StringBuffer();
            InputStream st = file.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(st));
            List<String> articulo = new ArrayList<String>();

            if (st != null) {
                while ((str = reader.readLine()) != null) {
                    lineaArchivo++;
                    data = str.split(";");
                    if (data.length == 9) {
                        if (data[0].equals(ordenActual) == false && ordenActual.equals("") == false) {
                            if (errores == 0) {

                                Set<String> quipu = new HashSet<String>(articulo);
                                for (String key : quipu) {
                                    if ((Collections.frequency(articulo, key)) > 1) {
                                        buf.append("<p class='error'>Referencia " + key + " repetida</p>");
                                        errores++;
                                    }
                                }

                                if (errores == 0) {
                                    crearCotizacion(completo, ordenActual, clienteCarga, productos);
                                    articulo.clear();
                                    articulo.add(data[3]);
                                }
                            } else {
                                buf.append("<p>=======ERROR=======" + "</p>");
                            }
                            articulo.clear();
                            articulo.add(data[3]);
                            productos = "";
                            errores = 0;
                        } else {
                            articulo.add(data[3]);
                        }
                        if (!ordenActual.equals(data[0])) {
                            ordenActual = data[0];
                            buf.append("<p>Procesando orden: " + ordenActual + "</p>");
                        }

                        ordenActual = data[0];
                        qty = data[4];
                        bruto = Double.parseDouble(data[7]);
                        neto = Double.parseDouble(data[8]);
                        clienteCarga = tercerosService.findByEan(data[5]);
                        Referencias articuloCarga = referenciasService.findByRefe(data[3]);

                        if (clienteCarga != null && !clienteCarga.getEan().equals("")) {
                            clienteCarga = obtenerVendedor(clienteCarga);

                            if (!clienteCarga.getListaPrecio().equals("") && clienteCarga.getVendedor() != 0.0) {
                                if (articuloCarga != null) {
                                    Precio prec = obtenerPrecio(completo.getBodega(), clienteCarga.getListaPrecio(),
                                            clienteCarga.getCodigoLuc(), articuloCarga.getCodigo());
                                    if (prec.getPrecio() > 0) {
                                        double netoCP = prec.getPrecio() * (1 - (prec.getDescuento() / 100));

                                        if (!prov.getNombre().equals("FRUTALIA")) {
                                            if ((neto - netoCP) > 100.00 || (neto - netoCP) < -100.00) {
                                                buf.append("<p class='error'>Articulo " + articuloCarga.getCodigo()
                                                        + " tiene diferencia en el descuento % </p>");
                                                errores++;
                                            }

                                            if (bruto != prec.getPrecio()) {
                                                buf.append("<p class='error'>Articulo " + articuloCarga.getCodigo()
                                                        + " tiene diferencia en el precio $, Precio: " + bruto
                                                        + "</p>");
                                                errores++;
                                            }
                                        } else {
                                            if (bruto != prec.getPrecio()) {
                                                buf.append("<p class='error'>Articulo " + articuloCarga.getCodigo()
                                                        + " tiene diferencia en el precio $, Precio: " + bruto
                                                        + "</p>");
                                                errores++;
                                            }
                                        }

                                        // descuento = (1 - (neto/bruto)) * 100;
                                        // descuento = ((bruto - neto)/bruto) * 100;

                                        if (productos.equals("")) {
                                            separador = "";
                                        } else {
                                            separador = ",";
                                        }

                                        String p = "";

                                        if (articuloCarga.getMarca().equals("01")
                                                || articuloCarga.getMarca().equals("02")
                                                || articuloCarga.getMarca().equals("09")
                                                || articuloCarga.getMarca().equals("10")) {
                                            p = "1";
                                        } else if (articuloCarga.getMarca().equals("03")) {
                                            p = "2";
                                        } else if (articuloCarga.getMarca().equals("04")) {
                                            p = "10";
                                        } else if (articuloCarga.getMarca().equals("05")) {
                                            p = "6";
                                        } else if (articuloCarga.getMarca().equals("06")) {
                                            p = "3";
                                        } else if (articuloCarga.getMarca().equals("08")) {
                                            p = "8";
                                        } else if (articuloCarga.getMarca().equals("14")) {
                                            p = "5";
                                        } else if (articuloCarga.getMarca().equals("15")) {
                                            p = "4";
                                        } else if (articuloCarga.getMarca().equals("16")) {
                                            p = "9";
                                        } else if (articuloCarga.getMarca().equals("18")) {
                                            p = "13";
                                        } else if (articuloCarga.getMarca().equals("20")) {
                                            p = "12";
                                        } else if (articuloCarga.getMarca().equals("21")) {
                                            p = "15";
                                        } else if (articuloCarga.getMarca().equals("22")) {
                                            p = "16";
                                        }

                                        /*
                                         * if (!prov.getId().equals(p)) { buf.append(
                                         * "<p class='error'>Proveedor diferente al de las referencias</p>"); errores++;
                                         * }
                                         */

                                        precio = (Double.parseDouble(qty)) * prec.getPrecio();
                                        descuento = prec.getDescuento() / 100;
                                        total = precio - (precio * descuento);
                                        DecimalFormat df = new DecimalFormat("0");
                                        String vNeto = df.format(total);

                                        productos = productos + separador + "{\"iinventario\":" + completo.getBodega()
                                                + ",\"irecurso\":\"" + articuloCarga.getCodigo()
                                                + "\",\"itiporec\":\"\",\"qrecurso\":" + qty + ",\"mprecio\":\""
                                                + prec.getPrecio() + "\",\"qporcdescuento\":\"" + prec.getDescuento()
                                                + "\",\"qporciva\":\"" + prec.getImpuesto() + "\",\"mvrtotal\":\""
                                                + vNeto
                                                + "\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"\",\"valor2\":\"\",\"valor3\":\"\",\"valor4\":\"\"}";
                                    } else {
                                        buf.append("<p>-Articulo " + articuloCarga.getCodigo()
                                                + " sin precio para cliente " + clienteCarga.getCodigoLuc()
                                                + " y lista " + clienteCarga.getListaPrecio() + "</p>");
                                        errores++;
                                    }
                                } else {
                                    buf.append("<p>-No se encontro el Articulo " + data[3] + "</p>");
                                    errores++;
                                }
                            } else {
                                buf.append("<p>-No se encontro el vendedor para el cliente o no tiene lista de precio "
                                        + clienteCarga.getCodigoLuc() + "</p>");
                                errores++;
                            }
                        } else {
                            buf.append("<p>-No se encontro el ID del cliente " + data[5] + "</p>");
                            errores++;
                        }
                    } else {
                        buf.append("<p>*************Linea " + lineaArchivo
                                + " no contiene la cantidad de campos correcta*****************" + "</p>");
                        errores++;
                    }
                }
            }

            if (!ordenActual.equals("") && errores == 0) {

                Set<String> quipu = new HashSet<String>(articulo);
                for (String key : quipu) {
                    if ((Collections.frequency(articulo, key)) > 1) {
                        buf.append("<p class='error'>Referencia " + key + " repetida</p>");
                        errores++;
                    }
                }

                if (errores == 0) {
                    crearCotizacion(prov, ordenActual, clienteCarga, productos);
                }
            }

            buf.append("</p>" + "Proceso finalizado" + "</p>");
            redirectAttributes.addFlashAttribute("message", buf.toString());
            redirectAttributes.addFlashAttribute("conecto", key);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/informe";
    }

    /* LIBERAR COTIZACIONES */
    @PostMapping("/libera")
    public String libera(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerCotizacionLiberar();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR SALIDAS */
    @PostMapping("/liberaSalida")
    public String liberaSalida(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerSalidaCP();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR AVERIAS */
    @PostMapping("/liberaTraslado")
    public String liberaTraslado(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerTrasladoCP();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR RECEPCION */
    @PostMapping("/liberaRecepcion")
    public String liberaRecepcion(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerRecepcionLiberar();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR ORDEN DE COMPRA */
    @PostMapping("/liberaOC")
    public String liberaOC(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerComprasLiberar();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR DEVOLUCION */
    @PostMapping("/liberaDev")
    public String liberaDev(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerDevLiberar();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /* LIBERAR DEVOLUCION */
    @PostMapping("/liberaAjuste")
    public String liberaAjuste(RedirectAttributes redirectAttributes) {
        buf = new StringBuffer();
        autenticaAgente();
        obtenerAjusteCP();
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    public boolean verificarFac(String cliente, String oc, String prov, String area, String tipoDoc) {

        autenticaAgente();

        areaTrabajo(Integer.parseInt(area));
        boolean existe = false;

        try {
            consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\"},\"camposderetorno\":[\"svaloradic3\",\"svaloradic2\", \"tdetalle\", \"fcreacion\", \"init\", \"itdsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ING1\"]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONArray datos = (JSONArray) respuesta.get("datos");
            String proveedor = "", orden = "", tercero = "", doc = "";

            for (int i = 0; i < datos.size(); i++) {
                JSONObject arreglo = (JSONObject) datos.get(i);
                proveedor = String.valueOf(arreglo.get("svaloradic3"));
                orden = String.valueOf(arreglo.get("svaloradic2"));
                tercero = String.valueOf(arreglo.get("init"));
                doc = String.valueOf(arreglo.get("itdsop"));
                if (prov.equals(proveedor) && oc.equals(orden) && cliente.equals(tercero) && tipoDoc.equals(doc)) {
                    existe = true;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return existe;

    }

    /* FACTURAR */
    @PostMapping("/factura")
    public String factura(RedirectAttributes redirectAttributes)
            throws UnsupportedEncodingException, PrinterException, ParseException {
        buf = new StringBuffer();
        ArrayList<EncabezadoOC> facturar = (ArrayList<EncabezadoOC>) ordenCompraService.findFinalizado();
        autenticaAgente();

        for (int i = 0; i < facturar.size(); i++) {
            CenData cen = cenService.findByOp(facturar.get(i).getDocumento());
            Proveedor vd = proveedorService.findById(cen.getProveedor());

            if (vd.getDocumento().equals("420")) {
                crearPedido(facturar.get(i), vd, cen);
            } else {
                /*
                 * boolean exist = verificarFac(facturar.get(i).getTercero().getCodigoLuc(),
                 * cen.getId(), vd.getNombre(), vd.getTipo(), vd.getDocumento());
                 */

                /*
                 * if (exist) { buf.append("<p class='error'>" + "Factura del cliente: " +
                 * facturar.get(i).getTercero().getNombres() + " y del proveedor " +
                 * vd.getNombre() + " ya ha sido creada" + "</p>"); } else {
                 */

                if (vd.getDocumento().equals("10")) {
                    boolean exist = verificarFac(facturar.get(i).getTercero().getCodigoLuc(), cen.getId(),
                            vd.getNombre(), vd.getTipo(), "701");
                    if (exist) {
                        buf.append("<p class='error'>" + "Factura del cliente: "
                                + facturar.get(i).getTercero().getNombres() + " y del proveedor " + vd.getNombre()
                                + " ya ha sido creada" + "</p>");
                    } else {
                        crearFacturaElec(facturar.get(i), vd, cen);
                    }
                } else {
                    boolean exist = verificarFac(facturar.get(i).getTercero().getCodigoLuc(), cen.getId(),
                            vd.getNombre(), vd.getTipo(), vd.getDocumento());
                    if (exist) {
                        buf.append("<p class='error'>" + "Factura del cliente: "
                                + facturar.get(i).getTercero().getNombres() + " y del proveedor " + vd.getNombre()
                                + " ya ha sido creada" + "</p>");
                    } else {
                        crearFactura(facturar.get(i), vd, cen);
                    }
                }

                // }
            }
        }
        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        return "redirect:/informe";
    }

    /*
     * CREAR RECEPCION O COMPROBANTE DE AJUSTE EN CONTAPYME CUANDO SE RECIBE UN
     * PROVEEDOR
     */
    @PostMapping("/pedidos/fin")
    public String fin(@Valid @RequestParam("num") Integer numero, @RequestParam("tipo") String tipo,
            RedirectAttributes redirectAttributes) throws UnsupportedEncodingException {
        buf = new StringBuffer();
        int errores = 0;
        String jsonPL[] = new String[2];
        autenticaAgente();
        List<DetalleOCPed> det = docPedService.findByDetalle(numero, tipo);
        List<LoteEnt> lot = loteEntService.findByOrc(Integer.toString(numero));

        for (int i = 0; i < det.size(); i++) {
            if (det.get(i).getCantidadRec() == null) {
                errores++;
            }
        }
        Ped encabezado = pedService.findbyPed(numero, tipo);

        buf.append("</p>" + "Proceso finalizado" + "</p>");
        redirectAttributes.addFlashAttribute("message", buf.toString());
        if (errores == 0) {
            jsonPL = jsonProdLote(det, encabezado, lot);
            String[] resultados = crearRecepcionEntrada(encabezado, det, jsonPL[0], jsonPL[1]);
            redirectAttributes.addAttribute("numero", numero);
            redirectAttributes.addAttribute("tipo", tipo);
            if (resultados[0].equals("true") && resultados[1].equals("true")) {
                return "redirect:/pedidos/fin/{numero}/{tipo} ";
            } else if (resultados[0].equals("false") && resultados[1].equals("true")) {
                return "redirect:/pedidos/errorRecep/{numero}/{tipo}";
            } else if (resultados[0].equals("true") && resultados[1].equals("false")) {
                return "redirect:/pedidos/errorCom/{numero}/{tipo}";
            }

            // return "redirect:/recepcion/pedidos";
        } else {
            redirectAttributes.addAttribute("numero", numero);
            redirectAttributes.addAttribute("tipo", tipo);
            return "redirect:/pedidos/error/{numero}/{tipo}";
        }
        return tipo;

    }

    /* CREAR RECEPCION EN CONTAPYME */
    private String[] crearRecepcionEntrada(Ped encabezado, List<DetalleOCPed> det, String prod, String lotes) {

        String[] resultados = new String[2];
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        Proveedor vd = proveedorService.findById(encabezado.getTercero().getVd());
        String proveedor = String.valueOf(encabezado.getTercero().getNit());
        String num = proveedor.split("E")[0];
        String num2 = num.substring(0, 1);
        String num3 = num.substring(2);
        proveedor = num2 + num3;

        try {
            consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ORD6\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \""
                    + vd.getTipo() + "\",\"tdetalle\": \"ENTRADA DE " + encabezado.getTercero().getNombres()
                    + " - FAC %23" + encabezado.getNotas()
                    + "\",\"itdsop\": \"520\",\"inumsop\": \"0\",\"snumsop\": \"<AUTO>\",\"fsoport\": \"" + fecha
                    + "\",\"iclasifop\": \"1\",\"iprocess\": \"0\",\"banulada\": \"F\",\"svaloradic1\": \"LI\"},\"datosprincipales\": {\"init\": \""
                    + proveedor + "\",\"inittransportador\": \"\",\"sobserv\": \"\",\"iinventario\": \""
                    + vd.getBodega()
                    + "\",\"ilistaprecios\": \"0\",\"blistaconiva\": \"F\",\"blistaconprecios\": \"T\",\"bregvrunit\": \"F\",\"bregvrtotal\": \"T\",\"ireferencia\":\"\",\"bcerrarref\": \"F\",\"breferenciabasadaencompras\": \"F\",\"qregseriesproductos\": \"1\"},\"listaproductos\": ["
                    + prod + "],\"series\":[" + lotes + "]}}";

            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject enc = (JSONObject) arr.get("encabezado");
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            // String operacion = String.valueOf( datos.get("inumoper"));
            String resul = String.valueOf(enc.get("resultado"));
            String ref = String.valueOf(datos.get("snumsop"));
            // Integer area = Integer.parseInt(vd.getTipo());
            // procesarRecepcion(operacion, area);
            boolean proceso = obtenerRecepcionLiberar();
            boolean compra = crearFacturaCompra(ref, encabezado, prod, lotes);

            resultados[0] = String.valueOf(proceso);
            resultados[1] = String.valueOf(compra);

            if (resul.equals("true")) {
                encabezado.setEstado("FI");
                pedService.savePed(encabezado);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultados;
    }

    /* CREAR JSON DE LOS PRODUCTOS Y LOTES */
    public String[] jsonProdLote(List<DetalleOCPed> det, Ped encabezado, List<LoteEnt> lot)
            throws UnsupportedEncodingException {

        String separadorP = "", separadorL = "", productosRep = "", lotesRep = "";
        String retorno[] = new String[2];
        SimpleDateFormat mdyFormat = new SimpleDateFormat("dd/MM/yyyy");
        Proveedor vd = proveedorService.findById(encabezado.getTercero().getVd());

        for (int i = 0; i < det.size(); i++) {
            if (productosRep.equals("")) {
                separadorP = "";
            } else {
                separadorP = ",";
            }
            if (det.get(i).getCantidadRec() > 0) {
                productosRep = productosRep + separadorP + "{\"iinventario\":\"" + vd.getBodega() + "\",\"irecurso\":\""
                        + det.get(i).getRefe().getCodigo() + "\",\"qrecurso\":\"" + det.get(i).getCantidadRec() + "\"}";
            }

        }

        for (int i = 0; i < lot.size(); i++) {
            if (lotesRep.equals("")) {
                separadorL = "";
            } else {
                separadorL = ",";
            }
            String fecha = URLEncoder.encode(mdyFormat.format(lot.get(i).getVencimiento()), "UTF-8");
            lotesRep = lotesRep + separadorL + "{\"iinventario\":\"" + vd.getBodega() + "\",\"irecurso\":\""
                    + lot.get(i).getCodigo() + "\",\"iserie\":\"" + lot.get(i).getLote() + "\",\"qrecurso\":\""
                    + lot.get(i).getCantidad() + "\",\"fecha1\":\"" + fecha + "\",\"fecha2\":\"" + fecha + "\"}";
        }

        retorno[0] = productosRep;
        retorno[1] = lotesRep;

        return retorno;
    }

    private boolean crearFacturaCompra(String ref, Ped encabezado, String prod, String lotes) {

        boolean proceso = false;
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        Proveedor vd = proveedorService.findById(encabezado.getTercero().getVd());
        String proveedor = String.valueOf(encabezado.getTercero().getNit());
        String num = proveedor.split("E")[0];
        String num2 = num.substring(0, 1);
        String num3 = num.substring(2);
        proveedor = num2 + num3;

        try {
            consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"COM1\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \""
                    + vd.getTipo() + "\",\"tdetalle\": \"\",\"itdsop\": \"110\",\"snumsop\": \"FC-"
                    + encabezado.getNotas() + "\",\"fsoport\": \"" + fecha
                    + "\",\"iclasifop\": \"1\",\"iprocess\": \"0\",\"banulada\": \"F\",\"svaloradic1\": \"FI\"},\"datosprincipales\": {\"init\": \""
                    + proveedor + "\",\"iinventario\": \"" + vd.getBodega()
                    + "\",\"bshowsupportinfo\":\"F\",\"bregvrunit\":\"T\",\"bshowcntfields\":\"T\",\"bfactporordcompra\":\"F\",\"bdistribuirgastos\":\"F\",\"icriteriodistribucion\":\"0\",\"busarotramoneda\":\"F\",\"imonedaimpresion\":\"\",\"mtasacambio\":\"0\",\"ireferencia\":\""
                    + ref
                    + "\",\"bcostoconiva\":\"F\",\"qreggastos\":\"0\",\"qregreferencias\":\"1\",\"qregconcdescuento\":\"0\"},\"listaproductos\": ["
                    + prod + "],\"referencias\":[{\"ireferencia\":\"" + ref + "\",\"bcerrar\":\"T\"}],\"series\":["
                    + lotes + "]}}";

            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject enc = (JSONObject) arr.get("encabezado");
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String oper = String.valueOf(datos.get("inumoper"));
            String resul = String.valueOf(enc.get("resultado"));
            proceso = Boolean.parseBoolean(resul);

            if (resul.equals("true")) {
                encabezado.setEstado("FI");
                pedService.savePed(encabezado);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return proceso;
    }

    /* CONEXION CON EL API */
    private void autenticaAgente() {

        log.debug("Previo Autentica");

        try {
            consulta = "{\"email\":\"" + usuario + "\",\"password\":\"" + clave
                    + "\",\"idmaquina\":\"537.22_136301143299\"}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TBasicoGeneral/GetAuth/" + consulta + "/" + app + "/"
                    + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            key = String.valueOf(datos.get("keyagente"));
            log.debug("Autentica key:" + key);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JSON PARA CAMBIAR EL AREA DE TRABAJO */
    private boolean areaTrabajo(int a) {

        autenticaAgente();

        try {
            consulta = "{\"iemp\":\"" + a + "\"}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TBasicoGeneral/SetDatosTrabajo/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String cambio = String.valueOf(datos.get("modifico"));
            if (cambio.equals("true")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA TRAER DATOS DE LAS COMPRAS (RECEPCION DE MATERIALES) */
    private boolean obtenerRecepcionLiberar() {
        boolean proceso = false;

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {
                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\" },\"camposderetorno\":[\"svaloradic1\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ORD6\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado = "";
                String operacion = "";
                String procesada = "";
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    if (procesada != null && procesada.equals("0")) {
                        if (estado != null && estado.equals("LI")) {
                            proceso = obtenerRecepcion(operacion);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return proceso;
    }

    /* JSON PARA TRAER DATOS DE LAS COMPRAS (RECEPCION DE MATERIALES) */
    private void obtenerComprasLiberar() {

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {
                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\" },\"camposderetorno\":[\"svaloradic1\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ORD5\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado = "";
                String operacion = "";
                String procesada = "";
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    if (procesada != null && procesada.equals("0")) {
                        if (estado != null && estado.equals("LI")) {
                            obtenerCompra(operacion);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JSON PARA TRAER DATOS DE LAS DEVOLUCIONES */
    private void obtenerDevLiberar() {

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {
                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\"},\"camposderetorno\":[\"svaloradic1\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"COM4\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado, operacion, procesada, proveedor;
                Integer consecutivo = 0;
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    proveedor = String.valueOf(arreglo.get("snumsop"));

                    if (procesada != null && procesada.equals("0")) {

                        if (estado != null && (estado.equals("NOTA PARCIAL O INCOMPLETA")
                                || estado.equals("NOTA RECUPERABLE") || estado.equals("NOTA POR ANULACION DE FACTURA")
                                || estado.equals("NOTA MERCANCIA NO PEDIDA")
                                || estado.equals("NOTA MERCANCIA NO CODIFICADA"))) {
                            if (a != 1) {
                                String[] parts = proveedor.split("-");
                                proveedor = parts[0];
                                consecutivo = Integer.parseInt(parts[1]);
                            } else {
                                String[] parts = proveedor.split("E");
                                proveedor = parts[0];
                                consecutivo = Integer.parseInt(parts[1]);
                            }

                            obtenerDev(operacion, a, proveedor, consecutivo);
                        } else {
                            boolean proceso = procesarDev(operacion, a);
                            if (proceso) {
                                buf.append("<p>Devolucion procesada</p><b>----------<b>");
                            } else {
                                buf.append("<p class='error'>Se presento un error y la devolucion " + consecutivo
                                        + " no fue procesada por favor procesarla manualmente</p>");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JSON PARA TRAER DATOS DE LAS COTIZACIONES */
    private void obtenerCotizacionLiberar() {

        areaTrabajo(1);
        // areaTrabajo(2);
        try {
            consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\"},\"camposderetorno\":[\"svaloradic3\",\"svaloradic2\",\"svaloradic1\",\"itdoper\", \"inumoper\", \"tdetalle\", \"fcreacion\", \"ncorto\", \"iestado\", \"ntdsop\", \"ntercero\", \"iprocess\", \"fsoport\", \"snumsop\", \"qerror\", \"qwarning\", \"banulada\", \"mingresos\", \"megresos\", \"mtotaloperacion\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ORD4\"]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONArray datos = (JSONArray) respuesta.get("datos");
            String estado = "";
            String operacion = "";
            for (int i = 0; i < datos.size(); i++) {
                JSONObject arreglo = (JSONObject) datos.get(i);
                estado = String.valueOf(arreglo.get("svaloradic1"));
                operacion = String.valueOf(arreglo.get("inumoper"));
                if (estado != null && estado.equals("LI")) {
                    obtenerCotizacion(operacion);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JSON PARA TRAER DATOS DE LAS COMPRAS (RECEPCION DE MATERIALES) */
    private void obtenerSalidaCP() {

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {
                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\" },\"camposderetorno\":[\"svaloradic1\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ORD2\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado = "";
                String operacion = "";
                String procesada = "";
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    if (procesada != null && procesada.equals("0")) {
                        if (estado != null && estado.equals("LI")) {
                            obtenerSalida(operacion);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JSON PARA TRAER DATOS DE LOS TRASLADOS (AVERIAS) */
    private void obtenerTrasladoCP() {

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {

                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\" },\"camposderetorno\":[\"svaloradic1\", \"svaloradic2\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"KAR2\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado, operacion, procesada, consecutivo, fecha, prov;
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    prov = String.valueOf(arreglo.get("svaloradic2"));
                    if (procesada != null && procesada.equals("0")) {
                        if (estado != null && estado.equals("LI")) {
                            consecutivo = String.valueOf(arreglo.get("snumsop"));
                            fecha = String.valueOf(arreglo.get("fsoport"));
                            obtenerTraslado(operacion, consecutivo, fecha, a, prov);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JSON PARA TRAER DATOS DE LOS TRASLADOS (AVERIAS) */
    private void obtenerAjusteCP() {

        for (int a = 1; a < 3; a++) {

            areaTrabajo(a);

            try {
                consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\" },\"camposderetorno\":[\"svaloradic1\", \"svaloradic2\", \"svaloradic3\", \"inumoper\", \"tdetalle\", \"init\", \"iprocess\", \"fsoport\", \"snumsop\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"COM2\"]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONArray datos = (JSONArray) respuesta.get("datos");
                String estado, operacion, procesada, prov, tipo;
                for (int i = 0; i < datos.size(); i++) {
                    JSONObject arreglo = (JSONObject) datos.get(i);
                    estado = String.valueOf(arreglo.get("svaloradic1"));
                    procesada = String.valueOf(arreglo.get("iprocess"));
                    operacion = String.valueOf(arreglo.get("inumoper"));
                    prov = String.valueOf(arreglo.get("svaloradic2"));
                    tipo = String.valueOf(arreglo.get("svaloradic3"));
                    if (procesada != null && procesada.equals("0")) {
                        if (estado != null && estado.equals("LI")) {
                            obtenerAjuste(operacion, prov, tipo);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @GetMapping("/cotizaciones")

    /* JSON PARA PROCESAR LAS RECEPCION DE MATERIALES */
    private boolean procesarRecepcion(String operacion, Integer area) {

        if (area == 1) {
            areaTrabajo(area);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\",\"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\",\"itdoper\":\"ORD6\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LAS REMISIONES (SALIDAS) */
    private boolean procesarSalida(String operacion, Integer area) {

        if (area == 2) {
            areaTrabajo(area);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD2\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);

            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LOS TRASLADOS (AVERIAS) */
    private boolean procesarTraslado(String operacion, Integer area) {

        if (area == 2) {
            areaTrabajo(area);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"KAR2\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);

            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LOS AJUSTES DE INVENTARIO */
    private boolean procesarAjuste(String operacion, Integer area) {

        if (area == 2) {
            areaTrabajo(area);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"COM2\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LAS ORDENES DE COMPRAS */
    private boolean procesarCompra(String operacion) {

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD5\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LAS DEVOLUCIONES */
    private boolean procesarDev(String operacion, Integer area) {

        if (area == 2) {
            areaTrabajo(area);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"COM4\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LAS FACURAS */
    private boolean procesarFac(String operacion, String area) {

        if (area.equals("1")) {
            areaTrabajo(1);
        } else {
            areaTrabajo(2);
        }

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ING1\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA PROCESAR LAS COTIZACIONES */
    private boolean procesarCoti(String operacion) {

        areaTrabajo(1);

        try {
            consulta = "{\"accion\":\"PROCESS\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD4\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String borro = String.valueOf(datos.get("resultado"));

            if (borro.equals("T")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /* GUARDAR DATOS DE LA COMPRA EN LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerCompra(String operacion) {
        String msj = "";

        try {
            buf = new StringBuffer();
            consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD5\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject encabezado = (JSONObject) datos.get("encabezado");
            JSONObject principales = (JSONObject) datos.get("datosprincipales");
            JSONArray series = (JSONArray) datos.get("listaproductos");
            Integer consecutivo = Integer.parseInt((String.valueOf(encabezado.get("snumsop")).split("-")[1]));
            String nitProv = String.valueOf(principales.get("init"));
            String fechaOrden = String.valueOf(encabezado.get("fsoport"));
            Integer area = Integer.parseInt(String.valueOf(encabezado.get("iemp")));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = sdf.parse(fechaOrden);
            Double nitSin = Double.parseDouble(nitProv);
            int errores = 0;
            String prefijo = "ORC";
            if (area == 2) {
                prefijo = "OPP";
            }
            if (prefijo.equals("")) {
                errores = 1;
            }

            Terceros t = tercerosService.findByTer(nitProv);

            int r = 0;
            if (r == 0) {

                Ped pOC = pedService.findbyPed(Integer.parseInt(operacion), prefijo);

                if (pOC == null) {

                    Ped ped = new Ped();
                    ped.setAnulado(0);
                    ped.setTercero(tercerosService.findByTer(nitProv));
                    ped.setSo_id(operacion);
                    ped.setFecha(date);
                    ped.setFechaHora(date);
                    ped.setFechaHoraEnt(date);
                    ped.setNitDestino(nitSin);
                    ped.setPc("API");
                    ped.setUsuario("API");
                    ped.setVendedor(tercerosService.findByTer(nitProv));
                    ped.setCondicion("21");
                    ped.setCodigoDir(1);
                    ped.setCodigoDirDest(1);
                    ped.setProveedor(nitProv);
                    ped.setTipo(prefijo);
                    ped.setPedKey(new PedKey(3, 15, consecutivo));
                    pedService.savePed(ped);

                    PedHis pedHis = new PedHis();
                    pedHis.setAnulado(0);
                    pedHis.setTercero(tercerosService.findByTer(nitProv));
                    pedHis.setFecha(date);
                    pedHis.setFechaHora(date);
                    pedHis.setFechaHoraEnt(date);
                    pedHis.setNitDestino(nitSin);
                    pedHis.setPc("API");
                    pedHis.setUsuario("API");
                    pedHis.setVendedor(tercerosService.findByTer(nitProv));
                    pedHis.setCondicion("21");
                    pedHis.setCodigoDir(1);
                    pedHis.setProveedor(nitProv);
                    pedHis.setTipo(prefijo);
                    pedHis.setPedKey(new PedKey(3, 15, consecutivo));
                    pedHisService.save(pedHis);

                    Referencias refIns;
                    buf.append("<p>" + "Liberando Orden de Compra " + prefijo + consecutivo + "</p>");

                    int secu = 1;
                    for (int i = 0; i < series.size(); i++) {

                        JSONObject arreglo = (JSONObject) series.get(i);
                        String articulo = String.valueOf(arreglo.get("irecurso"));
                        double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                        double precio = Double.parseDouble(String.valueOf(arreglo.get("mprecio")));
                        double descuento = Double.parseDouble(String.valueOf(arreglo.get("qporcdescuento")));
                        double iva = Double.parseDouble(String.valueOf(arreglo.get("qporciva")));
                        refIns = referenciasService.findByRefe(articulo);

                        if (refIns == null) {
                            msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                            errores++;
                        } else {
                            DetalleOCPed docPed = new DetalleOCPed();
                            docPed.setSw(3);
                            docPed.setBodega(15);
                            docPed.setNumero(consecutivo);
                            docPed.setRefe(refIns);
                            docPed.setSeq(secu);
                            docPed.setCantidad((int) cantidad);
                            docPed.setCantidadDes(cantidad);
                            docPed.setValorUni(precio);
                            docPed.setPorcentajeIva(iva);
                            docPed.setPorcentajeDes(descuento);
                            docPed.setUnd("UND");
                            docPed.setCantidadUnd(1.0);
                            docPed.setTipo(prefijo);
                            docPed.setEstado("x");
                            if (errores == 0) {
                                docPedService.saveDetOCPed(docPed);
                            }
                            secu++;
                        }
                    }

                    if (errores == 0) {
                        boolean proceso = procesarCompra(operacion);
                        if (proceso) {
                            buf.append("<p>Compra procesada</p><b>----------<b>");
                        } else {
                            buf.append("<p class='error'>Se presento un error y la compra " + consecutivo
                                    + " no fue procesada por favor procesarla manualmente</p>");
                        }
                    } else {
                        buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                    }
                } else {
                    buf.append("<p class='error'>Compra " + consecutivo + " ya ha sido ingresada</p>");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        buf.append(msj);
    }

    /* GUARDAR DATOS DE LA COMPRA EN LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerSalida(String operacion) {

        String msj = "";

        try {
            consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD2\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject encabezado = (JSONObject) datos.get("encabezado");
            JSONObject principales = (JSONObject) datos.get("datosprincipales");
            JSONArray series = (JSONArray) datos.get("series");
            Integer consecutivo = Integer.parseInt((String.valueOf(encabezado.get("snumsop")).split("-")[1]));
            String nitProv = String.valueOf(principales.get("init"));
            String fechaOrden = String.valueOf(encabezado.get("fsoport"));
            Integer area = Integer.parseInt(String.valueOf(encabezado.get("iemp")));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = sdf.parse(fechaOrden);
            int errores = 0;
            String prefijo = "RECL";
            if (area == 2) {
                prefijo = "RECR";
            }
            if (prefijo.equals("")) {
                errores = 1;
            }

            Terceros t = tercerosService.findByTer(nitProv);
            int r = 0;
            if (r == 0) {
                EncabezadoOC eOC = ordenCompraService.findById(consecutivo, prefijo);

                if (eOC == null) {

                    EncabezadoOC encOC = new EncabezadoOC();
                    ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                    DocKey docKey = new DocKey(consecutivo, prefijo);
                    encOC.setDocKey(docKey);
                    encOC.setBodega(15);
                    encOC.setSw(11);
                    encOC.setTercero(tercerosService.findByTer(nitProv));
                    encOC.setFecha(date);
                    encOC.setFechaHora(date);
                    encOC.setAnulado(false);
                    encOC.setUsuario("API");
                    encOC.setPc("API");

                    DetalleOC doc;
                    ReferenciasSto ref;
                    Referencias refIns;
                    Lote lt;
                    buf.append("<p>Liberando Salida de Materiales " + prefijo + " " + consecutivo + "</p>");

                    int secu = 1;
                    for (int i = 0; i < series.size(); i++) {

                        JSONObject arreglo = (JSONObject) series.get(i);
                        String articulo = String.valueOf(arreglo.get("irecurso"));
                        double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                        String lote = String.valueOf(arreglo.get("iserie"));
                        refIns = referenciasService.findByRefe(articulo);
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = (cal.get(Calendar.MONTH) + 1);
                        ref = refService.findByReg(articulo, year, month, 15);
                        lt = loteService.findByLote(articulo, lote);

                        if (refIns == null) {
                            msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                            errores++;
                        } else {
                            if (lt == null) {
                                errores++;
                                buf.append("<p class='error'>Lote del articulo " + articulo + " no existe</p>");
                            }

                            if (errores == 0) {
                                if (ref != null) {
                                    ref.setCanSal(ref.getCanSal() + cantidad);
                                    refService.saveRef(ref);
                                    doc = new DetalleOC();
                                    doc.setRefe(refIns);
                                    doc.setBodega(15);
                                    doc.setTercero(tercerosService.findByTer(nitProv));
                                    doc.setNumero(consecutivo);
                                    doc.setTipo(prefijo);
                                    doc.setSeq(secu);
                                    doc.setSw(11);
                                    doc.setCantidad(cantidad);
                                    doc.setCantidadPedida(cantidad);
                                    doc.setLote(lt);
                                    secu++;
                                    detalles.add(doc);
                                } else {
                                    errores++;
                                }
                            }
                        }
                    }

                    if (errores == 0) {
                        encOC.setDetalles(detalles);
                        ocService.saveOC(encOC);
                        boolean proceso = procesarSalida(operacion, area);
                        if (proceso) {
                            buf.append("<p>Salida procesada</p><b>----------<b>");
                        } else {
                            buf.append("<p class='error'>Se presento un error y la salida " + consecutivo
                                    + " no fue procesada por favor procesarla manualmente</p>");
                        }
                    } else {
                        buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                    }
                } else {
                    buf.append("<p class='error'>Recepcion " + consecutivo + " ya ha sido ingresada</p>");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        buf.append(msj);
    }

    /* GUARDAR DATOS DEL TRASLADO (AVERIAS) EN LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerTraslado(String operacion, String consecutivo, String fecha, Integer area, String prov) {
        String msj = "";
        int errores = 0;

        if (prov.equals("")) {
            errores++;
            msj = msj + "<p class='error'>Por favor ingrese el proveedor o linea</p>";
        } else {
            try {
                consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                        + "\", \"itdoper\":\"KAR2\"}]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                log.debug("Hace consulta compra " + url);
                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONObject datos = (JSONObject) respuesta.get("datos");
                JSONArray series = (JSONArray) datos.get("series");
                Integer consec = Integer.parseInt(consecutivo.split("-")[1]);
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                Date date = sdf.parse(fecha);
                Marca mar = marcaService.findByNit(prov);
                String proveedor = mar.getNit();
                /*
                 * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
                 * String num3 = num.substring(2); proveedor = num2 + num3;
                 */

                String prefijo = "TAL";
                if (area == 2) {
                    prefijo = "TAR";
                }
                if (prefijo.equals("")) {
                    errores = 1;
                }

                EncabezadoOC eOC = ordenCompraService.findById(consec, prefijo);

                if (eOC == null) {

                    EncabezadoOC encOC = new EncabezadoOC();
                    ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                    DocKey docKey = new DocKey(consec, prefijo);
                    encOC.setDocKey(docKey);
                    encOC.setBodega(15);
                    encOC.setSw(11);
                    encOC.setTercero(tercerosService.findByTer(proveedor));
                    encOC.setFecha(date);
                    encOC.setFechaHora(date);
                    encOC.setAnulado(false);
                    encOC.setUsuario("API");
                    encOC.setPc("API");

                    DetalleOC doc;
                    ReferenciasSto ref;
                    Referencias refIns;
                    Lote lt;
                    buf.append("<p>Liberando Traslado de Averias " + prefijo + " " + consec + "</p>");

                    int secu = 1;
                    for (int i = 0; i < series.size(); i++) {

                        JSONObject arreglo = (JSONObject) series.get(i);
                        String articulo = String.valueOf(arreglo.get("irecurso"));
                        double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                        String lote = String.valueOf(arreglo.get("iserie"));
                        refIns = referenciasService.findByRefe(articulo);
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = (cal.get(Calendar.MONTH) + 1);
                        ref = refService.findByReg(articulo, year, month, 15);
                        lt = loteService.findByLote(articulo, lote);

                        if (refIns == null) {
                            msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                            errores++;
                        } else {
                            if (lt == null) {
                                errores++;
                                buf.append("<p class='error'>Lote del articulo " + articulo + " no existe</p>");
                            }

                            if (errores == 0) {
                                if (ref != null) {
                                    ref.setCanSal(ref.getCanSal() + cantidad);
                                    refService.saveRef(ref);
                                    doc = new DetalleOC();
                                    doc.setRefe(refIns);
                                    doc.setBodega(15);
                                    doc.setTercero(tercerosService.findByTer(proveedor));
                                    doc.setNumero(consec);
                                    doc.setTipo(prefijo);
                                    doc.setSeq(secu);
                                    doc.setSw(11);
                                    doc.setCantidad(cantidad);
                                    doc.setCantidadPedida(cantidad);
                                    doc.setLote(lt);
                                    secu++;
                                    detalles.add(doc);
                                } else {
                                    errores++;
                                }
                            }
                        }
                    }

                    if (errores == 0) {
                        encOC.setDetalles(detalles);
                        ocService.saveOC(encOC);
                        boolean proceso = procesarTraslado(operacion, area);
                        if (proceso) {
                            buf.append("<p>Traslado procesada</p><b>----------<b>");
                        } else {
                            buf.append("<p class='error'>Se presento un error y el traslado " + consecutivo
                                    + " no fue procesado por favor procesarlo manualmente</p>");
                        }
                    } else {
                        buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                    }
                } else {
                    buf.append("<p class='error'>Traslado " + consecutivo + " ya ha sido ingresada</p>");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        buf.append(msj);
    }

    /* GUARDAR DATOS DEL AJUSTE DE INVENTARIO LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerAjuste(String operacion, String prov, String tipo) {

        String msj = "";
        int errores = 0;
        if (prov.equals("") || tipo.equals("")) {
            errores++;
            msj = msj + "<p class='error'>Por favor ingrese la linea o el tipo</p>";
        } else {
            try {

                consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                        + "\", \"itdoper\":\"COM2\"}]}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                log.debug("Hace consulta compra " + url);
                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONObject datos = (JSONObject) respuesta.get("datos");
                JSONArray series = (JSONArray) datos.get("series");
                JSONObject encabezado = (JSONObject) datos.get("encabezado");
                Integer consecutivo = Integer.parseInt((String.valueOf(encabezado.get("snumsop")).split("-")[1]));
                String fechaOrden = String.valueOf(encabezado.get("fsoport"));
                Integer area = Integer.parseInt(String.valueOf(encabezado.get("iemp")));
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                Date date = sdf.parse(fechaOrden);
                Marca mar = marcaService.findByNit(prov);
                DecimalFormat df = new DecimalFormat("#######0.##");
                String proveedor = df.format(mar.getNit());
                /*
                 * String proveedor = String.valueOf(BigDecimal.valueOf(mar.getNit()));
                 * BigDecimal.valueOf(mar.getNit()); Terceros t =
                 * tercerosService.findByTer(proveedor);
                 */
                /*
                 * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
                 * String num3 = num.substring(2); proveedor = num2 + num3;
                 */

                String prefijo = "";
                if (area == 1 && tipo.equals("ENTRADA")) {
                    prefijo = "CJL+";
                } else if (area == 1 && tipo.equals("SALIDA")) {
                    prefijo = "CJL-";
                } else if (area == 2 && tipo.equals("ENTRADA")) {
                    prefijo = "CJR+";
                } else if (area == 2 && tipo.equals("SALIDA")) {
                    prefijo = "CJR-";
                }

                if (prefijo.equals("")) {
                    errores = 1;
                }

                if (tipo.equals("ENTRADA")) {
                    EncabezadoOC eOC = ordenCompraService.findById(consecutivo, prefijo);

                    if (eOC == null) {

                        EncabezadoOC encOC = new EncabezadoOC();
                        ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                        DocKey docKey = new DocKey(consecutivo, prefijo);
                        encOC.setDocKey(docKey);
                        encOC.setBodega(15);
                        encOC.setSw(3);
                        encOC.setTercero(tercerosService.findByTer(proveedor));
                        encOC.setFecha(date);
                        encOC.setFechaHora(date);
                        encOC.setAnulado(false);
                        encOC.setUsuario("API");
                        encOC.setPc("API");

                        Lote lt;
                        DetalleOC doc;
                        ReferenciasSto ref;
                        Referencias refIns;
                        buf.append("<p>Liberando Ajuste de Inventario (" + tipo + ") " + prefijo + " " + consecutivo
                                + "</p>");

                        int secu = 1;
                        for (int i = 0; i < series.size(); i++) {

                            JSONObject arreglo = (JSONObject) series.get(i);
                            String articulo = String.valueOf(arreglo.get("irecurso"));
                            double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                            double inicial = 0;
                            String lote = String.valueOf(arreglo.get("iserie"));
                            String fechaVen = String.valueOf(arreglo.get("fecha1"));
                            Date ven = sdf.parse(fechaVen);
                            Calendar cal = Calendar.getInstance();
                            int year = cal.get(Calendar.YEAR);
                            int month = (cal.get(Calendar.MONTH) + 1);
                            ref = refService.findByReg(articulo, year, month, 15);
                            refIns = referenciasService.findByRefe(articulo);
                            lt = loteService.findByLote(articulo, lote);

                            if (refIns == null) {
                                msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                                errores++;
                            } else {
                                if (lt == null) {
                                    lt = new Lote();
                                    lt.setCodigo(articulo);
                                    lt.setLote(lote);
                                    lt.setVencimiento(ven);
                                    lt = loteService.saveLote(lt);
                                }
                                if (ref == null) {
                                    ref = new ReferenciasSto();
                                    ref.setCanEnt(cantidad);
                                    ref.setCanIni(inicial);
                                    ref.setCanSal(inicial);
                                    ref.setStoKey(new StoKey(15, articulo, year, month));
                                } else {
                                    ref.setCanEnt(ref.getCanEnt() + cantidad);
                                }
                                refService.saveRef(ref);

                                doc = new DetalleOC();
                                doc.setRefe(refIns);
                                doc.setBodega(15);
                                doc.setTercero(tercerosService.findByTer(proveedor));
                                doc.setNumero(consecutivo);
                                doc.setTipo(prefijo);
                                doc.setSeq(secu);
                                doc.setSw(3);
                                doc.setCantidad(cantidad);
                                doc.setCantidadPedida(cantidad);
                                doc.setLote(lt);
                                secu++;
                                detalles.add(doc);
                            }
                        }

                        if (errores == 0) {
                            encOC.setDetalles(detalles);
                            ocService.saveOC(encOC);
                            boolean proceso = procesarAjuste(operacion, area);
                            if (proceso) {
                                buf.append("<p>Ajuste procesado</p><b>----------<b>");
                            } else {
                                buf.append("<p class='error'>Se presento un error y el ajuste " + consecutivo
                                        + " no fue procesado, por favor procesarlo manualmente</p>");
                            }
                        } else {
                            buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                        }
                    } else {
                        buf.append("<p class='error'>Ajuste " + consecutivo + " ya ha sido ingresada</p>");
                    }
                } else {
                    EncabezadoOC eOC = ordenCompraService.findById(consecutivo, prefijo);

                    if (eOC == null) {

                        EncabezadoOC encOC = new EncabezadoOC();
                        ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                        DocKey docKey = new DocKey(consecutivo, prefijo);
                        encOC.setDocKey(docKey);
                        encOC.setBodega(15);
                        encOC.setSw(11);
                        encOC.setTercero(tercerosService.findByTer(proveedor));
                        encOC.setFecha(date);
                        encOC.setFechaHora(date);
                        encOC.setAnulado(false);
                        encOC.setUsuario("API");
                        encOC.setPc("API");

                        DetalleOC doc;
                        ReferenciasSto ref;
                        Referencias refIns;
                        Lote lt;

                        buf.append("<p>Liberando Ajuste de Inventario (" + tipo + ") " + prefijo + " " + consecutivo
                                + "</p>");

                        int secu = 1;
                        for (int i = 0; i < series.size(); i++) {

                            JSONObject arreglo = (JSONObject) series.get(i);
                            String articulo = String.valueOf(arreglo.get("irecurso"));
                            double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                            String lote = String.valueOf(arreglo.get("iserie"));
                            refIns = referenciasService.findByRefe(articulo);
                            Calendar cal = Calendar.getInstance();
                            int year = cal.get(Calendar.YEAR);
                            int month = (cal.get(Calendar.MONTH) + 1);
                            ref = refService.findByReg(articulo, year, month, 15);
                            lt = loteService.findByLote(articulo, lote);

                            if (refIns == null) {
                                msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                                errores++;
                            } else {
                                if (lt == null) {
                                    errores++;
                                    buf.append("<p class='error'>Lote del articulo " + articulo + " no existe</p>");
                                }

                                if (errores == 0) {
                                    if (ref != null) {
                                        ref.setCanSal(ref.getCanSal() + cantidad);
                                        refService.saveRef(ref);
                                        doc = new DetalleOC();
                                        doc.setRefe(refIns);
                                        doc.setBodega(15);
                                        doc.setTercero(tercerosService.findByTer(proveedor));
                                        doc.setNumero(consecutivo);
                                        doc.setTipo(prefijo);
                                        doc.setSeq(secu);
                                        doc.setSw(11);
                                        doc.setCantidad(cantidad);
                                        doc.setCantidadPedida(cantidad);
                                        doc.setLote(lt);
                                        secu++;
                                        detalles.add(doc);
                                    } else {
                                        errores++;
                                    }
                                }
                            }
                        }

                        if (errores == 0) {
                            encOC.setDetalles(detalles);
                            ocService.saveOC(encOC);
                            boolean proceso = procesarAjuste(operacion, area);
                            if (proceso) {
                                buf.append("<p>Ajuste procesado</p><b>----------<b>");
                            } else {
                                buf.append("<p class='error'>Se presento un error y el ajuste " + consecutivo
                                        + " no fue procesado, por favor procesarlo manualmente</p>");
                            }
                        } else {
                            buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                        }
                    } else {
                        buf.append("<p class='error'>Ajuste " + consecutivo + " ya ha sido ingresada</p>");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        buf.append(msj);
    }

    /* GUARDAR DATOS DE LA RECEPCION EN LAS RESPECTIVAS TABLAS DEL WMS */
    private boolean obtenerRecepcion(String operacion) {
        String msj = "";
        boolean proceso = false;
        try {

            consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD6\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject encabezado = (JSONObject) datos.get("encabezado");
            JSONObject principales = (JSONObject) datos.get("datosprincipales");
            JSONArray series = (JSONArray) datos.get("series");
            Integer consecutivo = Integer.parseInt((String.valueOf(encabezado.get("snumsop")).split("-")[1]));
            String nitProv = String.valueOf(principales.get("init"));
            String fechaOrden = String.valueOf(encabezado.get("fsoport"));
            Integer area = Integer.parseInt(String.valueOf(encabezado.get("iemp")));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = sdf.parse(fechaOrden);

            int errores = 0;
            String prefijo = "RML";
            if (area == 2) {
                prefijo = "RMR";
            }
            if (prefijo.equals("")) {
                errores = 1;
            }

            EncabezadoOC eOC = ordenCompraService.findById(consecutivo, prefijo);

            if (eOC == null) {

                EncabezadoOC encOC = new EncabezadoOC();
                ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                DocKey docKey = new DocKey(consecutivo, prefijo);
                encOC.setDocKey(docKey);
                encOC.setBodega(15);
                encOC.setSw(3);
                encOC.setTercero(tercerosService.findByTer(nitProv));
                encOC.setFecha(date);
                encOC.setFechaHora(date);
                encOC.setAnulado(false);
                encOC.setUsuario("API");
                encOC.setPc("API");

                Lote lt;
                DetalleOC doc;
                ReferenciasSto ref;
                Referencias refIns;
                buf.append("<p>Liberando Recepcion de Materiales " + prefijo + " " + consecutivo + "</p>");

                int secu = 1;
                for (int i = 0; i < series.size(); i++) {

                    JSONObject arreglo = (JSONObject) series.get(i);
                    String articulo = String.valueOf(arreglo.get("irecurso"));
                    double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                    double inicial = 0;
                    String lote = String.valueOf(arreglo.get("iserie"));
                    String fechaVen = String.valueOf(arreglo.get("fecha1"));
                    Date ven = sdf.parse(fechaVen);
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = (cal.get(Calendar.MONTH) + 1);
                    ref = refService.findByReg(articulo, year, month, 15);
                    refIns = referenciasService.findByRefe(articulo);
                    lt = loteService.findByLote(articulo, lote);

                    if (refIns == null) {
                        msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                        errores++;
                    } else {
                        if (lt == null) {
                            lt = new Lote();
                            lt.setCodigo(articulo);
                            lt.setLote(lote);
                            lt.setVencimiento(ven);
                            lt = loteService.saveLote(lt);
                        }
                        if (ref == null) {
                            ref = new ReferenciasSto();
                            ref.setCanEnt(cantidad);
                            ref.setCanIni(inicial);
                            ref.setCanSal(inicial);
                            ref.setStoKey(new StoKey(15, articulo, year, month));
                        } else {
                            ref.setCanEnt(ref.getCanEnt() + cantidad);
                        }
                        refService.saveRef(ref);

                        doc = new DetalleOC();
                        doc.setRefe(refIns);
                        doc.setBodega(15);
                        doc.setTercero(tercerosService.findByTer(nitProv));
                        doc.setNumero(consecutivo);
                        doc.setTipo(prefijo);
                        doc.setSeq(secu);
                        doc.setSw(3);
                        doc.setCantidad(cantidad);
                        doc.setCantidadPedida(cantidad);
                        doc.setLote(lt);
                        secu++;
                        detalles.add(doc);
                    }
                }

                if (errores == 0) {
                    encOC.setDetalles(detalles);
                    ocService.saveOC(encOC);
                    proceso = procesarRecepcion(operacion, area);
                    if (proceso) {
                        buf.append("<p>Recepcion procesada</p><b>----------<b>");
                    } else {
                        buf.append("<p class='error'>Se presento un error y la compra " + consecutivo
                                + " no fue procesada por favor procesarla manualmente</p>");
                    }
                } else {
                    buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                }
            } else {
                buf.append("<p class='error'>Recepcion " + consecutivo + " ya ha sido ingresada</p>");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        buf.append(msj);
        return proceso;
    }

    /* GUARDAR DATOS DE LA DEVOLUCION EN LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerDev(String operacion, Integer area, String proveedor, Integer consecutivo) {
        String msj = "";
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM/dd/yyyy");
        String fecha = mdyFormat.format(myDate);

        try {
            consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"COM4\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONArray series = (JSONArray) datos.get("series");
            JSONObject principales = (JSONObject) datos.get("datosprincipales");
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date date = sdf.parse(fecha);
            String nitProv = String.valueOf(principales.get("init"));
            int errores = 0;
            String prefijo = "DVL", vd = "1";
            if (area == 2) {
                if (proveedor.equals("DVA")) {
                    prefijo = "DVA";
                    vd = "6";
                } else if (proveedor.equals("DVR")) {
                    prefijo = "DVR";
                    vd = "9";
                } else if (proveedor.equals("DVC")) {
                    prefijo = "DVC";
                    vd = "15";
                } else if (proveedor.equals("DVF")) {
                    prefijo = "DVF";
                    vd = "4";
                } else if (proveedor.equals("DVIN")) {
                    prefijo = "DVIN";
                    vd = "2";
                } else if (proveedor.equals("DVS")) {
                    prefijo = "DVS";
                    vd = "5";
                } else if (proveedor.equals("DVG")) {
                    prefijo = "DVG";
                    vd = "10";
                } else if (proveedor.equals("DVFR")) {
                    prefijo = "DVFR";
                    vd = "3";
                } else if (proveedor.equals("DVI")) {
                    prefijo = "DVI";
                    vd = "8";
                } else if (proveedor.equals("DVINT")) {
                    prefijo = "DVT";
                    vd = "13";
                } else if (proveedor.equals("DVRI")) {
                    prefijo = "DVRI";
                    vd = "16";
                }
            }

            if (prefijo.equals("")) {
                errores = 1;
            }

            EncabezadoOC eOC = ordenCompraService.findById(consecutivo, prefijo);

            if (eOC == null) {
                buf.append("<p>Liberando Devolucion en Ventas " + consecutivo + "</p>");

                EncabezadoOC encOC = new EncabezadoOC();
                ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();
                DocKey docKey = new DocKey(consecutivo, prefijo);
                encOC.setDocKey(docKey);
                encOC.setBodega(15);
                encOC.setSw(3);
                encOC.setTercero(tercerosService.findByTer(nitProv));
                encOC.setFecha(date);
                encOC.setFechaHora(date);
                encOC.setAnulado(false);
                encOC.setUsuario("API");
                encOC.setPc("API");

                Lote lt;
                DetalleOC doc;
                ReferenciasSto ref;
                Referencias refIns;
                int secu = 1;

                for (int i = 0; i < series.size(); i++) {

                    JSONObject arreglo = (JSONObject) series.get(i);
                    String articulo = String.valueOf(arreglo.get("irecurso"));
                    double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                    double inicial = 0;
                    String lote = String.valueOf(arreglo.get("iserie"));
                    String fechaVen = String.valueOf(arreglo.get("fecha1"));
                    Date ven = sdf.parse(fechaVen);
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = (cal.get(Calendar.MONTH) + 1);
                    ref = refService.findByReg(articulo, year, month, 15);
                    refIns = referenciasService.findByRefe(articulo);
                    lt = loteService.findByLote(articulo, lote);

                    if (refIns == null) {
                        msj = msj + "<p class='error'>Articulo " + articulo + " no existe</p>";
                        errores++;
                    } else {
                        if (lt == null) {
                            lt = new Lote();
                            lt.setCodigo(articulo);
                            lt.setLote(lote);
                            lt.setVencimiento(ven);
                            lt = loteService.saveLote(lt);
                        }
                        if (ref == null) {
                            ref = new ReferenciasSto();
                            ref.setCanEnt(cantidad);
                            ref.setCanIni(inicial);
                            ref.setCanSal(inicial);
                            ref.setStoKey(new StoKey(15, articulo, year, month));
                        } else {
                            ref.setCanEnt(ref.getCanEnt() + cantidad);
                        }
                        refService.saveRef(ref);

                        doc = new DetalleOC();
                        doc.setRefe(refIns);
                        doc.setBodega(15);
                        doc.setTercero(tercerosService.findByTer(nitProv));
                        doc.setNumero(consecutivo);
                        doc.setTipo(prefijo);
                        doc.setSeq(secu);
                        doc.setSw(3);
                        doc.setCantidad(cantidad);
                        doc.setCantidadPedida(cantidad);
                        doc.setLote(lt);
                        secu++;
                        detalles.add(doc);
                    }
                }

                if (errores == 0) {
                    encOC.setDetalles(detalles);
                    ocService.saveOC(encOC);
                    boolean proceso = procesarDev(operacion, area);
                    if (proceso) {
                        buf.append("<p>Devolucion procesada</p><b>----------<b>");
                    } else {
                        buf.append("<p class='error'>Se presento un error y la devolucion " + consecutivo
                                + " no fue procesada por favor procesarla manualmente</p>");
                    }
                } else {
                    buf.append("<p class='error'>Se encontraron errores, por favor revise el documento</p>");
                }
            } else {
                buf.append("<p class='error'>Devolucion " + consecutivo + " ya ha sido ingresada</p>");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        buf.append(msj);
    }

    /* GUARDAR DATOS DE LA COTIZACION EN LAS RESPECTIVAS TABLAS DEL WMS */
    private void obtenerCotizacion(String operacion) {
        String msj = "";
        Date myDate = new Date();

        try {
            consulta = "{\"accion\":\"LOAD\", \"operaciones\":[{\"inumoper\":\"" + operacion
                    + "\", \"itdoper\":\"ORD4\"}]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject encabezado = (JSONObject) datos.get("encabezado");
            JSONObject principales = (JSONObject) datos.get("datosprincipales");
            JSONArray productos = (JSONArray) datos.get("listaproductos");
            String nit = String.valueOf(principales.get("init"));
            boolean nitNormal = nit.contains("/");
            Double nitSin = 0.0;

            if (nitNormal == true) {
                String nit1 = ((String.valueOf(principales.get("init")).split("/")[0]));
                String nit2 = ((String.valueOf(principales.get("init")).split("/")[1]));
                String nit3 = nit1 + nit2;
                nitSin = Double.parseDouble(nit3);
            } else {
                nitSin = Double.parseDouble(nit);
            }

            productos.toString();
            String proveedor = String.valueOf(encabezado.get("svaloradic3"));

            int r = 0;
            r = validarReferencias(productos, proveedor);
            if (r == 0) {
                String vendedor = String.valueOf(principales.get("initvendedor"));
                String orden = String.valueOf(encabezado.get("svaloradic2"));
                String cot = String.valueOf(encabezado.get("snumsop"));
                String ruta = String.valueOf(encabezado.get("svaloradic4"));
                String fechaSoport = String.valueOf(encabezado.get("fsoport"));
                String fs[] = fechaSoport.split("/");
                String numPlanilla = "";

                String rute = "";
                if (ruta.equals("RUTA NORTE")) {
                    rute = "N";
                } else if (ruta.equals("RUTA SUR")) {
                    rute = "S";
                } else if (ruta.equals("RUTA ALTERNA")) {
                    rute = "A";
                } else if (ruta.equals("RUTA TRANSPORTADORA")) {
                    rute = "T";
                }
                for (int i = 0; i < fs.length; i++) {
                    numPlanilla = numPlanilla + fs[i];
                }

                numPlanilla = numPlanilla + rute;

                Proveedor tmpProv = proveedorService.findByName(proveedor);
                buf.append("<p>Liberando cotizacion " + cot + "</p>");
                Date fecCarga = myDate;
                int errores = 0;
                Ped verifica = pedService.findbyId(operacion);

                if (tmpProv != null) {
                    if (orden == null || orden.equals("")) {
                        if (tmpProv.getId().equals("1")) {
                            orden = nit + cot;
                            CenData nv = new CenData();
                            nv.setId(orden);
                            nv.setProveedor(tmpProv.getId());
                            nv.setPedido(operacion);
                            fecCarga = myDate;
                            nv.setFecha(fecCarga);
                            cenService.saveCen(nv);
                        } else {
                            msj = msj + "<p class='error'>Orden no puede ser vacia</p>";
                        }
                    } else {
                        CenData busca = cenService.findById(orden);
                        if (busca == null) {
                            busca = cenService.findById(nit + orden);
                        }
                        if (busca == null) {
                            orden = nit + orden;
                            CenData nv = new CenData();
                            nv.setId(orden);
                            nv.setProveedor(tmpProv.getId());
                            nv.setPedido(operacion);
                            fecCarga = myDate;
                            nv.setFecha(fecCarga);
                            cenService.saveCen(nv);
                        } else {
                            if (busca.getPedido() == null || busca.getPedido().equals("")) {
                                busca.setPedido(operacion);
                            } else {
                                msj = msj + "<p class='error'>Orden ya tiene un pedido asociado</p>";
                            }
                        }
                    }
                } else {
                    msj = msj + "<p class='error'>Configure el proveedor</p>";
                }

                Terceros cliPed = tercerosService.findByTer(nit);
                Terceros venPed = tercerosService.findByTer(vendedor);

                if (cliPed == null) {
                    msj = msj + "<p class='error'>Cliente no se encuentra</p>";
                }
                if (venPed == null) {
                    msj = msj + "<p class='error'>Vendedor no se encuentra</p>";
                }
                if (msj.equals("")) {

                    String prefijo = "LUC";
                    if (tmpProv.getTipo().equals("2")) {
                        prefijo = "REP";
                    }
                    if (prefijo.equals("") || fecCarga == null) {
                        errores = 1;
                    }
                    if (orden.contains(nit)) {
                        orden = orden.substring(nit.length());
                    }

                    if (verifica == null) {
                        Ped ped = new Ped();
                        ped.setAnulado(0);
                        ped.setTercero(tercerosService.findByTer(nit));
                        ped.setSo_id(operacion);
                        ped.setFecha(fecCarga);
                        ped.setFechaHora(fecCarga);
                        ped.setFechaHoraEnt(fecCarga);
                        ped.setNitDestino(nitSin);
                        ped.setPc("API");
                        ped.setUsuario("API");
                        ped.setVendedor(venPed);
                        ped.setCondicion("21");
                        ped.setCodigoDir(1);
                        ped.setCodigoDirDest(1);
                        ped.setNotas(orden);
                        ped.setProveedor(proveedor);
                        ped.setTipo(prefijo);
                        ped.setPedKey(new PedKey(1, 15, Integer.parseInt(operacion)));

                        pedService.savePed(ped);

                        PedHis pedHis = new PedHis();
                        pedHis.setAnulado(0);
                        pedHis.setTercero(tercerosService.findByTer(nit));
                        pedHis.setFecha(fecCarga);
                        pedHis.setFechaHora(fecCarga);
                        pedHis.setFechaHoraEnt(fecCarga);
                        pedHis.setNitDestino(nitSin);
                        pedHis.setPc("API");
                        pedHis.setUsuario("API");
                        pedHis.setVendedor(venPed);
                        pedHis.setCondicion("21");
                        pedHis.setCodigoDir(1);
                        pedHis.setNotas(orden);
                        pedHis.setProveedor(proveedor);
                        pedHis.setTipo(prefijo);
                        pedHis.setPedKey(new PedKey(1, 15, Integer.parseInt(operacion)));

                        pedHisService.save(pedHis);

                        buf.append("<h3>" + "Insertando pedido " + operacion + " correspondiente a la orden " + orden
                                + "</h3>");

                        int secu = 1;
                        for (int i = 0; i < productos.size(); i++) {
                            JSONObject arreglo = (JSONObject) productos.get(i);
                            String articulo = String.valueOf(arreglo.get("irecurso"));
                            double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                            double precio = Double
                                    .parseDouble((String.valueOf(arreglo.get("mprecio")).replace(",", ".")));
                            double iva = Double
                                    .parseDouble((String.valueOf(arreglo.get("qporciva")).replace(",", ".")));
                            double descuento = Double
                                    .parseDouble((String.valueOf(arreglo.get("qporcdescuento")).replace(",", ".")));
                            Referencias tmpArt = referenciasService.findByRefe(articulo);

                            if (tmpArt == null) {
                                msj = msj + "<p class='error'>Articulo no existe</p>";
                                errores++;
                            } else {
                                DetalleOCPed pedguardado = docPedService.findById(Integer.parseInt(operacion), articulo,
                                        secu);

                                if (pedguardado == null) {
                                    DetalleOCPed docPed = new DetalleOCPed();
                                    docPed.setSw(1);
                                    docPed.setBodega(15);
                                    docPed.setNumero(Integer.parseInt(operacion));
                                    docPed.setRefe(tmpArt);
                                    docPed.setSeq(secu);
                                    docPed.setCantidad((int) cantidad);
                                    docPed.setCantidadDes(0.0);
                                    docPed.setValorUni(precio);
                                    docPed.setPorcentajeIva(iva);
                                    docPed.setPorcentajeDes(descuento);
                                    docPed.setUnd("UND");
                                    docPed.setCantidadUnd(1.0);

                                    DetalleOCPedHis docPedHis = new DetalleOCPedHis();
                                    docPedHis.setSw(1);
                                    docPedHis.setBodega(15);
                                    docPedHis.setNumero(Integer.parseInt(operacion));
                                    docPedHis.setCodigo(articulo);
                                    docPedHis.setSeq(secu);
                                    docPedHis.setCantidad(cantidad);
                                    docPedHis.setCantidadDes(0.0);
                                    docPedHis.setValorUni(precio);
                                    docPedHis.setPorcentajeIva(iva);
                                    docPedHis.setPorcentajeDes(descuento);
                                    docPedHis.setUnd("UND");
                                    docPedHis.setCantidadUnd(1.0);

                                    if (errores == 0) {
                                        docPedService.saveDetOCPed(docPed);
                                        docPedHisService.saveDetOCPedHis(docPedHis);
                                        buf.append("<p>" + "Insertando Articulo " + tmpArt.getCodigo() + "</p>");
                                    }
                                    secu++;
                                } else {
                                    secu++;
                                }
                            }
                        }
                    } else {
                        int secu = 1;
                        for (int i = 0; i < productos.size(); i++) {
                            JSONObject arreglo = (JSONObject) productos.get(i);
                            String articulo = String.valueOf(arreglo.get("irecurso"));
                            double cantidad = Double.parseDouble(String.valueOf(arreglo.get("qrecurso")));
                            double precio = Double
                                    .parseDouble((String.valueOf(arreglo.get("mprecio")).replace(",", ".")));
                            double iva = Double
                                    .parseDouble((String.valueOf(arreglo.get("qporciva")).replace(",", ".")));
                            double descuento = Double
                                    .parseDouble((String.valueOf(arreglo.get("qporcdescuento")).replace(",", ".")));
                            Referencias tmpArt = referenciasService.findByRefe(articulo);

                            if (tmpArt == null) {
                                msj = msj + "<p class='error'>Articulo no existe</p>";
                                errores++;
                            } else {
                                DetalleOCPed pedguardado = docPedService.findById(Integer.parseInt(operacion), articulo,
                                        secu);

                                if (pedguardado == null) {
                                    DetalleOCPed docPed = new DetalleOCPed();
                                    docPed.setSw(1);
                                    docPed.setBodega(15);
                                    docPed.setNumero(Integer.parseInt(operacion));
                                    docPed.setRefe(tmpArt);
                                    docPed.setSeq(secu);
                                    docPed.setCantidad((int) cantidad);
                                    docPed.setCantidadDes(0.0);
                                    docPed.setValorUni(precio);
                                    docPed.setPorcentajeIva(iva);
                                    docPed.setPorcentajeDes(descuento);
                                    docPed.setUnd("UND");
                                    docPed.setCantidadUnd(1.0);

                                    DetalleOCPedHis docPedHis = new DetalleOCPedHis();
                                    docPedHis.setSw(1);
                                    docPedHis.setBodega(15);
                                    docPedHis.setNumero(Integer.parseInt(operacion));
                                    docPedHis.setCodigo(articulo);
                                    docPedHis.setSeq(secu);
                                    docPedHis.setCantidad(cantidad);
                                    docPedHis.setCantidadDes(0.0);
                                    docPedHis.setValorUni(precio);
                                    docPedHis.setPorcentajeIva(iva);
                                    docPedHis.setPorcentajeDes(descuento);
                                    docPedHis.setUnd("UND");
                                    docPedHis.setCantidadUnd(1.0);

                                    if (errores == 0) {
                                        docPedService.saveDetOCPed(docPed);
                                        docPedHisService.saveDetOCPedHis(docPedHis);
                                        buf.append("<p>" + "Insertando Articulo " + tmpArt.getCodigo() + "</p>");
                                    }
                                    secu++;
                                } else {
                                    secu++;
                                }
                            }
                        }
                    }

                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    if (errores == 0) {

                        Ped guardado = pedService.findbyId(operacion);
                        if (guardado != null) {
                            boolean borro = borraCorizacion(guardado.getSo_id());
                            if (borro) {
                                buf.append("<p>Cotizacion Eliminada con Exito</p>");
                            } else {
                                buf.append("<p class='error'>Se presento un error y la cotizacion " + cot
                                        + " no fue eliminada por favor eliminela manualmente</p>");
                            }
                        } else {
                            cenService.delete(orden);
                            buf.append("<p class='error'>Se presento un error y el pedido no fue procesado</p>");
                        }
                    } else {
                        buf.append("<p class='error'>=====ERROR=====</p>");
                    }

                    if (errores == 0) {

                        Planilla busqueda = planillaService.findById(numPlanilla);
                        if (busqueda == null) {
                            Planilla planilla = new Planilla();
                            planilla.setId(numPlanilla);
                            planilla.setEstado("x");
                            planilla.setFecha(fecCarga);
                            planillaService.save(planilla);

                        }

                        PlanillaLin planillaLin = new PlanillaLin();
                        planillaLin.setNumPlanilla(numPlanilla);
                        planillaLin.setOc(orden);
                        planillaLin.setFecha(fecCarga);
                        planillaLin.setAlmacen(tercerosService.findByTer(nit));
                        planillaLin.setNumPedido(operacion);
                        planillaLin.setProveedor(proveedor);
                        planillaLinService.save(planillaLin);

                    }
                } else {
                    cenService.delete(orden);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        buf.append(msj);
    }

    /* OBTENER LAS COTIZACIONES QUE SE ENCUENTRAN EN CONTAPYME */
    public ModelAndView obtenerCotizacionesContapyme() {
        autenticaAgente();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/pedidos/cotizaciones");

        try {
            consulta = "{\"datospagina\":{ \"cantidadregistros\":\"200\", \"pagina\":\"1\"},\"camposderetorno\":[\"svaloradic3\",\"svaloradic2\",\"svaloradic1\",\"itdoper\", \"inumoper\", \"tdetalle\", \"fcreacion\", \"ncorto\", \"iestado\", \"ntdsop\", \"ntercero\", \"iprocess\", \"fsoport\", \"snumsop\", \"qerror\", \"qwarning\", \"banulada\", \"mingresos\", \"megresos\", \"mtotaloperacion\"],\"datosfiltro\":{},\"ordenarpor\":{ \"fsoport\":\"desc\"},\"itdoper\":[\"ORD4\"]}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetListaOperaciones/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONArray datos = (JSONArray) respuesta.get("datos");
            ArrayList<CotizacionVista> cotizacionesConta = new ArrayList<CotizacionVista>();
            for (int i = 0; i < datos.size(); i++) {
                JSONObject arreglo = (JSONObject) datos.get(i);
                CotizacionVista cotmp = new CotizacionVista();
                cotmp.setNumero(String.valueOf(arreglo.get("snumsop")));
                cotmp.setEstado(String.valueOf(arreglo.get("svaloradic1")));
                cotmp.setOrden(String.valueOf(arreglo.get("svaloradic2")));
                cotmp.setCliente(String.valueOf(arreglo.get("ntercero")));
                cotmp.setFecha(String.valueOf(arreglo.get("fcreacion")));
                cotizacionesConta.add(cotmp);
            }
            modelAndView.addObject("cotizaciones", cotizacionesConta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    /* JSON PARA ELIMINAR COTIZACION DE CONTAPYME DESPUES DE LIBERADA */
    private boolean borraCorizacion(String id) {
        consulta = "{\"accion\":\"DELETE\",\"operaciones\":[{\"inumoper\":\"" + id + "\",\"itdoper\":\"ORD4\"}]}";
        url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

        try {
            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject encabezado = (JSONObject) arr.get("encabezado");
            String borro = String.valueOf(encabezado.get("resultado"));

            if (borro.equals("true")) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JSON PARA OBETENER PLAZO A PAGAR DEL CLIENTE */
    private Integer obtenerPlazo(String tercero) {
        int dias = 0;

        try {

            consulta = "{\"init\":\"" + tercero + "\"}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatTerceros/GetInfoTercero/" + consulta + "/" + key
                    + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject infoBasica = (JSONObject) datos.get("infobasica");
            String plazo = String.valueOf(infoBasica.get("qdiasplazocxc"));
            dias = Integer.parseInt(plazo);

        } catch (Exception e) {

        }
        return dias;
    }

    /* JSON PARA OBETENER DATOS DEL VENDEDOR ASOCIADO AL CLIENTE */
    private Terceros obtenerVendedor(Terceros tercero) {

        tercero.setListaPrecio("");
        tercero.setVendedor(0.0);

        try {

            consulta = "{\"init\":\"" + URLEncoder.encode(tercero.getCodigoLuc(), "UTF-8") + "\"}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatTerceros/GetInfoTercero/" + consulta + "/" + key
                    + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta a vendedor " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject infoBasica = (JSONObject) datos.get("infobasica");
            String listaPrecio = String.valueOf(infoBasica.get("ilistaprecios"));
            String vender = String.valueOf(infoBasica.get("ivendedor"));
            Double vendedor = Double.parseDouble(vender);
            tercero.setListaPrecio(listaPrecio);
            tercero.setVendedor(vendedor);

        } catch (Exception e) {

        }
        return tercero;
    }

    /* JSON PARA OBTENER EL PRECIO DEL PRODUCTO */
    private Precio obtenerPrecio(String inventario, String listaprecios, String nit, String articulo)
            throws UnsupportedEncodingException {
        Precio retorno = new Precio();

        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);

        try {

            consulta = "{\"iinventario\":\"" + inventario + "\",\"ilistaprecios\":\"" + listaprecios + "\",\"init\":\""
                    + URLEncoder.encode(nit, "UTF-8") + "\",\"iproducto\":\"" + articulo + "\",\"fsoport\":\"" + fecha
                    + "\"}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/GetValoresVentaProducto/" + consulta
                    + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            double precio = Double.parseDouble((String.valueOf(datos.get("mprecio")).replace(",", ".")));
            double descuento = Double.parseDouble((String.valueOf(datos.get("qporcdescuento")).replace(",", ".")));
            double iva = Double.parseDouble((String.valueOf(datos.get("qporciva")).replace(",", ".")));
            retorno.setPrecio(precio);
            retorno.setImpuesto(iva);
            retorno.setDescuento(descuento);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    /* CREAR CONTIZACIONES EN CONTAPYME */
    private void crearCotizacion(Proveedor prov, String orden, Terceros tercero, String productos)
            throws UnsupportedEncodingException {
        CenData cen = cenService.findById(orden);
        if (cen == null) {

            Date myDate = new Date();
            SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
            String fecha = mdyFormat.format(myDate);
            String vendedor = String.format("%.0f", tercero.getVendedor());

            try {
                consulta = "{\"accion\":\"CREATE\", \"operaciones\":[{\"itdoper\":\"ORD4\"}],\"oprdata\":{ \"datosprincipales\":{\"init\":\""
                        + URLEncoder.encode(tercero.getCodigoLuc(), "UTF-8") + "\", \"initvendedor\":\"" + vendedor
                        + "\", \"finicio\":\"" + fecha + "\", \"qdias\":\"5\", \"ilistaprecios\":\""
                        + tercero.getListaPrecio()
                        + "\", \"sobservenc\":\"\", \"bregvrunit\":\"F\", \"qporcdescuento\":\"\", \"bregvrtotal\":\"F\", \"frmenvio\":\"\", \"frmpago\":\"1\", \"condicion1\":\"\", \"blistaconiva\":\"F\",\"busarotramoneda\":\"F\", \"imonedaimpresion\":\"\", \"mtasacambio\":\"\", \"ireferencia\":\"\", \"bcerrarref\":\"F\", \"itdprintobs\":\"-1\",\"icontactocliente\":\"\"},\"encabezado\":{ \"iemp\":\""
                        + "1" + "\", \"itdsop\":\"410\", \"fsoport\":\"" + fecha
                        + "\",\"iclasifop\":\"0\", \"imoneda\":\"10\", \"iprocess\":\"0\", \"banulada\":\"F\", \"inumsop\":\"0\", \"snumsop\":\"<AUTO>\",\"tdetalle\":\""
                        + orden + "\", \"svaloradic1\":\"RE\", \"svaloradic2\":\"" + orden + "\", \"svaloradic3\":\""
                        + prov.getNombre()
                        + "\"},\"formacobro\":{},\"liquidacion\":{\"parcial\":\"\",\"descuento\":\"\",\"iva\":\"\",\"total\":\"\"},\"listaproductos\":["
                        + productos + "],\"listaanexos\":[],\"qoprsok\":\"0\"}}";
                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONObject datos = (JSONObject) respuesta.get("datos");
                String numero = String.valueOf(datos.get("inumoper"));
                String cotizacion = String.valueOf(datos.get("snumsop"));

                if (!numero.toString().equals("")) {
                    CenData nuevo = new CenData();
                    nuevo.setId(orden);
                    nuevo.setProveedor(prov.getId());
                    nuevo.setFecha(myDate);
                    cenService.saveCen(nuevo);
                    procesarCoti(numero);
                    buf.append("<p>Crea cotizacion: " + cotizacion + "</p>");
                    buf.append("<p>=======OK=======" + "</p>");
                } else {
                    buf.append("<p>=======ERROR INESPERADO=======" + "</p>");
                }
            } catch (IOException e) {
                buf.append("<p>=======ERROR INESPERADO=======" + e.toString() + "</p>");
            }
        } else {
            buf.append("<p>=======Orden creada anteriormente=======" + "</p>");
        }
    }

    /* CREA EL PEDIDO DE ACUERDO AL TIPO DE MERCANCIA QUE SE SEPARO */
    private void crearPedido(EncabezadoOC pedido, Proveedor vd, CenData cen) throws UnsupportedEncodingException {
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);

        try {

            String productos[] = prod(pedido, vd, cen);
            String vendedor = String.valueOf(BigDecimal.valueOf(pedido.getVendedor().getNit()));
            /*
             * String num = vendedor.split("E")[0]; String num2 = num.substring(0, 1);
             * String num3 = num.substring(2); String vender = num2 + num3;
             */

            consulta = "{\"accion\":\"CREATE\", \"operaciones\":[{\"itdoper\":\"ORD1\"}],\"oprdata\":{\"datosprincipales\":{\"init\":\""
                    + URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8") + "\",\"initvendedor\":\""
                    + vendedor + "\",\"finicio\":\"" + fecha + "\",\"iinventario\":\"" + vd.getBodega()
                    + "\",\"qdias\":\"5\",\"sobservenc\":\"\",\"bregvrunit\":\"F\",\"qporcdescuento\":\"\",\"bregvrtotal\":\"F\",\"frmenvio\":\"\", \"frmpago\":\"1\",\"condicion1\":\"\", \"blistaconiva\":\"F\",\"busarotramoneda\":\"F\",\"imonedaimpresion\":\"\",\"mtasacambio\":\"\",\"ireferencia\":\"\",\"bcerrarref\":\"F\",\"itdprintobs\":\"-1\",\"icontactocliente\":\"\"},\"encabezado\":{\"iemp\":\""
                    + vd.getTipo() + "\",\"itdsop\":\"420\",\"fsoport\":\"" + fecha
                    + "\",\"iclasifop\":\"0\",\"imoneda\":\"10\",\"iprocess\":\"0\",\"banulada\":\"F\",\"inumsop\":\"0\",\"snumsop\":\"<AUTO>\",\"tdetalle\":\""
                    + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic2\":\""
                    + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic3\":\"" + vd.getNombre()
                    + "\"},\"formacobro\":{},\"liquidacion\":{\"parcial\":\"\",\"descuento\":\"\",\"iva\":\"\",\"total\":\"\"},\"listaproductos\":["
                    + productos[0] + "],\"listaanexos\":[],\"qoprsok\":\"0\"}}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String numero = String.valueOf(datos.get("inumoper"));
            String numped = String.valueOf(datos.get("snumsop"));

            if (!numero.toString().equals("")) {
                buf.append("<p>Crea pedido: " + numped + " para orden de compra " + cen.getId() + "</p>");
                buf.append("<p>=======OK=======" + "</p>");
                pedido.setEstado("FA");
                pedido.setFecha(myDate);
                ordenCompraService.saveOC(pedido);
            } else {
                buf.append("<p>=======ERROR INESPERADO=======" + "</p>");
            }
        } catch (IOException e) {
            buf.append("<p>=======ERROR INESPERADO=======" + e.toString() + "</p>");
        }
    }

    /* CREA LA FACTURA DE ACUERDO A LA EMPRESA */
    private void crearFactura(EncabezadoOC pedido, Proveedor vd, CenData cen)
            throws UnsupportedEncodingException, PrinterException, ParseException {

        /*
         * boolean exist = verificarFac(pedido.getTercero().getCodigoLuc(), cen.getId(),
         * vd.getNombre(), vd.getTipo(), vd.getDocumento());
         */

        /*
         * if (exist) { buf.append("<p class='error'>" + "Factura del cliente: " +
         * pedido.getTercero().getNombres() + " y del proveedor " + vd.getNombre() +
         * " ya ha sido creada" + "</p>"); } else {
         */
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        int ter = obtenerPlazo(URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8"));

        try {
            String productos[] = prod(pedido, vd, cen);
            String lotes = jsonLotes(pedido, vd);
            Impuesto imp = calcularImpuestos(pedido, vd, cen, productos[0], productos[1]);
            String vendedor = pedido.getVendedor().getCodigoLuc();
            String impuestos = URLEncoder.encode(imp.getImpuesto(), "UTF-8");

            if (vd.getTipo().equals("1")) {
                recepcionRemision(pedido, vd, cen);
            }

            consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ING1\"}],\"oprdata\":{\"encabezado\":{\"iemp\":\""
                    + vd.getTipo() + "\",\"inumoper\":\"\",\"itdsop\":\"" + vd.getDocumento() + "\",\"fsoport\":\""
                    + fecha
                    + "\",\"iclasifop\":\"1\",\"imoneda\":\"10\",\"iprocess\":\"0\",\"banulada\":\"F\",\"inumsop\":\"\",\"snumsop\":\"<AUTO>\",\"tdetalle\":\""
                    + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic1\":\"\",\"svaloradic2\":\""
                    + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic3\":\"" + vd.getNombre()
                    + "\"},\"datosprincipales\":{\"init\":\""
                    + URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8") + "\",\"iinventario\":\""
                    + vd.getBodega() + "\",\"initvendedor\":\"" + vendedor
                    + "\",\"busarotramoneda\":\"F\",\"sobserv\":\"\",\"imoneda\":\"\",\"mtasacambio\":\"1.0000\",\"bshowsupportinfo\":\"F\",\"bregvrunit\":\"F\",\"bshowcntfields\":\"F\",\"qporcdescuento\":\"0.0000\",\"bregvrtotal\":\"F\",\"ilistaprecios\":\""
                    + pedido.getTercero().getListaPrecio() + "\"},\"listaproductos\":[" + productos[0]
                    + "],\"series\":[" + lotes + "],\"liquidimpuestos\":" + impuestos
                    + ",\"formacobro\":{\"mtotalreg\":\"" + imp.getTotal() + "\",\"mtotalpago\":\"" + imp.getTotal()
                    + "\",\"fcobrocxc\":[{\"id\":\"1\",\"init\":\""
                    + String.valueOf(BigDecimal.valueOf(pedido.getTercero().getNitReal()))
                    + "\",\"icuenta\":\"130505\",\"qdiascxc\":\"" + ter + "\",\"icc\":\"\",\"mvalor\":\""
                    + imp.getTotal() + "\"}]},\"listaanexos\":[],\"qoprsok\":\"0\"}}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String area = String.valueOf(datos.get("iemp"));
            String numero = String.valueOf(datos.get("inumoper"));
            boolean proceso = procesarFac(numero, area);

            if (proceso != true) {
                buf.append("<p class='error'>No se pudo procesar la factura, por favor revisar</p>");
            }

            String numped = String.valueOf(datos.get("snumsop"));

            if (!numero.toString().equals("")) {
                buf.append("<p>Creada Factura: " + numped + " para orden de compra " + cen.getId() + "</p>");
                buf.append("<p>=======OK=======" + "</p>");
                pedido.setEstado("FA");
                pedido.setFecha(myDate);
                ordenCompraService.saveOC(pedido);
            } else {
                buf.append("<p>=======ERROR INESPERADO=======" + "</p>");
            }
        } catch (IOException e) {
            buf.append("<p>=======ERROR INESPERADO=======" + e.toString() + "</p>");
        }
        // }

    }

    ////////////////////

    /* VALIDA SI EL DOCUMENTOS YA HA SIDO CREADO PARA MANDAR A IMPRIMIR O NO */
    /*
     * public void imp(EncabezadoOC pedido, Proveedor vd, CenData cen, String
     * productos, String totalP, String imp, Double total, ByteArrayOutputStream b,
     * String numFac) throws ParseException { try { CargaController printer = new
     * CargaController(); if (b == null) { crearDocumentoiText(pedido, vd, cen,
     * productos, totalP, imp, total, numFac); } // ByteArrayOutputStream
     * documentoBytes = //
     * printer.crearDocumentoiText(pedido,vd,cen,productos,totalP,imp,total); if (b
     * != null) { ByteArrayOutputStream documentoBytes = b;
     * printer.imprimir(documentoBytes); } } catch (IOException | PrinterException
     * ex) { // JOptionPane.showMessageDialog(null, "Error de impresion", "Error",
     * // JOptionPane.ERROR_MESSAGE); } }
     */

    /* CREAR UN DOCUMENTO PDF CON LOS DATOS A FACTURAR PERO SIENDO TIPO REMISION */
    /*
     * public void crearDocumentoiText(EncabezadoOC pedido, Proveedor vd, CenData
     * cen, String productos, String totalP, String imp, Double total, String
     * numFac) throws IOException, ParseException {
     * 
     * ByteArrayOutputStream documentoBytes = new ByteArrayOutputStream();
     * 
     * try { consulta = "{\"init\":\"" +
     * URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8") + "\"}"; url =
     * "http://" + ip + ":" + puerto + "/datasnap/rest/TCatTerceros/GetInfoTercero/"
     * + consulta + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";
     * 
     * URL url2 = new URL(url); HttpURLConnection conn = (HttpURLConnection)
     * url2.openConnection(); conn.setRequestMethod("GET");
     * conn.setRequestProperty("Accept", "application/xml");
     * 
     * if (conn.getResponseCode() != 200) { throw new
     * RuntimeException("Failed : HTTP error code : " + conn.getResponseCode()); }
     * String output; String resultado = ""; BufferedReader br = new
     * BufferedReader(new InputStreamReader((conn.getInputStream())));
     * 
     * while ((output = br.readLine()) != null) { resultado = resultado + output; }
     * 
     * log.debug("Hace consulta a vendedor " + url); JSONObject jsonObject =
     * (JSONObject) JSONValue.parse(resultado); JSONArray result = (JSONArray)
     * jsonObject.get("result"); JSONObject arr = (JSONObject) result.get(0);
     * JSONObject respuesta = (JSONObject) arr.get("respuesta"); JSONObject datos =
     * (JSONObject) respuesta.get("datos"); JSONObject infoBasica = (JSONObject)
     * datos.get("infobasica"); String nombre = String.valueOf(
     * infoBasica.get("ntercero")); String apellido = String.valueOf(
     * infoBasica.get("napellido"); String direccion )= String.valueOf(
     * infoBasica.get("tdireccion"); String vendedor = String.valueOf)(
     * infoBasica.get("ivendedor"); String telefono = String.valueOf)(
     * infoBasica.get("ttelefono"); String ciudad = String.valueOf(
     * infoBasica.get("nmun")); String digito = String.valueOf(
     * infoBasica.get("idigchequeo"); String diasP )= String.valueOf(
     * infoBasica.get("qdiasplazocxc"); Integer plazo )= Integer.parseInt(diasP);
     * Terceros vidas = tercerosService.findByTer(vendedor); vendedor =
     * vidas.getNombres();
     * 
     * String cliente = ""; if (apellido.equals("")) { cliente = nombre; } else {
     * cliente = nombre + " " + apellido; }
     * 
     * // IMPRESION DE FACTURA DE VENTA Date date = new Date(); SimpleDateFormat
     * objSDF = new SimpleDateFormat("MM/dd/yyyy"); String fecha =
     * objSDF.format(date); productos = "[" + productos + "]"; Calendar cal =
     * Calendar.getInstance(); cal.setTime(date); cal.add(Calendar.DAY_OF_YEAR,
     * plazo); Date datePlazo = cal.getTime(); String fechaPlazo =
     * objSDF.format(datePlazo);
     * 
     * PdfWriter pdfWriter = new PdfWriter(documentoBytes); PdfDocument pdfDoc = new
     * PdfDocument(pdfWriter); Document documento = new Document(pdfDoc,
     * PageSize.A4);
     * 
     * documento.setMargins(225, 20, 100, 30); Table tablaDatos = new
     * Table(UnitValue.createPercentArray((new float[] { 10, 55, 35 }))); Table
     * tablaProductos = new Table(UnitValue.createPercentArray((new float[] { 12,
     * 35, 10, 14, 6, 6, 17 }))); Table tablaImp = new
     * Table(UnitValue.createPercentArray((new float[] { 20, 20, 6, 9, 15, 15, 15
     * }))); Table tablaTotal = new Table(UnitValue.createPercentArray((new float[]
     * { 20, 20, 6, 9, 15, 15, 15 })));
     * 
     * PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
     * 
     * tablaDatos.setWidth(550); tablaProductos.setWidth(550);
     * tablaImp.setWidth(550); tablaTotal.setWidth(550);
     * 
     * EventoPagina evento = new EventoPagina(documento, pedido, cliente, fecha,
     * digito, direccion, telefono, ciudad, numFac, vendedor);
     * 
     * pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, evento);
     * 
     * Cell cell; // Añadimos los títulos a la tabla. cell = new Cell().add(new
     * Paragraph("CODIGO").setFontSize(7)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("DESCRIPCION").setFontSize(7));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("CANTIDAD").setFontSize(7));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("VALOR UNIT.").setFontSize(7));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("DCTO").setFontSize(7)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("IVA").setFontSize(7)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("SUBTOTAL").setFontSize(7));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell(1,
     * 7).add(new Paragraph(""));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell(1,
     * 7).add(new Paragraph(""));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER));
     * 
     * JSONParser jsonParser = new JSONParser(); Object object =
     * jsonParser.parse(productos); JSONArray arrayObj = (JSONArray) object;
     * DecimalFormat formato = new DecimalFormat("$#,###.###"); DecimalFormat
     * formato2 = new DecimalFormat("#0.00"); double parcial = 0;
     * 
     * for (int i = 0; i < arrayObj.size(); i++) { JSONObject arreglo = (JSONObject)
     * arrayObj.get(i); if (!arreglo.get("qporciva").equals("0.0")) { parcial +=
     * Double.parseDouble(String.valueOf( arreglo.get("mvrtotal")); )}
     * 
     * Referencias refIns = referenciasService.findByRefe(String.valueOf)(
     * arreglo.get("irecurso")); cell = new Cell().add(new
     * Paragraph(String.valueOf)( arreglo.get("irecurso")).setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell
     * = new Cell().add(new Paragraph(refIns.getDescripcion()).setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell
     * = new Cell().add(new Paragraph( Integer.toString((int)
     * Double.parseDouble(String.valueOf( arreglo.get("qproducto"))) + " UD"))
     * .setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell
     * = new Cell().add(new
     * Paragraph(formato.format(Double.parseDouble(String.valueOf)(
     * arreglo.get("mprecio")))) .setFontSize(8)); tablaProductos
     * .addCell(cell.setBorder(Border.NO_BORDER).setPadding(0).setTextAlignment(
     * TextAlignment.RIGHT)); if (arreglo.get("qporcdescuento").equals("0.0")) {
     * tablaProductos.addCell(new Cell().add(new
     * Paragraph("")).setBorder(Border.NO_BORDER).setPadding(0)); } else { cell =
     * new Cell().add(new Paragraph( Integer.toString((int)
     * Double.parseDouble(String.valueOf( arreglo.get("qporcdescuento"))) + "%"))
     * .setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); }
     * cell = new Cell().add(new Paragraph(String.valueOf( arreglo.get("qporciva")
     * )+ "%").setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell
     * = new Cell() .add(new
     * Paragraph(formato.format(Double.parseDouble(String.valueOf)(
     * arreglo.get("mvrtotal")))) .setFontSize(8)); tablaProductos
     * .addCell(cell.setBorder(Border.NO_BORDER).setPadding(0).setTextAlignment(
     * TextAlignment.RIGHT)); }
     * 
     * tablaImp.addCell(new Cell(1, 7).add(new
     * Paragraph(" ").setFontSize(8)).setBorder(Border.NO_BORDER));
     * tablaImp.addCell(new Cell(1, 7).add(new
     * Paragraph(" ").setFontSize(8)).setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("LIQUIDACION").setFont(font).setFontSize(8));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("VALOR BASE").setFont(font).setFontSize(8));
     * cell.setNextRenderer(new DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("%").setFont(font).setFontSize(8)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("VALOR").setFont(font).setFontSize(8)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("FORMA DE PAGO").setFontSize(8)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("IDENTIFICACION").setFontSize(8)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("VALOR").setFontSize(8)); cell.setNextRenderer(new
     * DottedHeaderCellRenderer(cell));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph("VALOR PARCIAL: ").setFont(font).setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); tablaImp.addCell((new
     * Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER)));
     * tablaImp.addCell((new Cell().add(new
     * Paragraph("")).setBorder(Border.NO_BORDER))); tablaImp.addCell((new
     * Cell().add(new Paragraph(formato.format(parcial)).setFontSize(8))
     * .setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)));
     * 
     * if (plazo > 1) { cell = new Cell().add(new
     * Paragraph("CREDITO").setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setTextAlignment(
     * TextAlignment.CENTER)); } else { cell = new Cell().add(new
     * Paragraph("DE CONTADO").setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setTextAlignment(
     * TextAlignment.CENTER)); }
     * 
     * cell = new Cell().add(new Paragraph("Vence: " + fechaPlazo).setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell().add(new
     * Paragraph(formato.format(total)).setFontSize(9));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setTextAlignment(
     * TextAlignment.RIGHT));
     * 
     * JSONParser jsonParser2 = new JSONParser(); Object object2 =
     * jsonParser2.parse(imp); JSONArray arrayObj2 = (JSONArray) object2;
     * 
     * Cell cell2 = null; for (int i = 0; i < arrayObj2.size(); i++) { JSONObject
     * arreglo2 = (JSONObject) arrayObj2.get(i); String signo = String.valueOf)(
     * arreglo2.get("isigno");
     * 
     * cell = new Cell().add(new Paragraph(String.valueOf)(
     * arreglo2.get("nconcepto")).setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell = new
     * Cell() .add(new Paragraph(formato.format(Double.parseDouble(String.valueOf)(
     * arreglo2.get("mvalorbase")))) .setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell = new
     * Cell() .add(new Paragraph(formato2.format(Double.parseDouble(String.valueOf)(
     * arreglo2.get("qpercent"))) + "%") .setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0));
     * 
     * if (signo.equals("-")) { cell = new Cell().add(new Paragraph( "(" +
     * formato.format(Double.parseDouble(String.valueOf( arreglo2.get("mvalor"))) +
     * ")")) .setFontSize(8)); tablaImp.addCell(
     * cell.setBorder(Border.NO_BORDER).setPadding(0).setTextAlignment(TextAlignment
     * .RIGHT)); } else { cell = new Cell() .add(new
     * Paragraph(formato.format(Double.parseDouble(String.valueOf)(
     * arreglo2.get("mvalor")))) .setFontSize(8)); tablaImp.addCell(
     * cell.setBorder(Border.NO_BORDER).setPadding(0).setTextAlignment(TextAlignment
     * .RIGHT)); }
     * 
     * if (cell2 == null) { cell2 = new Cell().add(new Paragraph(""));
     * tablaImp.addCell(cell2.setBorder(Border.NO_BORDER)); cell2 = new Cell(1,
     * 2).add(new Paragraph("FIRMA Y SELLO").setFontSize(10).setFont(font));
     * tablaImp.addCell(cell2.setBorder(Border.NO_BORDER).setPadding(0)); } else {
     * cell2 = new Cell(1, 3).add(new Paragraph(""));
     * tablaImp.addCell(cell2.setBorder(Border.NO_BORDER).setPadding(0)); } }
     * 
     * cell = new Cell(1, 3).add(new
     * Paragraph("VALOR TOTAL: ").setFont(font).setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0)); cell = new
     * Cell().add(new Paragraph(formato.format(total)).setFontSize(7));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setPadding(0).
     * setTextAlignment(TextAlignment.RIGHT)); cell = new Cell().add(new
     * Paragraph("").setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell(1,
     * 2).add(new Paragraph("C.C. O NIT.").setFontSize(8));
     * tablaImp.addCell(cell.setBorder(Border.NO_BORDER).setBorderTop(new
     * SolidBorder(1f)));
     * 
     * tablaImp.addCell(new Cell(1, 3).add(new
     * Paragraph("").setFontSize(8)).setBorder(Border.NO_BORDER));
     * tablaImp.addCell(new Cell(1, 7).add(new
     * Paragraph(" ").setFontSize(8)).setBorder(Border.NO_BORDER));
     * tablaImp.addCell(new Cell(1, 7).add(new
     * Paragraph(" ").setFontSize(8)).setBorder(Border.NO_BORDER));
     * tablaImp.addCell(new Cell(1, 7).add(new Paragraph(
     * "Res. DIAN No. 18762013522350 DE MARZO 18 2019 HASTA MARZO 18 2021 DESDE 180001 A 200000"
     * ) .setFontSize(6)) .setBorder(Border.NO_BORDER).setPadding(0));
     * tablaImp.addCell( new Cell(1, 7).add(new
     * Paragraph(" ").setFontSize(8)).setBorder(Border.NO_BORDER).setPadding(0));
     * tablaImp.addCell(new Cell(1, 7).add(new Paragraph(
     * "Girar cheque a nombre de LINEA UNO COMERCIALIZADORA LTDA. cuneta corriente Bancolombia 077-113803-01"
     * ) .setFontSize(6)) .setBorder(Border.NO_BORDER).setPadding(0));
     * tablaImp.addCell(new Cell(1, 7) .add(new
     * Paragraph("Actividad ICA 201-06 tarifa 3.3 x mil Actividad principal 4631").
     * setFontSize(6)) .setBorder(Border.NO_BORDER).setPadding(0));
     * 
     * documento.add(tablaProductos); documento.add(new Paragraph(" ")); if
     * (arrayObj.size() > 25) { documento.add(new
     * AreaBreak(AreaBreakType.NEXT_AREA)); documento.add(tablaImp); } else {
     * documento.add(tablaImp); }
     * 
     * documento.close();
     * 
     * imp(pedido, vd, cen, productos, totalP, imp, total, documentoBytes, numFac);
     */

    /*
     * IMPRESION DE REMISION!!!!!
     * 
     * Date date = new Date(); SimpleDateFormat objSDF = new
     * SimpleDateFormat("MM/dd/yyyy"); String fecha = objSDF.format(date); productos
     * = "["+productos+"]";
     * 
     * PdfWriter pdfWriter = new PdfWriter(documentoBytes); PdfDocument pdfDoc = new
     * PdfDocument(pdfWriter); Document documento = new Document(pdfDoc,
     * PageSize.A4);
     * 
     * Table tablaInicio = new Table(UnitValue.createPercentArray((new float[] {35,
     * 30, 35}))); Table tablaEncabezado = new
     * Table(UnitValue.createPercentArray((new float[] {35, 30, 35}))); Table
     * tablaDatos = new Table(UnitValue.createPercentArray((new float[] {12, 53,
     * 35}))); Table tablaProductos = new Table(UnitValue.createPercentArray((new
     * float[] {12, 30, 10}))); Table tablaRecibido = new
     * Table(UnitValue.createPercentArray((new float[] {50,50})));
     * 
     * PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
     * 
     * tablaInicio.setWidth(500F); tablaEncabezado.setWidth(500F);
     * tablaDatos.setWidth(500F); tablaProductos.setWidth(500F);
     * 
     * String ruta = System.getProperty("user.home"); Image logo = new
     * Image(ImageDataFactory.create(ruta + "/Desktop/logocompleto.jpg")); Cell
     * cell;
     * 
     * cell = new Cell(2, 1).add(logo.setAutoScale(true));
     * tablaInicio.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell(2,
     * 1).add(new Paragraph(""));
     * tablaInicio.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell(1,1).add(new
     * Paragraph("Factura de venta").setFontSize(10).setFont(font));
     * tablaInicio.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell(1,1).add(new Paragraph(numRem).setFontSize(10));
     * tablaInicio.addCell(cell.setBorder(Border.NO_BORDER));
     * 
     * cell = new Cell().add(new Paragraph("CL 39 No 15 - 17 CALI").setFontSize(8));
     * tablaEncabezado.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("TEL.: 8897172 - 311 602 83 74").setFontSize(8));
     * tablaEncabezado.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("FECHA: MM/DD/AAAA  " + fecha).setFontSize(8));
     * tablaEncabezado.addCell(cell.setBorder(Border.NO_BORDER));
     * 
     * documento.add(tablaInicio); documento.add(tablaEncabezado); documento.add(new
     * Paragraph(" ")); documento.add(new Paragraph(" ")); documento.add(new
     * Paragraph(" "));
     * 
     * cell = new Cell(1, 2).add(new
     * Paragraph(cliente).setFontSize(10).setFont(font)); tablaDatos.addCell(cell);
     * 
     * if (digito.equals("")) { cell = new Cell(2, 1).add(new Paragraph("OC: " +
     * pedido.getNotas()).setFontSize(14).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("CC No.: ").setFontSize(8).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new
     * Paragraph(df.format(pedido.getTercero().getNitReal())).setFontSize(8));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); }else{ cell = new
     * Cell(2, 1).add(new Paragraph("OC: " +
     * pedido.getNotas()).setFontSize(14).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("NIT No.: ").setFontSize(8).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(df.format(pedido.getTercero().getNitReal()) + "-" +
     * digito).setFontSize(8));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); } cell = new
     * Cell().add(new Paragraph("DIRECCION:").setFontSize(8).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(direccion).setFontSize(8));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new Cell(4,
     * 1).add(new Paragraph("FAC ELEC:" + numFac).setFontSize(14).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("TELEFONO:").setFontSize(8).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(telefono).setFontSize(8));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("CIUDAD:").setFontSize(8).setFont(font));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(ciudad + ", COLOMBIA").setFontSize(8));
     * tablaDatos.addCell(cell.setBorder(Border.NO_BORDER));
     * 
     * documento.add(tablaDatos); documento.add(new Paragraph(" "));
     * documento.add(new Paragraph(" ")); documento.add(new Paragraph(" "));
     * 
     * cell = new Cell().add(new Paragraph("CODIGO").setFontSize(7).setFont(font));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("DESCRIPCION").setFontSize(7).setFont(font));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph("CANTIDAD").setFontSize(7).setFont(font));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER));
     * 
     * JSONParser jsonParser = new JSONParser(); Object object =
     * jsonParser.parse(productos); JSONArray arrayObj = (JSONArray) object;
     * 
     * for (int i = 0; i < arrayObj.size(); i++){ JSONObject arreglo = (JSONObject)
     * arrayObj.get(i); Referencias refIns =
     * referenciasService.findByRefe(String.valueOf)( arreglo.get("irecurso")); cell
     * = new Cell().add(new Paragraph(String.valueOf)(
     * arreglo.get("irecurso")).setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(refIns.getDescripcion()).setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(Integer.toString((int)
     * Double.parseDouble(String.valueOf)( arreglo.get("qproducto"))) +
     * " UD").setFontSize(8));
     * tablaProductos.addCell(cell.setBorder(Border.NO_BORDER)); }
     * 
     * documento.add(tablaProductos); documento.add(new Paragraph(" "));
     * documento.add(new Paragraph(" "));
     * 
     * cell = new Cell().add(new Paragraph("RECIBI").setFontSize(10).setFont(font));
     * tablaRecibido.addCell(cell.setBorder(Border.NO_BORDER)); cell = new
     * Cell().add(new Paragraph(""));
     * tablaRecibido.addCell(cell.setBorder(Border.NO_BORDER)); for(int i = 0; i <
     * 10;i++){ cell = new Cell().add(new Paragraph(""));
     * tablaRecibido.addCell(cell.setBorder(Border.NO_BORDER)); } cell = new
     * Cell().add(new Paragraph("C.C. O NIT.").setFontSize(8));
     * tablaRecibido.addCell(cell.setBorder(Border.NO_BORDER).setBorderTop(new
     * SolidBorder(1f))); cell = new Cell().add(new Paragraph(""));
     * tablaRecibido.addCell(cell.setBorder(Border.NO_BORDER).setBorderTop(new
     * SolidBorder(1f)));
     * 
     * documento.add(tablaRecibido); documento.close();
     * 
     * imp(pedido, vd, cen, productos, totalP, imp, total, documentoBytes, numRem,
     * numFac);
     * 
     */

    /*
     * } catch (ParseException pe) {
     * 
     * } }
     */

    /*
     * private class DottedHeaderCellRenderer extends CellRenderer { public
     * DottedHeaderCellRenderer(Cell modelElement) { super(modelElement); }
     * 
     * @Override public void draw(DrawContext drawContext) {
     * super.draw(drawContext); PdfCanvas canvas = drawContext.getCanvas();
     * canvas.setLineDash(3f, 3f);
     * canvas.moveTo(this.getOccupiedArea().getBBox().getLeft(),
     * this.getOccupiedArea().getBBox().getBottom());
     * canvas.lineTo(this.getOccupiedArea().getBBox().getRight(),
     * this.getOccupiedArea().getBBox().getBottom()); canvas.stroke(); } }
     */

    /**
     * Envia a imprimir el ByteArrayOutoutStream creado de un documento iText
     * 
     * @param documentoBytes
     * @throws IOException
     * @throws PrinterException
     */
    /* IMPRIME EL DOCUMENTO CREADO, EN LA IMPRESORA PREDETERMINADA */
    /*
     * public void imprimir(ByteArrayOutputStream documentoBytes) throws
     * IOException, PrinterException {
     * 
     * // Aqui convertimos la el arreglo de salida a uno de entrada que podemos
     * mandar // a la impresora ByteArrayInputStream bais = new
     * ByteArrayInputStream(documentoBytes.toByteArray());
     * 
     * // Creamos un PDDocument con el arreglo de entrada que creamos PDDocument
     * document = PDDocument.load(bais);
     * 
     * PrinterJob job = PrinterJob.getPrinterJob(); job.setPageable(new
     * PDFPageable(document)); for (int i = 0; i < 3; i++) { job.print(); } }
     */

    /* CREA LA FACTURA ELECTRONICA LINEA UNO */

    private void crearFacturaElec(EncabezadoOC pedido, Proveedor vd, CenData cen) throws UnsupportedEncodingException {

        String doc = "701";
        boolean exist = verificarFac(pedido.getTercero().getCodigoLuc(), cen.getId(), vd.getNombre(), vd.getTipo(),
                doc);

        if (exist) {
            buf.append("<p class='error'>" + "Factura del cliente: " + pedido.getTercero().getNombres()
                    + " y del proveedor " + vd.getNombre() + " ya ha sido creada" + "</p>");
        } else {
            Date myDate = new Date();
            SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
            String fecha = mdyFormat.format(myDate);

            try {

                String productos[] = prod(pedido, vd, cen);
                String lotes = jsonLotes(pedido, vd);
                Impuesto imp = calcularImpuestos(pedido, vd, cen, productos[0], productos[1]);
                String vendedor = pedido.getVendedor().getCodigoLuc();
                String impuestos = URLEncoder.encode(imp.getImpuesto(), "UTF-8");

                consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ING1\"}],\"oprdata\":{\"encabezado\":{\"iemp\":\""
                        + vd.getTipo() + "\",\"inumoper\":\"\",\"itdsop\":\"" + doc + "\",\"fsoport\":\"" + fecha
                        + "\",\"iclasifop\":\"1\",\"imoneda\":\"10\",\"iprocess\":\"0\",\"banulada\":\"F\",\"inumsop\":\"\",\"snumsop\":\"<AUTO>\",\"tdetalle\":\""
                        + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic1\":\"\",\"svaloradic2\":\""
                        + URLEncoder.encode(cen.getId(), "UTF-8") + "\",\"svaloradic3\":\"" + vd.getNombre()
                        + "\"},\"datosprincipales\":{\"init\":\""
                        + URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8") + "\",\"iinventario\":\""
                        + vd.getBodega() + "\",\"initvendedor\":\"" + vendedor
                        + "\",\"busarotramoneda\":\"F\",\"sobserv\":\"\",\"imoneda\":\"\",\"mtasacambio\":\"1.0000\",\"bshowsupportinfo\":\"F\",\"bregvrunit\":\"F\",\"bshowcntfields\":\"F\",\"qporcdescuento\":\"0.0000\",\"bregvrtotal\":\"F\",\"ilistaprecios\":\""
                        + pedido.getTercero().getListaPrecio() + "\"},\"listaproductos\":[" + productos[0]
                        + "],\"series\":[" + lotes + "],\"liquidimpuestos\":" + impuestos
                        + ",\"formacobro\":{\"mtotalreg\":\"" + imp.getTotal() + "\",\"mtotalpago\":\"" + imp.getTotal()
                        + "\",\"fcobrocxc\":[{\"id\":\"1\",\"init\":\""
                        + String.valueOf(BigDecimal.valueOf(pedido.getTercero().getNitReal()))
                        + "\",\"icuenta\":\"130505\",\"icc\":\"\",\"mvalor\":\"" + imp.getTotal()
                        + "\"}]},\"listaanexos\":[],\"qoprsok\":\"0\"}}";

                url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta
                        + "/" + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

                URL url2 = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/xml");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                String output;
                String resultado = "";
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

                while ((output = br.readLine()) != null) {
                    resultado = resultado + output;
                }

                JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
                JSONArray result = (JSONArray) jsonObject.get("result");
                JSONObject arr = (JSONObject) result.get(0);
                JSONObject respuesta = (JSONObject) arr.get("respuesta");
                JSONObject datos = (JSONObject) respuesta.get("datos");
                String area = String.valueOf(datos.get("iemp"));
                String numero = String.valueOf(datos.get("inumoper"));

                if (vd.getTipo().equals("1")) {
                    recepcionRemision(pedido, vd, cen);
                }

                boolean proceso = procesarFac(numero, area);
                if (proceso != true) {
                    buf.append("<p class='error'>No se pudo procesar la factura, por favor revisar</p>");
                }

                String numped = String.valueOf(datos.get("snumsop"));

                if (!numero.toString().equals("")) {
                    buf.append("<p>Creada Factura Electronica: " + numped + " para orden de compra " + cen.getId()
                            + "</p>");
                    buf.append("<p>=======OK=======" + "</p>");
                    pedido.setEstado("FA");
                    pedido.setFecha(myDate);
                    ordenCompraService.saveOC(pedido);
                } else {
                    buf.append("<p>=======ERROR INESPERADO=======" + "</p>");
                }
            } catch (IOException e) {
                buf.append("<p>=======ERROR INESPERADO=======" + e.toString() + "</p>");
            }
        }

    }

    /* JSON PARA CALCULAR LOS IMPUESTOS DE CADA CLIENTE */
    public Impuesto calcularImpuestos(EncabezadoOC pedido, Proveedor vd, CenData cen, String productos,
            String valorNeto) {

        Impuesto retorno = new Impuesto();
        String impuestos = "";
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        String vendedor = pedido.getVendedor().getCodigoLuc();
        double neto = Double.parseDouble(valorNeto);
        NumberFormat format = new DecimalFormat("0.############################################################");
        // String nitReal =
        // String.valueOf(BigDecimal.valueOf(pedido.getTercero().getNitReal()));
        String nitReal = format.format(pedido.getTercero().getNitReal());

        try {
            consulta = "{\"accion\":\"CALCULARIMPUESTOS\",\"operaciones\":[{\"inumoper\":\"\",\"itdoper\":\"ING1\"}],\"oprdata\":{\"datosprincipales\":{\"init\":\""
                    + URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8") + "\",\"iinventario\":\""
                    + vd.getBodega() + "\",\"initvendedor\":\"" + vendedor
                    + "\",\"initvendedor2\":\"\",\"busarotramoneda\":\"F\",\"sobserv\":\"\",\"imoneda\":\"10\",\"qporcdescuento\":\"0.0000\",\"ilistaprecios\":\""
                    + pedido.getTercero().getListaPrecio()
                    + "\",\"blistaconiva\": \"F\",\"isucursalcliente\":\"-1\",\"bfacturaexportacion\":\"F\"},\"encabezado\":{\"iemp\":\""
                    + vd.getTipo() + "\",\"inumoper\":\"\",\"tdetalle\":\"" + URLEncoder.encode(cen.getId(), "UTF-8")
                    + "\",\"itdsop\":\"" + vd.getDocumento() + "\",\"inumsop\":\"0\",\"snumsop\":\"\",\"fsoport\":\""
                    + fecha + "\",\"iclasifop\":\"0\",\"imoneda\":\"10\"},\"listaproductos\":[" + productos + "]}}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            JSONObject resultado1 = (JSONObject) datos.get("resultado");
            JSONArray liquida = (JSONArray) resultado1.get("liquidimpuestos");

            for (int i = 0; i < liquida.size(); i++) {
                JSONObject arreglo = (JSONObject) liquida.get(i);
                String signo = String.valueOf(arreglo.get("isigno"));
                double valor = Double.parseDouble(String.valueOf(arreglo.get("mvalor")));
                arreglo.put("init", nitReal);

                if (signo.equals("+")) {
                    neto += valor;
                } else if (signo.equals("-")) {
                    neto -= valor;
                }
            }

            DecimalFormat df = new DecimalFormat("0");
            String valorF = df.format(neto);
            neto = Double.parseDouble(valorF);

            impuestos = liquida.toString();
            retorno.setImpuesto(impuestos);
            retorno.setTotal(neto);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    /* ENVIAR PRODUCTOS SIN QUE FALTE UNO EN LA FACTURA */
    public String[] prod(EncabezadoOC pedido, Proveedor vd, CenData cen) {

        String productos[] = new String[2];
        String prod = "", separador = "";
        String ref[] = new String[2];
        Double neto = 0.0;
        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        ArrayList<Object> lista = new ArrayList<>(Arrays.asList());
        int pos[];
        int contador1 = 0, contador2 = 0;

        for (int i = 0; i < detalles.size(); i++) {
            DetalleOC det = (DetalleOC) detalles.get(i);
            String id = det.getRefe().getCodigo();
            lista.add(id);
            lista.remove(null);
        }

        for (int i = 0; i < lista.size(); i++) {
            DetalleOC det = (DetalleOC) detalles.get(i);
            String id = det.getRefe().getCodigo();

            if (i != 0) {
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).equals(id)) {
                        contador1++;
                    }
                }

                for (int j = 0; j < i; j++) {
                    if (lista.get(j).equals(id)) {
                        contador2++;
                    }
                }

                if (contador2 == 0) {
                    int contador = 0;
                    pos = new int[contador1];
                    for (int x = 0; x < lista.size(); x++) {
                        if (lista.get(x).equals(id)) {
                            pos[contador] = x;
                            contador++;
                        }
                    }

                    productos = jsonProductos(pos, pedido, vd);
                    if (!productos[0].equals("")) {
                        if (productos[0].equals("")) {
                            separador = "";
                        } else {
                            separador = ",";
                        }
                        prod += productos[0] + separador;
                        neto += Double.parseDouble(productos[1]);
                        contador1 = 0;
                    }
                } else {
                    contador1 = 0;
                    contador2 = 0;
                }
            } else {
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).equals(id)) {
                        contador1++;
                    }
                }

                int contador = 0;
                pos = new int[contador1];
                for (int x = 0; x < lista.size(); x++) {
                    if (lista.get(x) == id) {
                        pos[contador] = x;
                        contador++;
                    }
                }

                productos = jsonProductos(pos, pedido, vd);
                double valor = Double.parseDouble(productos[1]);
                if (!productos[0].equals("")) {
                    if (productos[0].equals("")) {
                        separador = "";
                    } else {
                        separador = ",";
                    }
                    prod += productos[0] + separador;
                    neto += valor;
                    contador1 = 0;
                }
            }
        }
        prod = prod.substring(0, prod.length() - 1);
        ref[0] = prod;
        ref[1] = String.valueOf(neto);

        return ref;
    }

    /* JSON PARA LOS PRODUCTOS DE CADA FACTURA */
    public String[] jsonProductos(int[] pos, EncabezadoOC pedido, Proveedor vd) {

        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        int ubi = 0;
        double precio, descuento, valorNeto = 0.0, cantAcum = 0;
        String prod = "";
        String retorno[];
        retorno = new String[2];

        for (int i = 0; i < pos.length; i++) {
            DetalleOC det = (DetalleOC) detalles.get(pos[i]);
            cantAcum += det.getCantidad();
            ubi = pos[i];
        }

        DetalleOC det = (DetalleOC) detalles.get(ubi);

        if (det.getCantidad().doubleValue() > 0.0D) {
            try {
                Precio prec = obtenerPrecio(vd.getId(), pedido.getTercero().getListaPrecio(),
                        pedido.getTercero().getCodigoLuc(), det.getRefe().getCodigo());

                precio = cantAcum * det.getValor();
                descuento = det.getDescuento() / 100;
                valorNeto = precio - (precio * descuento);

                DecimalFormat df = new DecimalFormat("0");
                String neto = df.format(valorNeto);

                prod = "{\"icc\":\"1\",\"iinventario\":" + vd.getBodega() + ",\"irecurso\":\""
                        + det.getRefe().getCodigo() + "\",\"itiporec\":\"\",\"qproducto\":\"" + cantAcum
                        + "\",\"qrecurso\":\"" + cantAcum + "\",\"mprecio\":\"" + det.getValor()
                        + "\",\"qporcdescuento\":\"" + det.getDescuento() + "\",\"qporciva\":\"" + prec.getImpuesto()
                        + "\",\"mvrtotal\":\"" + neto + "\"}";

                /*
                 * prod = "{\"icc\":\"1\",\"iinventario\":" + vd.getBodega() +
                 * ",\"irecurso\":\"" + det.getRefe().getCodigo() +
                 * "\",\"itiporec\":\"\",\"qproducto\":\"" + cantAcum + "\",\"qrecurso\":\"" +
                 * cantAcum + "\",\"mprecio\":\"" + det.getValor() + "\",\"qporcdescuento\":\""
                 * + det.getDescuento() + "\",\"qporciva\":\"" + prec.getImpuesto() +
                 * "\",\"mvrtotal\":\"" + valorNeto + "\"}";
                 */

                retorno[0] = prod;
                retorno[1] = String.valueOf(valorNeto);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return retorno;
    }

    /* JSON PARA LOS LOTES DE CADA PRODUCTO */
    public String jsonLotes(EncabezadoOC pedido, Proveedor vd) throws UnsupportedEncodingException {

        String lotes = "";
        SimpleDateFormat mdyFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        String separador = "";

        for (int i = 0; i < detalles.size(); i++) {
            DetalleOC det = (DetalleOC) detalles.get(i);
            String fecha = URLEncoder.encode(mdyFormat.format(det.getLote().getVencimiento()), "UTF-8");
            if (lotes.equals("")) {
                separador = "";
            } else {
                separador = ",";
            }
            if (det.getCantidad().doubleValue() > 0.0D) {
                lotes = lotes + separador + "{\"icc\":\"1\",\"iinventario\":" + vd.getBodega() + ",\"irecurso\":\""
                        + det.getRefe().getCodigo() + "\",\"iserie\":\"" + det.getLote().getLote()
                        + "\",\"qrecurso\":\"" + det.getCantidad() + "\",\"fecha1\":\"" + fecha + "\",\"fecha2\":\""
                        + fecha
                        + "\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"notas\":\"\",\"inumsoporigen\":\"\"}";
            }
        }

        return lotes;
    }

    /* CREAR RECEPCION Y REMISION */
    public void recepcionRemision(EncabezadoOC pedido, Proveedor vd, CenData cen) throws UnsupportedEncodingException {

        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        ArrayList<Object> lista = new ArrayList<>(Arrays.asList());
        int pos[];
        int contador1 = 0, contador2 = 0;
        String valor = "";

        for (int i = 0; i < detalles.size(); i++) {
            DetalleOC det = (DetalleOC) detalles.get(i);
            String id = det.getRefe().getMarca();
            lista.add(id);
            lista.remove(null);
        }

        for (int i = 0; i < lista.size(); i++) {
            DetalleOC det = (DetalleOC) detalles.get(i);
            String id = det.getRefe().getMarca();
            Double prov = det.getRefe().getNit();

            if (i != 0) {
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).equals(id)) {
                        contador1++;
                    }
                }

                for (int j = 0; j < i; j++) {
                    if (lista.get(j).equals(id)) {
                        contador2++;
                    }
                }

                if (contador2 == 0) {
                    int contador = 0;
                    pos = new int[contador1];
                    for (int x = 0; x < lista.size(); x++) {
                        valor = String.valueOf(lista.get(x));
                        if (valor.equals(id)) {
                            pos[contador] = x;
                            contador++;
                        }
                    }

                    String productos[] = jsonProductosRR(pos, pedido, vd, prov);
                    String lotes[] = jsonLotesRR(pos, pedido, vd, prov);

                    if (productos[0] != "" && productos[1] != "") {
                        contador1 = 0;
                        valor = "";
                        crearRecepcion(productos[1], pedido, vd, lotes[1], cen, prov);
                        crearRemision(productos[0], pedido, vd, lotes[0], cen, prov, "REM", "");
                    } else {
                        contador1 = 0;
                        contador2 = 0;
                        valor = "";
                    }
                } else {
                    contador1 = 0;
                    contador2 = 0;
                }
            } else {
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).equals(id)) {
                        contador1++;
                    }
                }

                int contador = 0;
                pos = new int[contador1];
                for (int x = 0; x < lista.size(); x++) {
                    valor = String.valueOf(lista.get(x));
                    if (valor.equals(id)) {
                        pos[contador] = x;
                        contador++;
                    }
                }

                String productos[] = jsonProductosRR(pos, pedido, vd, prov);
                String lotes[] = jsonLotesRR(pos, pedido, vd, prov);

                if ((productos[0] != "") && (productos[1] != "") && (lotes[0] != "") && (lotes[1] != "")) {
                    contador1 = 0;
                    contador2 = 0;
                    valor = "";
                    crearRecepcion(productos[1], pedido, vd, lotes[1], cen, prov);
                    crearRemision(productos[0], pedido, vd, lotes[0], cen, prov, "REM", "");
                } else {
                    contador1 = 0;
                    contador2 = 0;
                    valor = "";
                }
            }
        }
    }

    /* PRODUCTOS DE RECEPCION Y REMISION DE PEDIDOS DE LUC CON REP */
    public String[] jsonProductosRR(int[] pos, EncabezadoOC pedido, Proveedor vd, Double prov) {
        String productosRem = "", productosRep = "", separador = "";
        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        DecimalFormat df = new DecimalFormat("#######0.##");
        String proveedor = df.format(prov);
        // String proveedor = String.valueOf(BigDecimal.valueOf(prov));
        /*
         * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
         * String num3 = num.substring(2); proveedor = num2 + num3;
         */
        String productos[] = new String[2];
        Terceros clienteCarga = tercerosService.findByTer(proveedor);
        Proveedor vd2 = proveedorService.findById(clienteCarga.getVd());

        for (int i = 0; i < pos.length; i++) {
            DetalleOC det = (DetalleOC) detalles.get(pos[i]);
            String id = det.getRefe().getMarca();

            if (id.equals("01") || id.equals("02") || id.equals("09") || id.equals("10")) {

            } else {
                if (productosRem.equals("") && productosRep.equals("")) {
                    separador = "";
                } else {
                    separador = ",";
                }
                if (det.getCantidad().doubleValue() > 0.0D) {
                    productosRem = productosRem + separador + "{\"iinventario\":\"" + vd2.getBodega()
                            + "\",\"irecurso\":\"" + det.getRefe().getCodigo() + "\",\"itiporec\":\"\",\"qrecurso\":\""
                            + det.getCantidad()
                            + "\",\"mprecio\":\"0.0000\",\"qporcdescuento\":\"0.0000\",\"qporciva\":\"0.0000\",\"mvrtotal\":\"0.0000\",\"icc\":\"\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"valor3\":\"0.0000\",\"valor4\":\"0.0000\"}";
                    productosRep = productosRep + separador + "{\"iinventario\":\"1\",\"irecurso\":\""
                            + det.getRefe().getCodigo() + "\",\"itiporec\":\"\",\"qrecurso\":\"" + det.getCantidad()
                            + "\",\"mprecio\":\"0.0000\",\"qporcdescuento\":\"0.0000\",\"qporciva\":\"0.0000\",\"mvrtotal\":\"0.0000\",\"icc\":\"\",\"sobserv\":\"\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"dato5\":\"\",\"dato6\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"valor3\":\"0.0000\",\"valor4\":\"0.0000\"}";
                }
            }
        }

        productos[0] = productosRem;
        productos[1] = productosRep;

        return productos;
    }

    /* LOTES DE RECEPCION Y REMISION DE PEDIDOS DE LUC CON REP */
    public String[] jsonLotesRR(int[] pos, EncabezadoOC pedido, Proveedor vd, Double prov)
            throws UnsupportedEncodingException {

        String lotesRem = "", lotesRep = "", separador = "";
        SimpleDateFormat mdyFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<Object> detalles = Arrays.asList(pedido.getDetalles().toArray());
        String lotes[] = new String[2];

        for (int i = 0; i < pos.length; i++) {
            DetalleOC det = (DetalleOC) detalles.get(pos[i]);
            String id = det.getRefe().getMarca();
            String fecha = URLEncoder.encode(mdyFormat.format(det.getLote().getVencimiento()), "UTF-8");
            DecimalFormat df = new DecimalFormat("#######0.##");
            String proveedor = df.format(prov);
            // String proveedor =
            // String.valueOf(BigDecimal.valueOf(det.getRefe().getNit()));
            /*
             * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
             * String num3 = num.substring(2); proveedor = num2 + num3;
             */

            if (id.equals("01") || id.equals("02") || id.equals("09") || id.equals("10")) {

            } else {

                Terceros clienteCarga = tercerosService.findByTer(proveedor);
                Proveedor vd2 = proveedorService.findById(clienteCarga.getVd());

                if (lotesRep.equals("") && lotesRem.equals("")) {
                    separador = "";
                } else {
                    separador = ",";
                }

                if (det.getCantidad().doubleValue() > 0.0D) {
                    lotesRep = lotesRep + separador + "{\"iinventario\":\"1\",\"irecurso\":\""
                            + det.getRefe().getCodigo() + "\",\"iserie\":\"" + det.getLote().getLote()
                            + "\",\"irecursopadre\":\"\",\"iseriepadre\":\"\",\"qrecurso\":\"" + det.getCantidad()
                            + "\",\"fecha1\":\"" + fecha + "\",\"fecha2\":\"" + fecha
                            + "\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"notas\":\"\",\"inumsoporigen\":\"\"}";
                    lotesRem = lotesRem + separador + "{\"iinventario\":\"" + vd2.getBodega() + "\",\"irecurso\":\""
                            + det.getRefe().getCodigo() + "\",\"iserie\":\"" + det.getLote().getLote()
                            + "\",\"irecursopadre\":\"\",\"iseriepadre\":\"\",\"qrecurso\":\"" + det.getCantidad()
                            + "\",\"fecha1\":\"" + fecha + "\",\"fecha2\":\"" + fecha
                            + "\",\"dato1\":\"\",\"dato2\":\"\",\"dato3\":\"\",\"dato4\":\"\",\"valor1\":\"0.0000\",\"valor2\":\"0.0000\",\"notas\":\"\",\"inumsoporigen\":\"\"}";
                }
            }
        }

        lotes[0] = lotesRem;
        lotes[1] = lotesRep;

        return lotes;
    }

    /* CREAR RECEPCION PARA PEDIDOS DE LUC CON REFERENCIAS REP */
    private void crearRecepcion(String productos, EncabezadoOC pedido, Proveedor vd, String lotes, CenData cen,
            Double prov) {

        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        DecimalFormat df = new DecimalFormat("#######0.##");
        String proveedor = df.format(prov);
        // String proveedor = String.valueOf(BigDecimal.valueOf(prov));
        /*
         * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
         * String num3 = num.substring(2); proveedor = num2 + num3;
         */
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = (cal.get(Calendar.MONTH) + 1);
        Terceros clienteCarga = tercerosService.findByTer(proveedor);
        Proveedor vd2 = proveedorService.findById(clienteCarga.getVd());

        try {
            consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ORD6\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \"1\",\"tdetalle\": \"VENTA DE "
                    + vd2.getNombre() + " POR LUC MES " + month + "-" + year
                    + "\",\"itdsop\": \"520\",\"inumsop\": \"0\",\"snumsop\": \"<AUTO>\",\"fsoport\": \"" + fecha
                    + "\",\"iclasifop\": \"1\",\"iprocess\": \"0\",\"banulada\": \"F\",\"svaloradic1\": \"RE\"},\"datosprincipales\": {\"init\": \""
                    + URLEncoder.encode(proveedor, "UTF-8")
                    + "\",\"inittransportador\": \"\",\"sobserv\": \"\",\"iinventario\": \"1\",\"ilistaprecios\": \"0\",\"blistaconiva\": \"F\",\"blistaconprecios\": \"T\",\"bregvrunit\": \"F\",\"bregvrtotal\": \"T\",\"ireferencia\":\"\",\"bcerrarref\": \"F\",\"breferenciabasadaencompras\": \"F\",\"qregseriesproductos\": \"1\"},\"listaproductos\": ["
                    + productos + "],\"series\":[" + lotes + "]}}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String operacion = String.valueOf(datos.get("inumoper"));
            procesarRecepcion(operacion, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* CREAR REMISION PARA PEDIDOS DE LUC CON REFERENCIAS REP */
    private String crearRemision(String productos, EncabezadoOC pedido, Proveedor vd, String lotes, CenData cen,
            Double prov, String motivo, String ref) throws UnsupportedEncodingException {

        String numeroRem = "", proveedor = "";
        Date myDate = new Date();
        SimpleDateFormat mdyFormat = new SimpleDateFormat("MM-dd-yyyy");
        String fecha = mdyFormat.format(myDate);
        if (prov != 0.0) {
            DecimalFormat df = new DecimalFormat("#######0.##");
            proveedor = df.format(prov);
            // proveedor = String.valueOf(BigDecimal.valueOf(prov));
            /*
             * String num = proveedor.split("E")[0]; String num2 = num.substring(0, 1);
             * String num3 = num.substring(2); proveedor = num2 + num3;
             */
        }
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = (cal.get(Calendar.MONTH) + 1);
        Proveedor vd2 = null;

        String mensaje = ref;
        String cliente = URLEncoder.encode(pedido.getTercero().getCodigoLuc(), "UTF-8");
        String bodega = vd.getBodega();
        int area = 1;
        if (motivo.equals("REM")) {
            Terceros clienteCarga = tercerosService.findByTer(proveedor);
            vd2 = proveedorService.findById(clienteCarga.getVd());
            bodega = vd2.getBodega();
            mensaje = "VENTA DE " + vd2.getNombre() + " POR LUC MES " + month + "-" + year;
            area = 2;
            cliente = "800113803";
            ref = "";
        }

        try {
            consulta = "{\"accion\":\"CREATE\",\"operaciones\":[{\"itdoper\":\"ORD2\"}],\"oprdata\":{\"encabezado\": {\"iemp\": \""
                    + area + "\",\"inumoper\": \"\",\"itdsop\": \"430\",\"fsoport\": \"" + fecha
                    + "\",\"iclasifop\": \"1\",\"imoneda\": \"10\",\"iprocess\": \"0\",\"banulada\": \"F\",\"inumsop\": \"0\",\"snumsop\": \"<AUTO>\",\"tdetalle\": \""
                    + mensaje
                    + "\",\"svaloradic1\": \"RE\",\"svaloradic2\": \"\",\"svaloradic3\": \"\",\"svaloradic4\": \"\",\"svaloradic5\": \"\",\"svaloradic6\": \"\",\"svaloradic7\": \"\",\"svaloradic8\": \"\",\"svaloradic9\": \"\",\"svaloradic10\": \"\",\"svaloradic11\": \"\",\"svaloradic12\": \"\"}\"datosprincipales\": {\"init\": \""
                    + cliente + "\",\"inittransportador\": \"\",\"sobserv\": \"\",\"iinventario\": \"" + bodega
                    + "\",\"ilistaprecios\": \"1\",\"blistaconiva\": \"F\",\"blistaconprecios\": \"\",\"bregvrunit\": \"F\",\"bregvrtotal\": \"F\",\"ireferenciabasadaen\": \"\",\"isucursalcliente\": \"\",\"ireferencia\": \""
                    + ref + "\",\"bcerrarref\": \"F\",\"qregseriesproductos\": \"0\"},\"listaproductos\": [" + productos
                    + "],\"series\": [" + lotes + "]}}";
            url = "http://" + ip + ":" + puerto + "/datasnap/rest/TCatOperaciones/DoExecuteOprAction/" + consulta + "/"
                    + key + "/" + app + "/" + rnd.nextInt(9999) + "/";

            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            String output;
            String resultado = "";
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            while ((output = br.readLine()) != null) {
                resultado = resultado + output;
            }

            log.debug("Hace consulta compra " + url);
            JSONObject jsonObject = (JSONObject) JSONValue.parse(resultado);
            JSONArray result = (JSONArray) jsonObject.get("result");
            JSONObject arr = (JSONObject) result.get(0);
            JSONObject respuesta = (JSONObject) arr.get("respuesta");
            JSONObject datos = (JSONObject) respuesta.get("datos");
            String operacion = String.valueOf(datos.get("inumoper"));
            numeroRem = String.valueOf(datos.get("snumsop"));
            boolean proceso = procesarSalida(operacion, area);
            numeroRem = numeroRem + "/" + proceso;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return numeroRem;
    }

    @GetMapping("/informe")
    public String uploadStatus() {
        return "informe";
    }

    public int validarReferencias(JSONArray productos, String prov) {
        List<String> articulo = new ArrayList<String>();
        int errores = 0;

        for (int i = 0; i < productos.size(); i++) {
            JSONObject arreglo = (JSONObject) productos.get(i);
            String art = String.valueOf(arreglo.get("irecurso"));
            articulo.add(art);
        }

        Set<String> quipu = new HashSet<String>(articulo);
        for (String key : quipu) {
            if ((Collections.frequency(articulo, key)) > 1) {
                buf.append("<p class='error'>Referencia " + key + " repetida</p>");
                errores++;
            }
        }

        if (!prov.equals("LINEA UNO")) {
            for (int i = 0; i < articulo.size(); i++) {
                Referencias r = referenciasService.findByRefe(articulo.get(i));
                List<Marca> m = marcaService.findByTexto(prov);
                
                if (!r.getMarca().equals(m.get(0).getId())) {
                    buf.append("<p class='error'>Referencia " + r.getDescripcion() + " no es del proveedor " + m.get(0).getNombre() + "</p>");
                    errores++;
                }
            }
        }

       

        return errores;
    }
}