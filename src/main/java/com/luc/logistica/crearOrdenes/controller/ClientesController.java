package com.luc.logistica.crearOrdenes.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.luc.logistica.crearOrdenes.model.City;
import com.luc.logistica.crearOrdenes.model.Departamento;
import com.luc.logistica.crearOrdenes.model.Terceros;
import com.luc.logistica.crearOrdenes.model.Vendedor;
import com.luc.logistica.crearOrdenes.service.CityService;
import com.luc.logistica.crearOrdenes.service.DepartamentoService;
import com.luc.logistica.crearOrdenes.service.TercerosService;
import com.luc.logistica.crearOrdenes.service.VendedorService;
import javax.validation.Valid;

@Controller
public class ClientesController {
		
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TercerosService tercerosService;
	@Autowired
	private VendedorService vendedorService;
	@Autowired
	private DepartamentoService departamentoService;
	@Autowired
	private CityService cityService;

	
	@GetMapping("/clientes")
	public ModelAndView index(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/clientes/listado");
		modelAndView.addObject("terceros",tercerosService.findAll());
		return modelAndView;
	}
	
	@PostMapping("/clientes/buscar")
	public ModelAndView buscar(@RequestParam("texto") String texto){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/clientes/listado");
		if(texto.equals("")) {
			modelAndView.addObject("terceros",tercerosService.findAll());
		}else {
			modelAndView.addObject("terceros",tercerosService.findByTexto(texto));
		}
		return modelAndView;
	}
	
	@GetMapping("/clientes/crear")
	public ModelAndView formularioCliente(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/clientes/crear");
		modelAndView.addObject("terceros",new Terceros());
		modelAndView.addObject("vendedores", vendedorService.findAll());
		modelAndView.addObject("dptos", departamentoService.findAll());
		return modelAndView;
	}
	
	@PostMapping("/clientes/crear")
	public String crearCliente(@Valid @ModelAttribute("terceros") Terceros terceros, @RequestParam("vendedor") Vendedor vd, @RequestParam("dpto") Departamento dpto, RedirectAttributes redirectAttributes){
		//Cliente nuevo = clienteService.save(cliente);
		City ciudad = cityService.findCity(terceros.getCiudad());
		String codigo = String.valueOf(terceros.getCodigoLuc());
		boolean tiene = codigo.contains("/");
		double nit = 0.0;
		double nitReal = 0.0;
            
		if(tiene == true) {
			String nit1 = (codigo.split("/")[0]);
			String nit2 = (codigo.split("/")[1]);
			String nit3 = nit1 + nit2;
			nit = Double.parseDouble(nit3);
			nitReal = Double.parseDouble(nit1);
		} else {
			nit = Double.parseDouble(codigo);
		}

		terceros.setPais("COLOMBIA");
		terceros.setContribuyente(0);
		terceros.setAutoretenedor(0);
		terceros.setConcep1("1");
		terceros.setConcep6("2");
		terceros.setYpais("169");
		terceros.setNit(nit);
		terceros.setNitReal(nitReal);
		terceros.setVendedor(Double.parseDouble(vd.getId()));
		terceros.setYdpto(Integer.parseInt(dpto.getDepartamento()));	
		terceros.setYciudad(Integer.parseInt(ciudad.getCiudad()));	
		
		Terceros nuevo = tercerosService.saveTer(terceros);
		if(nuevo == null) {
			redirectAttributes.addFlashAttribute("message", "Error al crear el cliente");
		}else {
			redirectAttributes.addFlashAttribute("message", "Cliente " + nuevo.getNombres() + " creado con exito");
		}
		return "redirect:/informe";
	}
	
}
