package com.luc.logistica.crearOrdenes.service;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.Planilla;
import com.luc.logistica.crearOrdenes.model.Terceros;

public interface PlanillaService {

	public Planilla findByEstado(String estado,String id);
	public List<Planilla> findByAyudante3(String ayudante3);
	public List<Planilla> findByAyudante2(String ayudante2);
	public List<Planilla> findByAyudante(String ayudante1);
	public List<Planilla> findByConductor(String conductor);
	public List<Planilla> findByVehiculo(String vehiculo);
	public List<Terceros> findByAyudante();
	public List<Terceros> findByConductor();
	public List<Planilla> findByPD(String f);
	public Planilla findById(String id);
	public List<Planilla> findAll();
	public Planilla save(Planilla planilla);
}
