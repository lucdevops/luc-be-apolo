package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.EncabezadoPedido;
import com.luc.logistica.crearOrdenes.model.InfoPronostico;

public interface PedidoService {
	public EncabezadoPedido findById(String id);
	public List<EncabezadoPedido> findAll();
	public void savePedido(EncabezadoPedido pedido);
	public List<EncabezadoPedido> findByTexto(String texto);
	public List<EncabezadoPedido> findEnProceso();
	public InfoPronostico obtenerInfoPronostico(String articulo);
	public List<EncabezadoPedido> findFinalizado();
}
