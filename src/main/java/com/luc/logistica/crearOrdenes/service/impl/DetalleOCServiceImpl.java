package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.DetalleOC;
import com.luc.logistica.crearOrdenes.repository.DetalleOCRepository;
import com.luc.logistica.crearOrdenes.service.DetalleOCService;

@Service("detOrdenCompraService")
public class DetalleOCServiceImpl implements DetalleOCService {
	
	@Autowired
	private DetalleOCRepository docRepo;
	
	@Override
	public DetalleOC findById(Integer numero) {
		return docRepo.findById(numero);
	}

	@Override
	public List<DetalleOC> findAll() {
		return null;
	}

	@Override
	public void saveDetOC(DetalleOC doc) {
		docRepo.flush();
		try {
			docRepo.saveAndFlush(doc);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
