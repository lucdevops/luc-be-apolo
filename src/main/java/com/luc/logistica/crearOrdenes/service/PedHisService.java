package com.luc.logistica.crearOrdenes.service;

import com.luc.logistica.crearOrdenes.model.PedHis;
import java.util.List;

public interface PedHisService {
	public List<PedHis> findAll();
	public PedHis save(PedHis pedh);
}
