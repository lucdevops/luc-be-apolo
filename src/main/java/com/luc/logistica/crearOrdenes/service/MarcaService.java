package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Marca;

public interface MarcaService {
	public Marca findById(String id);
	public Marca findByNit(String nit);
	public List<Marca> findAll();
	public List<Marca> findByTexto(String texto);
	public Marca save(Marca marca);
}
