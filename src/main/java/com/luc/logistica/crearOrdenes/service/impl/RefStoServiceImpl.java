package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.ReferenciasSto;
import com.luc.logistica.crearOrdenes.repository.RefStoRepository;
import com.luc.logistica.crearOrdenes.service.RefStoService;

@Service("refService")
public class RefStoServiceImpl implements RefStoService {

	@Autowired
	private RefStoRepository refRepo;
	
	@Override
	public List<ReferenciasSto> findAll() {
		return refRepo.findAll();
	}

	@Override
	public ReferenciasSto saveRef(ReferenciasSto ref) {
		refRepo.flush();
		try {
			return refRepo.save(ref);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public ReferenciasSto findByReg(String codigo, Integer ano, Integer mes,Integer bodega) {
		return refRepo.findByReg(codigo, ano, mes,bodega);
	}
}
