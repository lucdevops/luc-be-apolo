package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Proveedor;

public interface ProveedorService {
	public Proveedor findById(String id);
	public Proveedor findByName(String nombre);
	public List<Proveedor> findAll();
}
