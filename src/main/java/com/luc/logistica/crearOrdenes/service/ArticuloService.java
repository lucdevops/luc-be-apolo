package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Articulo;

public interface ArticuloService {
	public List<Articulo>findByTexto(String texto);
	public Articulo findById(String id);
	public List<Articulo> findAll();
	public Articulo save(Articulo articulo);
}
