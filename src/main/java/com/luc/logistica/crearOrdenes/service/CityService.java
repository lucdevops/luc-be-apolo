package com.luc.logistica.crearOrdenes.service;

import com.luc.logistica.crearOrdenes.model.City;

public interface CityService {
    public City findCity(String desc);
}