package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Grupo;
import com.luc.logistica.crearOrdenes.repository.GrupoRepository;
import com.luc.logistica.crearOrdenes.service.GrupoService;

@Service("grupoService")
public class GrupoServiceImpl implements GrupoService{
	
	@Autowired
	private GrupoRepository grupoRepository;
	
	@Override
	public Grupo findByGru(String grupo) {
		return grupoRepository.findByGru(grupo);
	}

	@Override
	public List<Grupo> findAll() {
		return grupoRepository.findAll();
	}

	@Override
	public Grupo saveGru(Grupo grupo) {
		return grupoRepository.save(grupo);
	}
	
}