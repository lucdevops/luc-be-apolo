package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.User;

public interface UserService {
	public User findUserByUserName(String username);
	public void saveUser(User user);
	public List<User> findAll();
}