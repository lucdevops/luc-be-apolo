package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.DetalleOCPed;

public interface DetalleOCPedService {
	public DetalleOCPed findById(Integer numero, String codigo, Integer secuencia);
	public DetalleOCPed findByPed(Integer numero, String tipo, String producto);
	public List<DetalleOCPed> findAll();
	public List<DetalleOCPed> findByDetalle(Integer numero, String tipo);
	public DetalleOCPed saveDetOCPed(DetalleOCPed docPed);
}