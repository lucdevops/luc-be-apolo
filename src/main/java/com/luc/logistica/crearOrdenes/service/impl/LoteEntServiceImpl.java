package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.LoteEnt;
import com.luc.logistica.crearOrdenes.repository.LoteEntRepository;
import com.luc.logistica.crearOrdenes.service.LoteEntService;

@Service("loteEntService")
public class LoteEntServiceImpl implements LoteEntService {

	@Autowired
	private LoteEntRepository loteEntRepo;

	@Override
	public List<LoteEnt> findByOrc(String numero){
		return loteEntRepo.findByOrc(numero);
	}

	@Override
	public List<LoteEnt> findAll() {
		return loteEntRepo.findAll();
	}

	@Override
	public LoteEnt saveEntLote(LoteEnt lt) {
		loteEntRepo.flush();
		try {
			return loteEntRepo.save(lt);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public LoteEnt findByLote(String codigo, String lote) {
		return loteEntRepo.findByLote(codigo,lote);
	}

}