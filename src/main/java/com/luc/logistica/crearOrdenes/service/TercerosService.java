package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Terceros;

public interface TercerosService {
	public Terceros findByTer(String codigoLuc);
	public Terceros findByEan(String ean);
	public List<Terceros> findAll();
	public List<Terceros> findByTexto(String texto);
	public Terceros saveTer(Terceros ter);
}
