package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Articulo;
import com.luc.logistica.crearOrdenes.repository.ArticuloRepository;
import com.luc.logistica.crearOrdenes.service.ArticuloService;

@Service("articuloService")
public class ArticuloServiceImpl implements ArticuloService{
	
	@Autowired
	private ArticuloRepository articuloRepository;
	
	@Override
	public Articulo findById(String id) {
		return articuloRepository.findById(id);
	}

	@Override
	public List<Articulo> findAll() {
		return articuloRepository.findAll();
	}

	@Override
	public List<Articulo> findByTexto(String texto) {
		return articuloRepository.findByTexto(texto);
	}

	@Override
	public Articulo save(Articulo articulo) {
		return articuloRepository.save(articulo);
	}
	
	
}
