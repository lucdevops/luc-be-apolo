package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Vendedor;

public interface VendedorService {
	public Vendedor findById(String id);
	public List<Vendedor> findAll();
}
