package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Vendedor;
import com.luc.logistica.crearOrdenes.repository.VendedorRepository;
import com.luc.logistica.crearOrdenes.service.VendedorService;

@Service("vendedorService")
public class VendedorServiceImpl implements VendedorService{
	@Autowired
	private VendedorRepository vendedorRepo;
	
	@Override
	public Vendedor findById(String id) {
		return vendedorRepo.findById(id);
	}

	@Override
	public List<Vendedor> findAll() {
		return vendedorRepo.findAll();
	}

}
