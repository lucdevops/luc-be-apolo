package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Referencias;

public interface ReferenciasService {
	public List<Referencias>findByTexto(String texto);
	public Referencias findByRefe(String codigo);
	public List<Referencias> findAll();
	public Referencias saveRefe(Referencias refe);
}
