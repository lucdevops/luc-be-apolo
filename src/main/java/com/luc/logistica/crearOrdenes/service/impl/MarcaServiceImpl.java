package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Marca;
import com.luc.logistica.crearOrdenes.repository.MarcaRepository;
import com.luc.logistica.crearOrdenes.service.MarcaService;

@Service("marcaService")
public class MarcaServiceImpl implements MarcaService{
	
	@Autowired
	private MarcaRepository marcaRepository;
	
	@Override
	public Marca findById(String id) {
		return marcaRepository.findById(id);
	}

	@Override
	public Marca findByNit(String nit) {
		return marcaRepository.findByNit(nit);
	}

	@Override
	public List<Marca> findAll() {
		return marcaRepository.findAll();
	}


	@Override
	public List<Marca> findByTexto(String texto) {
		return marcaRepository.findByTexto(texto);
	}

	@Override
	public Marca save(Marca marca) {
		return marcaRepository.save(marca);
	}
	
}