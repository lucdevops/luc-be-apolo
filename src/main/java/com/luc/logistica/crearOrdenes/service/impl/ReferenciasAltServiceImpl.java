package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.ReferenciasAlt;
import com.luc.logistica.crearOrdenes.repository.ReferenciasAltRepository;
import com.luc.logistica.crearOrdenes.service.ReferenciasAltService;

@Service("referenciasAltService")
public class ReferenciasAltServiceImpl implements ReferenciasAltService{
	
	@Autowired
	private ReferenciasAltRepository referenciasAltRepository;
	
	@Override
	public ReferenciasAlt findByAlt(String codigo) {
		return referenciasAltRepository.findByAlt(codigo);
	}

	@Override
	public List<ReferenciasAlt> findAll() {
		return referenciasAltRepository.findAll();
	}

	@Override
	public ReferenciasAlt saveAlt(ReferenciasAlt alt) {
		referenciasAltRepository.flush();
		try {
			return referenciasAltRepository.save(alt);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	/*@Override
	public ReferenciasAlt saveAlt(ReferenciasAlt alt) {
		return null;
	}*/
	
}