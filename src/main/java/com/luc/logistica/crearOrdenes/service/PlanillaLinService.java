package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.PlanillaLin;
import com.luc.logistica.crearOrdenes.model.Terceros;

public interface PlanillaLinService {

	public PlanillaLin findByMotiv(String motivo, Integer id);
	public PlanillaLin findByObs(String observaciones, Integer id);
	public PlanillaLin findByEmp(String empresaTransp, Integer id);
	public PlanillaLin findByHoraCita(String horaCita,Integer id);
	public PlanillaLin findByNumCita(String numCita, Integer id);
	public List<PlanillaLin> findByNumFactura(String factura, String numPedido);
	public PlanillaLin findByNumPedido(String numPedido);
	public PlanillaLin findByNumGuia(String numGuia,Integer id);
	public PlanillaLin findByOc(String oc);
	public List<Terceros> findByAlmacen();
	public List<PlanillaLin> findByNumPl(String numPlanilla, Double codigoLuc);
	public List<PlanillaLin> findByNumPlanAlm(String numPlanilla);
	public PlanillaLin findById(Integer id);
	public Object[] findDatos(String numPlanilla);
	public List<PlanillaLin> findAll();
	public PlanillaLin save(PlanillaLin planillalin);
}