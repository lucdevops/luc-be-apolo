package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.CenData;
import com.luc.logistica.crearOrdenes.repository.CenRepository;
import com.luc.logistica.crearOrdenes.service.CenService;

@Service("cenService")
public class CenServiceImpl implements CenService{
	
	@Autowired
	private CenRepository cenRepository;
	
	@Override
	public CenData findById(String id) {
		return cenRepository.findById(id);
	}
	
	@Override
	public void delete(String id) {
		cenRepository.delete(id);
	}


	@Override
	public List<CenData> findAll() {
		return cenRepository.findAll();
	}
	
	@Override
	public void saveCen(CenData cen) {
		cenRepository.save(cen);
	}

	@Override
	public CenData findByOp(String id) {
		return cenRepository.findByOp(id);
	}
	
}
