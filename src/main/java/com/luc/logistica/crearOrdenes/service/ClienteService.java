package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Cliente;

public interface ClienteService {
	public Cliente findById(String id);
	public Cliente findById2(String id);
	public List<Cliente> findAll();
	public List<Cliente> findByTexto(String texto);
	public Cliente save(Cliente cliente);
}
