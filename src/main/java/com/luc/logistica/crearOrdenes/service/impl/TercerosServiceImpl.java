package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Terceros;
import com.luc.logistica.crearOrdenes.repository.TercerosRepository;
import com.luc.logistica.crearOrdenes.service.TercerosService;

@Service("terService")
public class TercerosServiceImpl implements TercerosService{

	@Autowired
	private TercerosRepository terRepo;

	@Override
	public Terceros findByTer(String codigoLuc) {
		return terRepo.findByTer(codigoLuc);
	}

	@Override
	public Terceros findByEan(String ean) {
		return terRepo.findByEan(ean);
	}
	
	@Override
	public List<Terceros> findAll() {
		return terRepo.findAll();
	}

	@Override
	public Terceros saveTer(Terceros ter) {
		terRepo.flush();
		try {
			return terRepo.save(ter);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public List<Terceros> findByTexto(String texto) {
		return terRepo.findByTexto(texto);
	}

}
