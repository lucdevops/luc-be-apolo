package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Proveedor;
import com.luc.logistica.crearOrdenes.repository.ProveedorRepository;
import com.luc.logistica.crearOrdenes.service.ProveedorService;

@Service("proveedorService")
public class ProveedorServiceImpl implements ProveedorService{
	
	@Autowired
	private ProveedorRepository proveedorRepository;
	
	@Override
	public Proveedor findById(String id) {
		return proveedorRepository.findById(id);
	}
	
	@Override
	public Proveedor findByName(String nombre) {
		return proveedorRepository.findByName(nombre);
	}

	@Override
	public List<Proveedor> findAll() {
		return proveedorRepository.findAll();
	}
	
}
