package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.DetalleOCPedHis;

public interface DetalleOCPedHisService {
	public DetalleOCPedHis findById(Integer numero);
	public List<DetalleOCPedHis> findAll();
	public DetalleOCPedHis saveDetOCPedHis(DetalleOCPedHis docPedHis);
}