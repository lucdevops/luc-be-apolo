package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.SubGrupo;

public interface SubGrupoService {
	public SubGrupo findBySub(String grupo);
	public List<SubGrupo> findAll();
	public SubGrupo findByTexto(String texto);
	public SubGrupo saveSub(SubGrupo grupo);
}
