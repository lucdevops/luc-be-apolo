package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Um;;

public interface UmService {
	public List<Um> findById(String codigo);
	public List<Um> findAll();
	public Um save(Um um);
}
