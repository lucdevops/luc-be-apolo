package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.SubGrupo;
import com.luc.logistica.crearOrdenes.repository.SubGrupoRepository;
import com.luc.logistica.crearOrdenes.service.SubGrupoService;

@Service("subGrupoService")
public class SubGrupoServiceImpl implements SubGrupoService{
	
	@Autowired
	private SubGrupoRepository subGrupoRepository;
	
	@Override
	public SubGrupo findBySub(String subGrupo) {
		return subGrupoRepository.findBySub(subGrupo);
	}

	@Override
	public List<SubGrupo> findAll() {
		return subGrupoRepository.findAll();
	}

	@Override
	public SubGrupo saveSub(SubGrupo subGrupo) {
		return subGrupoRepository.save(subGrupo);
	}

	@Override
	public SubGrupo findByTexto(String texto) {
		return subGrupoRepository.findByTexto(texto);
	}
	
}