package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.ReferenciasSto;

public interface RefStoService {
	public ReferenciasSto findByReg(String codigo, Integer ano, Integer mes,Integer bodega);
	public List<ReferenciasSto> findAll();
	public ReferenciasSto saveRef(ReferenciasSto ref);
}
