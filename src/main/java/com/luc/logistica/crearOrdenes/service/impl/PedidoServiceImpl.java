package com.luc.logistica.crearOrdenes.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.DetallePedido;
import com.luc.logistica.crearOrdenes.model.EncabezadoPedido;
import com.luc.logistica.crearOrdenes.model.InfoPronostico;
import com.luc.logistica.crearOrdenes.model.PedidoKey;
import com.luc.logistica.crearOrdenes.repository.DetalleRepository;
import com.luc.logistica.crearOrdenes.repository.PedidoRepository;
import com.luc.logistica.crearOrdenes.service.PedidoService;

@Service("pedidoService")
public class PedidoServiceImpl implements PedidoService {

	@Autowired
	private PedidoRepository pedidoRepo;
	@Autowired
	private DetalleRepository detalleRepo;
	@Override
	public EncabezadoPedido findById(String id) {
		return pedidoRepo.findById(id);
	}

	@Override
	public List<EncabezadoPedido> findAll() {
		return pedidoRepo.findAll();
	}
	@Override
	public void savePedido(EncabezadoPedido pedido) {
		pedidoRepo.flush();
		try {
			List<DetallePedido> detalles = pedido.getDetalles();
			pedido.setDetalles(new ArrayList<>());
			pedidoRepo.saveAndFlush(pedido);
			
			for (int i = 0; i < detalles.size(); i++) {
				DetallePedido tmp = detalles.get(i);
				PedidoKey p = new PedidoKey(pedido.getId(),tmp.getArticulo().getId());
				tmp.setPedidoKey(p);
				detalleRepo.save(tmp);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public List<EncabezadoPedido> findByTexto(String texto) {
		return pedidoRepo.findByTexto(texto);
	}

	@Override
	public List<EncabezadoPedido> findEnProceso() {
		return pedidoRepo.findEnProceso();
	}

	@Override
	public List<EncabezadoPedido> findFinalizado() {
		return pedidoRepo.findFinalizado();
	}

	@Override
	public InfoPronostico obtenerInfoPronostico(String articulo) {
		InfoPronostico info = new InfoPronostico();
		Double mesAnterior = pedidoRepo.pedidosArticuloAnoMes("2017", "10", "7703121000111");
		info.setMesAnterior(mesAnterior.doubleValue());
		return info;
	}

}
