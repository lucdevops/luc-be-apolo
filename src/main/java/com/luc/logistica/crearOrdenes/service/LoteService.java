package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Lote;

public interface LoteService {
	public Lote findById(Integer id);
	public Lote findByLote(String codigo, String lote);
	public List<Lote> findAll();
	public Lote saveLote(Lote lt);
}

