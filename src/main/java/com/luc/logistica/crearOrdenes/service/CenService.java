package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.CenData;

public interface CenService {
	public CenData findById(String id);
	public CenData findByOp(String id);
	public List<CenData> findAll();
	public void saveCen(CenData cen);
	public void delete(String id);
}
