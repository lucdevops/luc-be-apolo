package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Um;
import com.luc.logistica.crearOrdenes.repository.UmRepository;
import com.luc.logistica.crearOrdenes.service.UmService;

@Service("umService")
public class UmServiceImpl implements UmService{
	
	@Autowired
	private UmRepository umRepository;
	
	@Override
	public List<Um> findById(String codigo) {
		return umRepository.findById(codigo);
	}

	@Override
	public List<Um> findAll() {
		return umRepository.findAll();
	}


	@Override
	public Um save(Um um) {
		return umRepository.save(um);
	}
	
}