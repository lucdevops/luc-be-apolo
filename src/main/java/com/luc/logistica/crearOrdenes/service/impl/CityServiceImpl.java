package com.luc.logistica.crearOrdenes.service.impl;

import com.luc.logistica.crearOrdenes.model.City;
import com.luc.logistica.crearOrdenes.repository.CityRepository;
import com.luc.logistica.crearOrdenes.service.CityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cityService")
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Override
    public City findCity(String desc) {
        return cityRepository.findCity(desc);
    }
    
}