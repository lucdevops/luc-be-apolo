package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.ReferenciasAlt;

public interface ReferenciasAltService {
	public ReferenciasAlt findByAlt(String codigo);
	public List<ReferenciasAlt> findAll();
	public ReferenciasAlt saveAlt(ReferenciasAlt alt);
}
