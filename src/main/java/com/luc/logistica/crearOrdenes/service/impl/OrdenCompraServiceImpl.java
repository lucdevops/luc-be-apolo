package com.luc.logistica.crearOrdenes.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.DetalleOC;
import com.luc.logistica.crearOrdenes.model.EncabezadoOC;
import com.luc.logistica.crearOrdenes.repository.DetalleOCRepository;
import com.luc.logistica.crearOrdenes.repository.OrdenCompraRepository;
import com.luc.logistica.crearOrdenes.service.OrdenCompraService;

@Service("ordenCompraService")
public class OrdenCompraServiceImpl implements OrdenCompraService {

	@Autowired
	private OrdenCompraRepository ocRepo;
	
	@Autowired
	private DetalleOCRepository detOcRepo;
	
	@Override
	public void saveOC(EncabezadoOC oc) {
		ocRepo.flush();
		try {
			List<DetalleOC> detalles = oc.getDetalles();
			oc.setDetalles(new ArrayList<>());
			ocRepo.saveAndFlush(oc);
			
			for (int i = 0; i < detalles.size(); i++) {
				DetalleOC tmp = detalles.get(i);
				detOcRepo.save(tmp);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		ocRepo.flush();
	}

	@Override
	public EncabezadoOC findById(Integer numero, String tipo) {
		return ocRepo.findById(numero, tipo);
	}

	@Override
	public EncabezadoOC findByOper(String operacion) {
		return ocRepo.findByOper(operacion);
	}

	@Override
	public List<EncabezadoOC> findAll() {
		return null;
	}

	@Override
	public List<EncabezadoOC> findFinalizado() {
		return ocRepo.findFinalizado();
	}


}
