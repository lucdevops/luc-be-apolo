package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.DetalleOCPedHis;
import com.luc.logistica.crearOrdenes.repository.DetalleOCPedHisRepository;
import com.luc.logistica.crearOrdenes.service.DetalleOCPedHisService;

@Service("detPedHisService")

public class DetalleOCPedHisServiceImpl implements DetalleOCPedHisService {

	@Autowired
	private DetalleOCPedHisRepository docPedHisRepo;
	
	@Override
	public DetalleOCPedHis findById(Integer numero) {
		return docPedHisRepo.findById(numero);
	}

	@Override
	public List<DetalleOCPedHis> findAll() {
		return null;
	}

	@Override
	public DetalleOCPedHis saveDetOCPedHis(DetalleOCPedHis docPedHis) {
		docPedHisRepo.flush();
		try {
			return docPedHisRepo.save(docPedHis);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
}