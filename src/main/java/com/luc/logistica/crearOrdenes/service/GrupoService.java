package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Grupo;

public interface GrupoService {
	public Grupo findByGru(String grupo);
	public List<Grupo> findAll();
	public Grupo saveGru(Grupo grupo);
}
