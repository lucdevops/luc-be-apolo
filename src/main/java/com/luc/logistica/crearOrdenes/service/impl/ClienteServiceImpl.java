package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Cliente;
import com.luc.logistica.crearOrdenes.repository.ClienteRepository;
import com.luc.logistica.crearOrdenes.service.ClienteService;

@Service("clienteService")
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public Cliente findById(String id) {
		return clienteRepository.findById(id);
	}

	@Override
	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente findById2(String id) {
		return clienteRepository.findById2(id);
	}

	@Override
	public List<Cliente> findByTexto(String texto) {
		return clienteRepository.findByTexto(texto);
	}

	@Override
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
}