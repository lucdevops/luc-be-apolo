package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Lote;
import com.luc.logistica.crearOrdenes.repository.LoteRepository;
import com.luc.logistica.crearOrdenes.service.LoteService;

@Service("loteService")
public class LoteServiceImpl implements LoteService {

	@Autowired
	private LoteRepository loteRepo;

	@Override
	public Lote findById(Integer id) {
		return loteRepo.findById(id);
	}

	@Override
	public List<Lote> findAll() {
		return loteRepo.findAll();
	}

	@Override
	public Lote saveLote(Lote lt) {
		loteRepo.flush();
		try {
			return loteRepo.save(lt);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public Lote findByLote(String codigo, String lote) {
		return loteRepo.findByLote(codigo,lote);
	}


}