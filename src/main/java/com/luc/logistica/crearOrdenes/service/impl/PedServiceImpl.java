package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Ped;
import com.luc.logistica.crearOrdenes.repository.PedRepository;
import com.luc.logistica.crearOrdenes.service.PedService;

@Service("pedService")
public class PedServiceImpl implements PedService{

	@Autowired
	private PedRepository pedRepo;
	
	@Override
	public List<Ped> findAll() {
		return null;
	}
	
	@Override
	public Ped findbyPed(Integer numero, String tipo) {
		return pedRepo.findByPed(numero, tipo);
	}
	
	@Override
	public Ped findbyId(String id) {
		return pedRepo.findById(id);
	}

	@Override
	public Ped savePed(Ped ped) {
		pedRepo.flush();
		try {
			return pedRepo.save(ped);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public List<Ped> findEnEspera() {
		return pedRepo.findEnEspera();
	}

}
