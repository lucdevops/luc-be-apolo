package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.Departamento;

public interface DepartamentoService {
	public Departamento findByDpto(String codigo);
	public List<Departamento> findAll();
	public Departamento saveDpto(Departamento codigo);
}
