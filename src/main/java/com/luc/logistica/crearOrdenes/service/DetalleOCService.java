package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.DetalleOC;

public interface DetalleOCService {
	public DetalleOC findById(Integer numero);
	public List<DetalleOC> findAll();
	public void saveDetOC(DetalleOC doc);
}

