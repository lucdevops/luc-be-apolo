package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.Departamento;
import com.luc.logistica.crearOrdenes.repository.DepartamentoRepository;
import com.luc.logistica.crearOrdenes.service.DepartamentoService;

@Service("departamentoService")
public class DepartamentoServiceImpl implements DepartamentoService {

	@Autowired
	private DepartamentoRepository departamentoRepository;

	@Override
	public Departamento findByDpto(String codigo) {
		return departamentoRepository.findByDpto(codigo);
	}

	@Override
	public List<Departamento> findAll() {
		return departamentoRepository.findAll();
	}

	@Override
	public Departamento saveDpto(Departamento codigo) {
		return departamentoRepository.save(codigo);
	}

}