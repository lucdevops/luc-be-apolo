package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearOrdenes.model.PedHis;
import com.luc.logistica.crearOrdenes.repository.PedHisRepository;
import com.luc.logistica.crearOrdenes.service.PedHisService;

@Service("pedHisService")
public class PedHisServiceImpl implements PedHisService{

	@Autowired
	private PedHisRepository pedHisRepo;
	
	@Override
	public List<PedHis> findAll() {
		return null;
	}

	@Override
	public PedHis save(PedHis pedh) {
		pedHisRepo.flush();
		try {
			return pedHisRepo.save(pedh);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}
}
