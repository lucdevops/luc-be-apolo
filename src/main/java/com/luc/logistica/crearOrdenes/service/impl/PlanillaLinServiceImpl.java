package com.luc.logistica.crearOrdenes.service.impl;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.PlanillaLin;
import com.luc.logistica.crearOrdenes.model.Terceros;
import com.luc.logistica.crearOrdenes.repository.PlanillaLinRepository;
import com.luc.logistica.crearOrdenes.service.PlanillaLinService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("planillalinService")
public class PlanillaLinServiceImpl implements PlanillaLinService {

    @Autowired
    private PlanillaLinRepository planillalinRepository;

    @Override
    public PlanillaLin findByOc(String oc) {
        return planillalinRepository.findByOc(oc);
    }

    @Override
    public List<PlanillaLin> findByNumPl(String numPlanilla, Double codigoLuc) {
        return planillalinRepository.findByNumPl(numPlanilla, codigoLuc);
    }

    @Override
    public List<PlanillaLin> findByNumPlanAlm(String numPlanilla) {
        return planillalinRepository.findByNumPlanAlm(numPlanilla);
    }

    @Override
    public PlanillaLin findById(Integer id) {
        return planillalinRepository.findById(id);
    }

    @Override
    public List<PlanillaLin> findAll() {
        return planillalinRepository.findAll();
    }

    @Override
    public PlanillaLin save(PlanillaLin planillalin) {
        return planillalinRepository.save(planillalin);
    }

    @Override
    public List<Terceros> findByAlmacen() {
        return null;
    }

    @Override
    public PlanillaLin findByNumPedido(String numPedido) {
        return planillalinRepository.findByNumPedido(numPedido);
    }

    @Override
    public List<PlanillaLin> findByNumFactura(String factura, String numPedido) {
        return planillalinRepository.findByNumFactura(factura, numPedido);
    }

    @Override
    public PlanillaLin findByNumGuia(String numGuia, Integer id) {
        return planillalinRepository.findByNumGuia(numGuia, id);
    }

    @Override
    public PlanillaLin findByNumCita(String numCita, Integer id) {
        return planillalinRepository.findByNumCita(numCita, id);
    }

    @Override
    public PlanillaLin findByHoraCita(String horaCita, Integer id) {
        return planillalinRepository.findByHoraCita(horaCita, id);
    }

    @Override
    public PlanillaLin findByObs(String observaciones, Integer id) {
        return planillalinRepository.findByObs(observaciones, id);
    }

    @Override
    public PlanillaLin findByMotiv(String motivo, Integer id) {
        return planillalinRepository.findByMotiv(motivo, id);
    }

    @Override
    public Object[] findDatos(String numPlanilla) {
        return planillalinRepository.findDatos(numPlanilla);
    }

    @Override
    public PlanillaLin findByEmp(String empresaTransp, Integer id) {
        return planillalinRepository.findByEmp(empresaTransp, id);
    }


  
}