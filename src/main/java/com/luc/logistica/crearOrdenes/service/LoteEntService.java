package com.luc.logistica.crearOrdenes.service;

import java.util.List;
import com.luc.logistica.crearOrdenes.model.LoteEnt;

public interface LoteEntService {
	public LoteEnt findByLote(String codigo, String lote);
	public List<LoteEnt> findByOrc(String numero);
	public List<LoteEnt> findAll();
	public LoteEnt saveEntLote(LoteEnt lt);
}

