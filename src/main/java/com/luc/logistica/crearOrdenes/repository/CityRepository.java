package com.luc.logistica.crearOrdenes.repository;

import com.luc.logistica.crearOrdenes.model.City;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository("cityRepository")
public interface CityRepository extends JpaRepository<City, String> {
    @Query("select cd from City cd where cd.descripcion = ?1")
    City findCity(String desc);
}