package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.DetalleOCPed;

@Repository("detalleOCPedRepository")
public interface DetalleOCPedRepository extends JpaRepository<DetalleOCPed, Integer>{
	
	@Query("select doc from DetalleOCPed doc where doc.numero = ?1 and doc.refe.codigo = ?2 and doc.seq = ?3")
	DetalleOCPed findById(Integer numero, String codigo, Integer secuencia);

	@Query("select doc from DetalleOCPed doc where doc.numero = ?1 and doc.tipo = ?2 and doc.refe.codigo = ?3")
	DetalleOCPed findByPed(Integer numero, String tipo, String producto);

	@Query("select doc from DetalleOCPed doc where doc.numero = ?1 and doc.tipo = ?2")
	List<DetalleOCPed> findByDetalle(Integer numero, String tipo);

}