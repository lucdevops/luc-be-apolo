package com.luc.logistica.crearOrdenes.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Articulo;

@Repository("articuloRepository")
public interface ArticuloRepository extends JpaRepository<Articulo, String> {
	 @Query("select a from Articulo a where a.id = ?1")
	 Articulo findById(String id);
	 
	 @Query("SELECT c FROM Articulo c WHERE (c.nombre like %?1% OR c.id like %?1%)")
	 List<Articulo>findByTexto(String texto);
	
}