package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.LoteEnt;

@Repository("loteEntRepository")
public interface LoteEntRepository extends JpaRepository<LoteEnt, Integer> {
	
	@Query("select l from LoteEnt l where l.codigo = ?1 AND l.lote = ?2")
	LoteEnt findByLote(String codigo, String lote);

	@Query("select l from LoteEnt l where l.numero = ?1")
	List<LoteEnt> findByOrc(String numero);
}