package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.ReferenciasSto;
import com.luc.logistica.crearOrdenes.model.StoKey;

@Repository("refRepository")
public interface RefStoRepository extends JpaRepository<ReferenciasSto, StoKey> {
	
	@Query("select r from ReferenciasSto r where r.stoKey.codigo = ?1 and r.stoKey.ano = ?2 and r.stoKey.mes = ?3 and r.stoKey.bodega = ?4")
	ReferenciasSto findByReg(String codigo, Integer ano, Integer mes,Integer bodega);
}
