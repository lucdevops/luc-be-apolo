package com.luc.logistica.crearOrdenes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.DocKey;
import com.luc.logistica.crearOrdenes.model.EncabezadoOC;


@Repository("ordenCompraRepository")
public interface OrdenCompraRepository extends JpaRepository<EncabezadoOC, DocKey> {
	
	@Query("select en from EncabezadoOC en where en.docKey.numero = ?1 and en.docKey.tipo = ?2")
	EncabezadoOC findById(Integer numero, String tipo);
	
	//@Query("SELECT en from EncabezadoOC en WHERE en.docKey.tipo = 'FAC' OR en.docKey.tipo = 'FACV' AND en.estado is null")
	@Query("SELECT en from EncabezadoOC en WHERE en.docKey.tipo = 'FAC' AND en.estado IS NULL")
	List<EncabezadoOC>findFinalizado();

	@Query("select en from EncabezadoOC en where en.documento = ?1")
	EncabezadoOC findByOper(String operacion);
}