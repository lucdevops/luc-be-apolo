package com.luc.logistica.crearOrdenes.repository;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.PlanillaLin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository("planillaLinRepository")
public interface PlanillaLinRepository extends JpaRepository<PlanillaLin, Integer> {

     @Query("select pl from PlanillaLin pl where pl.id = ?1")
     PlanillaLin findById(Integer id);

     @Query("SELECT pl FROM PlanillaLin pl WHERE pl.numPlanilla = ?1 and pl.almacen.nit =?2")
     List<PlanillaLin> findByNumPl(String numPlanilla, Double codigoLuc);

     @Query("SELECT pl FROM PlanillaLin pl WHERE pl.numPlanilla = ?1")
     List<PlanillaLin> findByNumPlanAlm(String numPlanilla);

     @Query("select pl from PlanillaLin pl where pl.proveedor = ?1")
     PlanillaLin finByProv(Integer proveedor);

     @Query("select pl from PlanillaLin pl where pl.oc = ?1")
     PlanillaLin findByOc(String oc);

     @Query("select pl from PlanillaLin pl where pl.numPedido = ?1")
     PlanillaLin findByNumPedido(String numPedido);

     @Query("select pl from PlanillaLin pl where pl.factura = ?1 and pl.numPedido = ?2")
     List<PlanillaLin> findByNumFactura(String factura, String numPedido);

     @Query("select pl from PlanillaLin pl where pl.numGuia = ?1 and pl.id = ?2")
     PlanillaLin findByNumGuia(String numGuia, Integer id);

     @Query("select pl from PlanillaLin pl where pl.numCita = ?1 and pl.id = ?2")
     PlanillaLin findByNumCita(String numCita, Integer id);

     @Query("select pl from PlanillaLin pl where pl.horaCita = ?1 and pl.id= ?2")
     PlanillaLin findByHoraCita(String horaCita, Integer id);

     @Query("select pl from PlanillaLin pl where pl.empresaTransp = ?1 and  pl.id = ?2")
     PlanillaLin findByEmp(String empresaTransp,Integer id);

     @Query("select pl from PlanillaLin pl where pl.observaciones = ?1 and  pl.id =?2 and pl.oc=?2")
     PlanillaLin findByObs(String observaciones, Integer id);

     @Query("select pl from PlanillaLin pl where pl.motivo = ?1 and  pl.id =?2 and pl.oc=?2")
     PlanillaLin findByMotiv(String motivo, Integer id);

    // @Query("select (select count(*) from PlanillaLin pl where pl.numPlanilla = ?1 and factura is not null) as fac , count(*) as total from PlanillaLin pl where pl.numPlanilla = ?1")
     @Query("select (select count(*) from PlanillaLin pl where pl.numPlanilla = ?1 and (factura is not null) or (observaciones is not null)) as fac , count(*) as total from PlanillaLin pl where pl.numPlanilla = ?1")
     Object[] findDatos(String numPlanilla);
}