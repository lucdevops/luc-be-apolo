package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.SubGrupo;

@Repository("subGrupoRepository")
public interface SubGrupoRepository extends JpaRepository<SubGrupo, String>{
	
	@Query("select s from SubGrupo s where s.grupo = ?1")
	SubGrupo findBySub(String grupo);

	@Query("SELECT s FROM SubGrupo s WHERE s.nombre like %?1%")
	SubGrupo findByTexto(String texto);
}