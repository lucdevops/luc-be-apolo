package com.luc.logistica.crearOrdenes.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Marca;

@Repository("marcaRepository")
public interface MarcaRepository extends JpaRepository<Marca, String> {
	
	 @Query("select m from Marca m where m.id = ?1")
	 Marca findById(String id);

	 @Query("select m from Marca m where m.nit like %?1%")
	 Marca findByNit(String nit);
	 
	 @Query("SELECT m FROM Marca m WHERE (m.nombre like %?1% OR m.id like %?1%)")
	 List<Marca>findByTexto(String texto);
	
}