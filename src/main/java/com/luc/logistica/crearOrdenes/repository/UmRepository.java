package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.Um;

@Repository("umRepository")
public interface UmRepository extends JpaRepository<Um, String> {
	
	 @Query("select u from Um u where u.codigo = ?1")
	 List<Um> findById(String codigo);

}