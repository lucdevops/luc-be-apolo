package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Proveedor;

@Repository("proveedorRepository")
public interface ProveedorRepository extends JpaRepository<Proveedor, String> {
	
	 @Query("select p from Proveedor p where p.id = ?1")
	 Proveedor findById(String id);
	 
	 @Query("select p from Proveedor p where p.nombre = ?1")
	 Proveedor findByName(String nombre);
}