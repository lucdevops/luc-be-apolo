package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Grupo;

@Repository("grupoRepository")
public interface GrupoRepository extends JpaRepository<Grupo, String>{
	
	@Query("select g from Grupo g where g.grupo = ?1")
	Grupo findByGru(String grupo);
}