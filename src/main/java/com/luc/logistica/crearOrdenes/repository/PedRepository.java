package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearOrdenes.model.Ped;
import com.luc.logistica.crearOrdenes.model.PedKey;

@Repository("pedRepository")
public interface PedRepository extends JpaRepository<Ped, PedKey>{
	
	@Query("select pe from Ped pe where pe.pedKey.numero = ?1 and pe.tipo =?2")
	Ped findByPed(Integer numero, String tipo);
	
	@Query("select pe from Ped pe where pe.so_id = ?1")
	Ped findById(String id);

	@Query("SELECT pe from Ped pe WHERE (pe.tipo = 'ORC' AND pe.estado = NULL) OR (pe.tipo = 'OPP' AND pe.estado = NULL)")
	List<Ped> findEnEspera();
}
