package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Lote;

@Repository("loteRepository")
public interface LoteRepository extends JpaRepository<Lote, Integer> {
	
	@Query("select l from Lote l where l.id = ?1")
	Lote findById(Integer id);
	
	@Query("select l from Lote l where l.codigo = ?1 AND l.lote = ?2")
	Lote findByLote(String codigo, String lote);
}