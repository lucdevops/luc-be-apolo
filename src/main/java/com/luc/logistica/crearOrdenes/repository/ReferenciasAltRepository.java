package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.ReferenciasAlt;

@Repository("referenciasAltRepository")
public interface ReferenciasAltRepository extends JpaRepository<ReferenciasAlt, String>{
	
	@Query("select a from ReferenciasAlt a where a.codigo = ?1")
	ReferenciasAlt findByAlt(String codigo);
	
}