package com.luc.logistica.crearOrdenes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.EncabezadoPedido;

@Repository("pedidoRepository")
public interface PedidoRepository extends JpaRepository<EncabezadoPedido, String> {
	
	 @Query("select p from EncabezadoPedido p where p.id = ?1")
	 EncabezadoPedido findById(String id);
	 
	 @Query("SELECT p from EncabezadoPedido p WHERE (p.ordenCompra like %?1% OR p.cliente.nombre like %?1% ) AND (p.estado = 'AL' OR p.estado = 'TX')")
	 List<EncabezadoPedido>findByTexto(String texto);
	 
	 @Query("SELECT p from EncabezadoPedido p WHERE (p.estado = 'AL' OR p.estado = 'TX')")
	 List<EncabezadoPedido>findEnProceso();
	 
	 @Query("SELECT p from EncabezadoPedido p WHERE p.estado = 'FI'")
	 List<EncabezadoPedido>findFinalizado();
	 
	 @Query("SELECT sum(d.cantidadEmbarcada) as cantidad from EncabezadoPedido p, DetallePedido d WHERE d.encabezado = p.id and p.estado = 'FA' and YEAR(p.fecha) = ?1 and MONTH(p.fecha) = ?2 and d.articulo = ?3")
	 Double pedidosArticuloAnoMes(String ano,String mes,String articulo);
	
}