package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Vendedor;

@Repository("vendedorRepository")
public interface VendedorRepository extends JpaRepository<Vendedor, String>  {
	@Query("select v from Vendedor v where v.id = ?1")
	Vendedor findById(String id);
}
