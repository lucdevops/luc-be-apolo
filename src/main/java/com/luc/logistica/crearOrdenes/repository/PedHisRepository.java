package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.luc.logistica.crearOrdenes.model.PedHis;
import com.luc.logistica.crearOrdenes.model.PedKey;

@Repository("pedHisRepository")
public interface PedHisRepository extends JpaRepository<PedHis, PedKey> {

}
