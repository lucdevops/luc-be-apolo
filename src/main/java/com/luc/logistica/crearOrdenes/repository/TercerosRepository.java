package com.luc.logistica.crearOrdenes.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Terceros;

@Repository("terRepository")
public interface TercerosRepository extends JpaRepository<Terceros, Double>{
	
	@Query("select t from Terceros t where t.codigoLuc = ?1")
	Terceros findByTer(String codigoLuc);
	
	@Query("select t from Terceros t where t.ean = ?1")
	Terceros findByEan(String ean);
	
	@Query("SELECT t FROM Terceros t WHERE (t.nombres like %?1% OR t.ean like %?1%)")
	 List<Terceros> findByTexto(String texto);

}
