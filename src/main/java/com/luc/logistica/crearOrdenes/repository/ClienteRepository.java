package com.luc.logistica.crearOrdenes.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Cliente;

@Repository("clienteRepository")
public interface ClienteRepository extends JpaRepository<Cliente, String> {
	
     @Query("select c from Cliente c where c.id = ?1")
	 Cliente findById(String id);
     
	 @Query("select c from Cliente c where c.id2 = ?1")
	 Cliente findById2(String id);
	 
	 @Query("SELECT c FROM Cliente c WHERE (c.nombre like %?1% OR c.id like %?1% OR c.id2 like %?1%)")
	 List<Cliente>findByTexto(String texto);
	
}