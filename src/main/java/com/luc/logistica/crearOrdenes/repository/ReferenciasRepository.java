package com.luc.logistica.crearOrdenes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.luc.logistica.crearOrdenes.model.Referencias;

@Repository("refeRepository")
public interface ReferenciasRepository extends JpaRepository<Referencias, String>{

	@Query("select re from Referencias re where re.codigo = ?1")
	Referencias findByRefe(String codigo);
	
	@Query("SELECT re FROM Referencias re WHERE (re.descripcion like %?1% OR re.codigo like %?1%)")
	 List<Referencias>findByTexto(String texto);
}
