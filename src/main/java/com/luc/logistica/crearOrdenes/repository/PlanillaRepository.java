package com.luc.logistica.crearOrdenes.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.Planilla;
@Repository("planillaRepository")
public interface PlanillaRepository extends JpaRepository<Planilla, String> {

     @Query("select p from Planilla p where p.id = ?1")
     Planilla findById(String id);   

     @Query("select p from Planilla p where p.id like %?1%")
     List<Planilla> findByPD(String f);

     @Query("select p from Planilla p where  p.vehiculo= ?1 and p.id=?2")
     List<Planilla> findByVehiculo(String vehiculo);

     @Query("select p from Planilla p where p.conductor.codigoLuc = ?1 and p.id=?2")
     List<Planilla> findByConductor(String conductor);

     @Query("select p from Planilla p where p.ayudante1.codigoLuc = ?1 and p.id=?2")
     List<Planilla> findByAyudante(String ayudante1);

     @Query("select p from Planilla p where p.ayudante2.codigoLuc = ?1 and p.id=?2")
     List<Planilla> findByAyudante2(String ayudante2);
     
     @Query("select p from Planilla p where p.ayudante3.codigoLuc = ?1 and p.id=?2")
     List<Planilla> findByAyudante3(String ayudante3);
     
     @Query("select p from Planilla p where p.estado = ?1 and p.id = ?2")
     Planilla findByEstado(String estado, String id);
    
}