package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.DetalleOC;

@Repository("detalleOCRepository")
public interface DetalleOCRepository extends JpaRepository<DetalleOC, Integer>{
	
	@Query("select doc from DetalleOC doc where doc.id = ?1" )
	DetalleOC findById(Integer id);
}