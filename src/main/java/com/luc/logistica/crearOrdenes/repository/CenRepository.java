package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.CenData;

@Repository("cenRepository")
public interface CenRepository extends JpaRepository<CenData, String> {
	 @Query("select c from CenData c where c.id = ?1")
	 CenData findById(String id);
	 
	 @Query("select c from CenData c where c.pedido = ?1")
	 CenData findByOp(String id);
	
}
