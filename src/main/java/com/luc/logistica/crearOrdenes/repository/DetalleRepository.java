package com.luc.logistica.crearOrdenes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearOrdenes.model.DetallePedido;
import com.luc.logistica.crearOrdenes.model.PedidoKey;

@Repository("detalleRepository")
public interface DetalleRepository extends JpaRepository<DetallePedido, PedidoKey> {
	
}
