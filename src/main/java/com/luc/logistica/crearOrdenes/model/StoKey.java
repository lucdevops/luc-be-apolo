package com.luc.logistica.crearOrdenes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StoKey implements Serializable{
	
	@Column(name = "bodega")
    protected Integer bodega;
    @Column(name = "codigo")
    protected String codigo;
    @Column(name = "ano")
    protected Integer ano;
    @Column(name = "mes")
    protected Integer mes;
	
	public StoKey(Integer bodega, String codigo, Integer ano, Integer mes) {
		super();
		this.bodega = bodega;
		this.codigo = codigo;
		this.ano = ano;
		this.mes = mes;
	}
    
	public StoKey() {}

	public Integer getBodega() {
		return bodega;
	}

	public void setBodega(Integer bodega) {
		this.bodega = bodega;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}
    

    
}

