package com.luc.logistica.crearOrdenes.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sp_mstr")
public class Vendedor {
	@Id
	@Column(name="sp_id")
	private String id;
	@Column(name="sp_name")
	private String name;
	
	@OneToMany(mappedBy="vendedor")
	private List<EncabezadoOC> pedOC;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
