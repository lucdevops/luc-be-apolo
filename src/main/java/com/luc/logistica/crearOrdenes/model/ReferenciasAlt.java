package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "referencias_alt")
public class ReferenciasAlt {
	@Id
	@Column(name = "codigo")
	private String codigo;
	@Column(name = "alterno")
	private Integer alterno;
		
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getAlterno() {
		return alterno;
	}
	public void setAlterno(Integer alterno) {
		this.alterno = alterno;
	}	
}
