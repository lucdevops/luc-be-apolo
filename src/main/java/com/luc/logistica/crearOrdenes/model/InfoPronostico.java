package com.luc.logistica.crearOrdenes.model;

public class InfoPronostico {
	private Double anoPasado;
	private Double mesAnterior;
	private Double promedioTresMeses;
	public Double getAnoPasado() {
		return anoPasado;
	}
	public void setAnoPasado(Double anoPasado) {
		this.anoPasado = anoPasado;
	}
	public Double getMesAnterior() {
		return mesAnterior;
	}
	public void setMesAnterior(Double mesAnterior) {
		this.mesAnterior = mesAnterior;
	}
	public Double getPromedioTresMeses() {
		return promedioTresMeses;
	}
	public void setPromedioTresMeses(Double promedioTresMeses) {
		this.promedioTresMeses = promedioTresMeses;
	}
	
}
