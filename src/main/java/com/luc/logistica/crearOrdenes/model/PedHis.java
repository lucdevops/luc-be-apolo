package com.luc.logistica.crearOrdenes.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "documentos_ped_historia")
public class PedHis {

	@EmbeddedId
	protected PedKey pedKey;

	@ManyToOne
	@JoinColumn(name = "nit", columnDefinition = "DECIMAL")
	private Terceros tercero;
	@Column(name = "anulado")
	private Integer anulado;
	@Column(name = "fecha")
	private Date fecha;
	@Column(name = "fecha_hora")
	private Date fechaHora;
	@Column(name = "fecha_hora_entrega")
	private Date fechaHoraEnt;
	@Column(name = "usuario")
	private String usuario;
	@Column(name = "pc")
	private String pc;
	@Column(name = "nit_destino")
	private Double nitDestino;
	@ManyToOne
	@JoinColumn(name = "vendedor")
	private Terceros vendedor;
	@Column(name = "condicion")
	private String condicion;
	@Column(name = "codigo_direccion")
	private Integer codigoDir;
	@Column(name = "notas")
	private String notas;
	@Column(name = "proveedor")
	private String proveedor;
	@Column(name = "tipo")
	private String tipo;


	public Integer getCodigoDir() {
		return codigoDir;
	}

	public void setCodigoDir(Integer codigoDir) {
		this.codigoDir = codigoDir;
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	public Terceros getTercero() {
		return tercero;
	}

	public void setTercero(Terceros tercero) {
		this.tercero = tercero;
	}

	public Integer getAnulado() {
		return anulado;
	}

	public void setAnulado(Integer anulado) {
		this.anulado = anulado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Date getFechaHoraEnt() {
		return fechaHoraEnt;
	}

	public void setFechaHoraEnt(Date fechaHoraEnt) {
		this.fechaHoraEnt = fechaHoraEnt;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public Double getNitDestino() {
		return nitDestino;
	}

	public void setNitDestino(Double nitDestino) {
		this.nitDestino = nitDestino;
	}

	public PedKey getPedKey() {
		return pedKey;
	}

	public void setPedKey(PedKey pedKey) {
		this.pedKey = pedKey;
	}

	public Terceros getVendedor() {
		return vendedor;
	}

	public void setVendedor(Terceros vendedor) {
		this.vendedor = vendedor;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}




}