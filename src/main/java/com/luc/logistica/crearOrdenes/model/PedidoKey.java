package com.luc.logistica.crearOrdenes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PedidoKey implements Serializable{

    @Column(name = "sod_so_id")
    protected String encabezado;
    @Column(name = "sod_pt_id")
    protected String articulo;

	public PedidoKey(String encabezado, String articulo) {
		super();
		this.encabezado = encabezado;
		this.articulo = articulo;
	}

	public PedidoKey() {}

}
