package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sod_det")
public class DetallePedido {
	
	@EmbeddedId
    protected PedidoKey pedidoKey;
	
	@ManyToOne
	@JoinColumn(name="sod_so_id", insertable = false, updatable = false)
    private EncabezadoPedido encabezado;
    
	public PedidoKey getPedidoKey() {
		return pedidoKey;
	}
	public void setPedidoKey(PedidoKey pedidoKey) {
		this.pedidoKey = pedidoKey;
	}
	
	@ManyToOne
	@JoinColumn(name="sod_pt_id", insertable = false, updatable = false)
    private Articulo articulo;
	
	@Column(name = "sod_qty_ord")
	private Double cantidadOrdenada;
	@Column(name = "sod_qty_emb")
	private Double cantidadEmbarcada;
	@Column(name = "sod_unitprice")
	private Double precioUnitario;
	@Column(name = "sod_taxval")
	private Double impuesto;
	@Column(name = "sod_discval")
	private Double descuento;
	@Column(name = "sod_subtotal")
	private Double subtotal;
	@Column(name = "sod_total")
	private Double total;
	@Column(name = "sod_status")
	private String status;
	@Column(name = "sod_qty_pick")
	private Double cantidadAlistada;
	
	public EncabezadoPedido getEncabezado() {
		return encabezado;
	}
	public void setEncabezado(EncabezadoPedido encabezado) {
		this.encabezado = encabezado;
	}
	public Articulo getArticulo() {
		return articulo;
	}
	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	public Double getCantidadOrdenada() {
		return cantidadOrdenada;
	}
	public void setCantidadOrdenada(Double cantidadOrdenada) {
		this.cantidadOrdenada = cantidadOrdenada;
	}
	public Double getCantidadEmbarcada() {
		return cantidadEmbarcada;
	}
	public void setCantidadEmbarcada(Double cantidadEmbarcada) {
		this.cantidadEmbarcada = cantidadEmbarcada;
	}
	public Double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public Double getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(Double impuesto) {
		this.impuesto = impuesto;
	}
	public Double getDescuento() {
		return descuento;
	}
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getCantidadAlistada() {
		return cantidadAlistada;
	}
	public void setCantidadAlistada(Double cantidadAlistada) {
		this.cantidadAlistada = cantidadAlistada;
	}
    
}
