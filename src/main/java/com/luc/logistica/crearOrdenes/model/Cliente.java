package com.luc.logistica.crearOrdenes.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cm_mstr")
public class Cliente {
	
	 	@Id
		@Column(name = "cm_id2")
		private String id2;
		@Column(name = "cm_id")
		private String id;
		@Column(name = "cm_name")
		private String nombre;
		@Column(name = "cm_city")
		private String ciudad;
		@Column(name = "cm_lat")
		private Double latitud;
		@Column(name = "cm_lon")
		private Double longitud;
		@Column(name = "cm_canal")
		private String canal;
		@Column(name = "cm_sp_id")
		private String vendedor;
		@Column(name = "cm_pr_list")
		private String listaPrecio;
		@OneToMany(mappedBy="cliente")
		private List<EncabezadoPedido> pedidos;
		
		public String getVendedor() {
			return vendedor;
		}
		public void setVendedor(String vendedor) {
			this.vendedor = vendedor;
		}
		public String getListaPrecio() {
			return listaPrecio;
		}
		public void setListaPrecio(String listaPrecio) {
			this.listaPrecio = listaPrecio;
		}
		public String getId2() {
			return id2;
		}
		public void setId2(String id2) {
			this.id2 = id2;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getCiudad() {
			return ciudad;
		}
		public void setCiudad(String ciudad) {
			this.ciudad = ciudad;
		}
		public double getLatitud() {
			return latitud;
		}
		public void setLatitud(double latitud) {
			this.latitud = latitud;
		}
		public double getLongitud() {
			return longitud;
		}
		public void setLongitud(double longitud) {
			this.longitud = longitud;
		}
		public String getCanal() {
			return canal;
		}
		public void setCanal(String canal) {
			this.canal = canal;
		}
		public List<EncabezadoPedido> getPedidos() {
			return pedidos;
		}
		public void setPedidos(List<EncabezadoPedido> pedidos) {
			this.pedidos = pedidos;
		}
		public void setLatitud(Double latitud) {
			this.latitud = latitud;
		}
		public void setLongitud(Double longitud) {
			this.longitud = longitud;
		}

	
}
