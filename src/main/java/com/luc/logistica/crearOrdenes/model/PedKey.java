package com.luc.logistica.crearOrdenes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PedKey implements Serializable{
	@Column(name = "sw")
    protected Integer sw;
    @Column(name = "bodega")
    protected Integer bodega;
    @Column(name = "numero")
    protected Integer numero;
    
    public PedKey(Integer sw, Integer bodega, Integer numero) {
		super();
		this.sw = sw;
		this.bodega = bodega;
		this.numero = numero;
	}
    
    public PedKey() {}

	public Integer getSw() {
		return sw;
	}

	public void setSw(Integer sw) {
		this.sw = sw;
	}

	public Integer getBodega() {
		return bodega;
	}

	public void setBodega(Integer bodega) {
		this.bodega = bodega;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
    
}
