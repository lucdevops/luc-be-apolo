package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "referencias_sto")
public class ReferenciasSto {
	
	@EmbeddedId
    protected StoKey stoKey;
	
	@Column(name="can_ini")
	private Double canIni;
	@Column(name="can_ent")
	private Double canEnt;
	@Column(name="can_sal")
	private Double canSal;
	
	public StoKey getStoKey() {
		return stoKey;
	}
	public void setStoKey(StoKey stoKey) {
		this.stoKey = stoKey;
	}
	public Double getCanIni() {
		return canIni;
	}
	public void setCanIni(Double canIni) {
		this.canIni = canIni;
	}
	public Double getCanEnt() {
		return canEnt;
	}
	public void setCanEnt(Double canEnt) {
		this.canEnt = canEnt;
	}
	public Double getCanSal() {
		return canSal;
	}
	public void setCanSal(Double canSal) {
		this.canSal = canSal;
	}
}
