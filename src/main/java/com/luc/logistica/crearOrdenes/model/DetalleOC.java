package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "documentos_lin")
public class DetalleOC {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",nullable = false, unique = true)
	private Integer id;
	@Column(name="numero")
	private Integer numero;
	@Column(name="tipo")
	private String tipo;
	@Column(name="sw")
	private Integer sw;
	@ManyToOne
	@JoinColumn(name="nit",columnDefinition = "DECIMAL")
	private Terceros tercero;
	@Column(name="bodega")
	private Integer bodega;
	@Column(name="seq")
	private Integer seq;
	@Column(name="cantidad")
	private Double cantidad;
	@Column(name="cantidad_pedida")
	private Double cantidadPedida;
	@Column(name="cantidad_recibida")
	private Double cantidadRecibida;
	@Column(name="porcentaje_iva")
	private Double iva;
	@Column(name="valor_unitario")
	private Double valor;
	@Column(name="porcentaje_descuento")
	private Double descuento;
	@Column(name="costo_unitario")
	private Double subtotal;
	@Column(name="estado")
	private String estado;
	
	public EncabezadoOC getDocumento() {
		return documento;
	}
	public void setDocumento(EncabezadoOC documento) {
		this.documento = documento;
	}
	@ManyToOne
	@JoinColumn(name="id_lote")
	private Lote lote;
	
	@Column(name="Id_Documentos_lin_ped")
	private Integer idDocLinPed;
	
	@MapsId("numero")
	@ManyToOne
    @JoinColumns({
        @JoinColumn(name="tipo", referencedColumnName = "tipo",insertable=false,updatable=false),
        @JoinColumn(name="numero", referencedColumnName = "numero",insertable=false,updatable=false)
    })
    private EncabezadoOC documento;
	
	
	@ManyToOne
	@JoinColumn(name="codigo")
    private Referencias refe;
	
	public Integer getIdDocLinPed() {
		return idDocLinPed;
	}
	public void setIdDocLinPed(Integer idDocLinPed) {
		this.idDocLinPed = idDocLinPed;
	}
	
	public Lote getLote() {
		return lote;
	}
	public void setLote(Lote lote) {
		this.lote = lote;
	}
	public Integer getId() {
		return id;
	}
	public Double getCantidadPedida() {
		return cantidadPedida;
	}
	public void setCantidadPedida(Double cantidadPedida) {
		this.cantidadPedida = cantidadPedida;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getSw() {
		return sw;
	}
	public void setSw(Integer sw) {
		this.sw = sw;
	}
	public Terceros getTercero() {
		return tercero;
	}
	public void setTercero(Terceros tercero) {
		this.tercero = tercero;
	}
	public Integer getBodega() {
		return bodega;
	}
	public void setBodega(Integer bodega) {
		this.bodega = bodega;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Referencias getRefe() {
		return refe;
	}
	public void setRefe(Referencias refe) {
		this.refe = refe;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getDescuento() {
		return descuento;
	}
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	public Double getCantidadRecibida() {
		return cantidadRecibida;
	}
	public void setCantidadRecibida(Double cantidadRecibida) {
		this.cantidadRecibida = cantidadRecibida;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
