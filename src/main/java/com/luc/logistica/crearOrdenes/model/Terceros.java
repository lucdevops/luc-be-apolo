package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "terceros")
public class Terceros {
	
	@Id
	@Column(name = "nit",columnDefinition = "DECIMAL")
	private Double nit;
	@Column(name = "ean")
	private String ean;
	@Column(name = "nombres")
	private String nombres;
	@Column(name = "codigo_luc")
    private String codigoLuc;
	@Column(name = "list_precio")
    private String listaPrecio;
	@Column(name = "ciudad")
    private String ciudad;
	@Column(name = "direccion")
    private String direccion;
	@Column(name = "nit_real")
    private Double nitReal;
	@Column(name = "vendedor")
	private Double vendedor;
	@Column(name = "vd")
	private String vd;
	@Column(name = "pais")
	private String pais;
	@Column(name = "gran_contribuyente")
	private Integer contribuyente;
	@Column(name = "autoretenedor")
	private Integer autoretenedor;
	@Column(name = "concepto_1")
	private String concep1;
	@Column(name = "concepto_6")
	private String concep6;
	@Column(name = "y_pais")
	private String ypais;
	@Column(name = "y_dpto")
	private Integer ydpto;
	@Column(name = "y_ciudad")
	private Integer yciudad;
	
	public String getCodigoLuc() {
		return codigoLuc;
	}
	public void setCodigoLuc(String codigoLuc) {
		this.codigoLuc = codigoLuc;
	}
	public Double getNit() {
		return nit;
	}
	public void setNit(Double nit) {
		this.nit = nit;
	}
	public String getEan() {
		return ean;
	}
	public void setEan(String ean) {
		this.ean = ean;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getListaPrecio() {
		return listaPrecio;
	}
	public void setListaPrecio(String listaPrecio) {
		this.listaPrecio = listaPrecio;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Double getNitReal() {
		return nitReal;
	}
	public void setNitReal(Double nitReal) {
		this.nitReal = nitReal;
	}
	public Double getVendedor() {
		return vendedor;
	}
	public void setVendedor(Double vendedor) {
		this.vendedor = vendedor;
	}
	public String getVd() {
		return vd;
	}
	public void setVd(String vd) {
		this.vd = vd;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public Integer getContribuyente() {
		return contribuyente;
	}
	public void setContribuyente(Integer contribuyente) {
		this.contribuyente = contribuyente;
	}
	public Integer getAutoretenedor() {
		return autoretenedor;
	}
	public void setAutoretenedor(Integer autoretenedor) {
		this.autoretenedor = autoretenedor;
	}
	public String getConcep1() {
		return concep1;
	}
	public void setConcep1(String concep1) {
		this.concep1 = concep1;
	}
	public String getConcep6() {
		return concep6;
	}
	public void setConcep6(String concep6) {
		this.concep6 = concep6;
	}
	public String getYpais() {
		return ypais;
	}
	public void setYpais(String ypais) {
		this.ypais = ypais;
	}
	public Integer getYdpto() {
		return ydpto;
	}
	public void setYdpto(Integer ydpto) {
		this.ydpto = ydpto;
	}
	public Integer getYciudad() {
		return yciudad;
	}
	public void setYciudad(Integer yciudad) {
		this.yciudad = yciudad;
	}
}
