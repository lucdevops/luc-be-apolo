package com.luc.logistica.crearOrdenes.model;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "documentos")
public class EncabezadoOC {
	
	@EmbeddedId
    protected DocKey docKey;
	@Column(name="sw",columnDefinition = "TINYINT")
	private Integer sw;
	@Column(name="fecha")
	private Date fecha;
	@Column(name="fecha_hora")
	private Date fechaHora;
	@ManyToOne
	@JoinColumn(name="nit",columnDefinition = "DECIMAL")
	private Terceros tercero;
	@Column(name="bodega",columnDefinition = "SMALLINT")
	private Integer bodega;
	@Column(name="anulado", columnDefinition="BIT")
	private Boolean anulado;
	@Column(name="usuario")
	private String usuario;
	@Column(name="pc")
	private String pc;
	@Column(name="notas")
	private String notas;
	@ManyToOne
	@JoinColumn(name="vendedor")
	private Terceros vendedor;
	@Column(name="documento")
	private String documento;
	@Column(name="estado")
	private String estado;
	@Column(name="valor_total")
	private Double valorTotal;
	@OneToMany(mappedBy="documento")
	private List<DetalleOC> detalles;
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Integer getBodega() {
		return bodega;
	}
	public void setBodega(Integer bodega) {
		this.bodega = bodega;
	}
	public Boolean getAnulado() {
		return anulado;
	}
	public void setAnulado(Boolean anulado) {
		this.anulado = anulado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPc() {
		return pc;
	}
	public void setPc(String pc) {
		this.pc = pc;
	}
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	public DocKey getDocKey() {
		return docKey;
	}
	public void setDocKey(DocKey docKey) {
		this.docKey = docKey;
	}
	public Integer getSw() {
		return sw;
	}
	public void setSw(Integer sw) {
		this.sw = sw;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Terceros getTercero() {
		return tercero;
	}
	public void setTercero(Terceros tercero) {
		this.tercero = tercero;
	}
	public Terceros getVendedor() {
		return vendedor;
	}
	public void setVendedor(Terceros vendedor) {
		this.vendedor = vendedor;
	}
	public List<DetalleOC> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleOC> detalles) {
		this.detalles = detalles;
	}
	public Double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
		
	
}
