package com.luc.logistica.crearOrdenes.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "referencias")
public class Referencias {
	@Id
	@Column(name = "codigo")
	private String codigo;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "generico")
	private String generico;
	@Column(name = "clase")
	private String clase;
	@Column(name = "contable")
	private Integer contable;
	@Column(name = "grupo")
	private String grupo;
	@Column(name = "subgrupo")
	private String subgrupo;
	@Column(name = "nit")
	private Double nit;
	@Column(name = "marca")
	private String marca;
	@Column(name = "valor_unitario")
	private Double valorUni;
	@Column(name = "porcentaje_iva")
	private Integer iva;
	@Column(name = "maneja_inventario")
	private Integer inventario;
	@Column(name = "tam_alto")
	private Integer alto;
	@Column(name = "tam_ancho")
	private Integer ancho;
	
	
	@OneToMany(mappedBy="refe")
	private List<DetalleOC> detalles;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getGenerico() {
		return generico;
	}
	public void setGenerico(String generico) {
		this.generico = generico;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public Integer getContable() {
		return contable;
	}
	public void setContable(Integer contable) {
		this.contable = contable;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getSubgrupo() {
		return subgrupo;
	}
	public void setSubgrupo(String subgrupo) {
		this.subgrupo = subgrupo;
	}
	public Double getNit() {
		return nit;
	}
	public void setNit(Double nit) {
		this.nit = nit;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Double getValorUni() {
		return valorUni;
	}
	public void setValorUni(Double valorUni) {
		this.valorUni = valorUni;
	}
	public Integer getIva() {
		return iva;
	}
	public void setIva(Integer iva) {
		this.iva = iva;
	}
	public Integer getInventario() {
		return inventario;
	}
	public void setInventario(Integer inventario) {
		this.inventario = inventario;
	}
	public Integer getAlto() {
		return alto;
	}
	public void setAlto(Integer alto) {
		this.alto = alto;
	}
	public Integer getAncho() {
		return ancho;
	}
	public void setAncho(Integer ancho) {
		this.ancho = ancho;
	}
	
}
