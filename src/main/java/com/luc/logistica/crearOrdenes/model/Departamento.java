package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "y_departamentos")
public class Departamento {

	@Id
	@Column(name="departamento")
	private String departamento;
	@Column(name="descripcion")
	private String nombre;

	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}