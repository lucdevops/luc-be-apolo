package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "um_mstr")
public class Um {

	@Id
	@Column(name = "um_id")
	private String id;
	@Column(name = "um_org")
	private String org;
	@Column(name = "um_dest")
	private String dest;
	@Column(name = "um_pt_id")
	private String codigo;
	@Column(name = "um_conv")
	private Integer conv;

	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getConv() {
		return conv;
	}
	public void setConv(Integer conv) {
		this.conv = conv;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
}
