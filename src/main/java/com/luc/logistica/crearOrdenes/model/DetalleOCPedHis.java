package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "documentos_lin_ped_historia")
public class DetalleOCPedHis {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",nullable = false, unique = true)
	private Integer id;
	@Column(name="sw")
	private Integer sw;
	@Column(name="bodega")
	private Integer bodega;
	@Column(name="numero")
	private Integer numero;
	@Column(name="codigo")
	private String codigo;
	@Column(name="seq")
	private Integer seq;
	@Column(name="cantidad")
	private Double cantidad;
	@Column(name="cantidad_despachada")
	private Double cantidadDes;
	@Column(name="valor_unitario")
	private Double valorUni;
	@Column(name="porcentaje_iva")
	private Double porcentajeIva;
	@Column(name="porcentaje_descuento")
	private Double porcentajeDes;
	@Column(name="und")
	private String und;
	@Column(name="cantidad_und")
	private Double cantidadUnd;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSw() {
		return sw;
	}
	public void setSw(Integer sw) {
		this.sw = sw;
	}
	public Integer getBodega() {
		return bodega;
	}
	public void setBodega(Integer bodega) {
		this.bodega = bodega;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public Double getCantidadDes() {
		return cantidadDes;
	}
	public void setCantidadDes(Double cantidadDes) {
		this.cantidadDes = cantidadDes;
	}
	public Double getValorUni() {
		return valorUni;
	}
	public void setValorUni(Double valorUni) {
		this.valorUni = valorUni;
	}
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public Double getPorcentajeDes() {
		return porcentajeDes;
	}
	public void setPorcentajeDes(Double porcentajeDes) {
		this.porcentajeDes = porcentajeDes;
	}
	public String getUnd() {
		return und;
	}
	public void setUnd(String und) {
		this.und = und;
	}
	public Double getCantidadUnd() {
		return cantidadUnd;
	}
	public void setCantidadUnd(Double cantidadUnd) {
		this.cantidadUnd = cantidadUnd;
	}
	
}
