package com.luc.logistica.crearOrdenes.model;

public class Impuesto {
	private double total;
	private String impuesto;

	public String getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
}
