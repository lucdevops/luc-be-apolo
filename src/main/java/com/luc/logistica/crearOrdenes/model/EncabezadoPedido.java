package com.luc.logistica.crearOrdenes.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "so_hist")
public class EncabezadoPedido {
	
	@Id
	@Column(name="so_id")
	private String id;
	@Column(name="so_dt_id")
	private Date fecha;
	@Column(name="so_fec_libera")
	private Date fechaLibera;
	@Column(name="so_fec_carga")
	private Date fechaCarga;
	@ManyToOne
	@JoinColumn(name="so_cm_id")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name="so_sp_id")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="so_vd_id")
	private Proveedor proveedor;
	@Column(name="so_ped")
	private String ordenCompra;
	@Column(name="so_status")
	private String estado;
	@Column(name="so_pr_list")
	private String listaPrecio;
	@Column(name="so_us_id")
	private String alista;
	@Column(name="so_us_id_val")
	private String valida;
	@OneToMany(mappedBy="encabezado")
	private List<DetallePedido> detalles;
	
	
	public String getAlista() {
		return alista;
	}
	public void setAlista(String alista) {
		this.alista = alista;
	}
	public String getValida() {
		return valida;
	}
	public void setValida(String valida) {
		this.valida = valida;
	}
	public EncabezadoPedido() {
		detalles = new ArrayList<DetallePedido>();
	}
	public String getId() {
		return id;
	}
	public Date getFechaLibera() {
		return fechaLibera;
	}
	public void setFechaLibera(Date fechaLibera) {
		this.fechaLibera = fechaLibera;
	}
	public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Vendedor getVendedor() {
		return vendedor;
	}
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public String getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getListaPrecio() {
		return listaPrecio;
	}
	public void setListaPrecio(String listaPrecio) {
		this.listaPrecio = listaPrecio;
	}
	public List<DetallePedido> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetallePedido> detalles) {
		this.detalles = detalles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EncabezadoPedido other = (EncabezadoPedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
		
	
}
