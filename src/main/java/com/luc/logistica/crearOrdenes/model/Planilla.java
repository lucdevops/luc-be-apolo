package com.luc.logistica.crearOrdenes.model;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "planilla")
public class Planilla {

    @Id

    @Column(name = "num_planilla")
    private String id;
    @Column(name = "fecha")
    private Date fecha;
    @ManyToOne
    @JoinColumn(name = "conductor")
    private Terceros conductor;
    @ManyToOne
    @JoinColumn(name = "ayudante1")
    private Terceros ayudante1;
    @Column(name = "vehiculo")
    private String vehiculo;
    @Column(name = "estado")
    private String estado;
    @ManyToOne
    @JoinColumn(name = "ayudante2")
    private Terceros ayudante2;
    @ManyToOne
    @JoinColumn(name = "ayudante3")
    private Terceros ayudante3;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Terceros getConductor() {
        return conductor;
    }

    public void setConductor(Terceros conductor) {
        this.conductor = conductor;
    }

    public Terceros getAyudante1() {
        return ayudante1;
    }

    public void setAyudante1(Terceros ayudante1) {
        this.ayudante1 = ayudante1;
    }

    public Terceros getAyudante2() {
        return ayudante2;
    }

    public void setAyudante2(Terceros ayudante2) {
        this.ayudante2 = ayudante2;
    }

    public Terceros getAyudante3() {
        return ayudante3;
    }

    public void setAyudante3(Terceros ayudante3) {
        this.ayudante3 = ayudante3;
    }

}
