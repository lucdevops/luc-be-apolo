package com.luc.logistica.crearOrdenes.model;

public class Precio {
	private double precio;
	private double impuesto;
	private double descuento;
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public double getTotal() {
		return precio * (descuento / 100) * (1+ (impuesto / 100));
	}
	
	
}
