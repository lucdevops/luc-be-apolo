package com.luc.logistica.crearOrdenes.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cen_data")
public class CenData {
	@Id
	@Column(name = "cen_id")
	private String id;
	@Column(name = "cen_vd")
	private String proveedor;
	@Column(name = "cen_date")
	private Date fecha;
	@Column(name = "cen_so_id")
	private String pedido;
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getPedido() {
		return pedido;
	}
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	
}