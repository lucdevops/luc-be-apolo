package com.luc.logistica.crearOrdenes.model;

import java.io.Serializable;

public class ErrorResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2019443809468476569L;
	private int status;
	private String msj;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsj() {
		return msj;
	}
	public void setMsj(String msj) {
		this.msj = msj;
	}
	
}

