package com.luc.logistica.crearOrdenes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "referencias_gru")
public class Grupo {

	@Id
	@Column(name="grupo")
	private String grupo;
	@Column(name="descripcion")
	private String desc;

	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}